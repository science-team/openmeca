// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@yakuru.fr>
//
// Copyright (C) 2012-2017 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Action_DeleteUserItem_hpp
#define OpenMeca_Action_DeleteUserItem_hpp


#include "OpenMeca/Core/System.hpp"
#include "OpenMeca/Gui/MainWindow.hpp"
#include "OpenMeca/Gui/Dialog/DialogCheckDeleteItem.hpp"

namespace OpenMeca
{
  namespace Core
  {
    
    template <class T>
    class ActionDeleteUserItem
    {
    public:
      static QString Text();
      static std::string Id();
      static QIcon Icon();
      template<class Action> static void DoAction(T&, Action&);
      template<class Action> static void DeleteItem(T&, Action&);
    };
    
    template<class T> 
    inline QString
    ActionDeleteUserItem<T>::Text()
    {
      return Action::tr("Delete"); 
    }

    template<class T> 
    inline std::string
    ActionDeleteUserItem<T>::Id()
    {
      return "Delete"; 
    }

    template<class T> 
    inline QIcon
    ActionDeleteUserItem<T>::Icon()
    {
      QPixmap pixmap = Core::Singleton< Core::ItemCommonProperty<T> >::Get().GetIconSymbol().pixmap(14,14);
      QPainter painter(&pixmap);
      QPen pen(Qt::red);
      pen.setWidth(2);
      painter.setPen(pen);
      painter.drawLine (0,  14,  14, 0);
      painter.drawLine (14, 14,  0,  0);
      return QIcon(pixmap);
    }

    template<class T>
    template<class Action> 
    inline void
    ActionDeleteUserItem<T>::DeleteItem(T& item, Action& action)
    {
      Core::System::Get().DisableDrawing();
      delete(&item);
      Core::System::Get().EnableDrawing();
      Core::System::Get().Update();
      Core::System::Get().GetHistoric().SystemEdited();
      Gui::MainWindow::Get().AddToHistoric(action);
    }



    template<class T>
    template<class Action>    
    inline void
    ActionDeleteUserItem<T>::DoAction(T& item, Action& action)
    {
      const Core::SetOfBase<Core::UserItem>& dependentItems = item.GetAllDependentItems();
      if (dependentItems.GetTotItemNumber() == 0)
	{
	  DeleteItem(item, action);
	}
      else
        {
	  Gui::DialogCheckDeleteItem& dialog = Gui::DialogCheckDeleteItem::Get();
	  dialog.BuildListView(dependentItems);
	  if (dialog.exec())
	    {
	      DeleteItem(item, action);
	    }
	}
    }
   
  }
}

#endif
