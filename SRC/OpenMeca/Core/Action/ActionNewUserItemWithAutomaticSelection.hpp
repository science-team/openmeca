// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@yakuru.fr>
//
// Copyright (C) 2012-2017 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Core_Action_ActionNewUserItemWithAutomaticSelection_hpp
#define OpenMeca_Core_Action_ActionNewUserItemWithAutomaticSelection_hpp


//
// This action call the constructor with the first item of the global set as parent
//

#include "OpenMeca/Core/Action/ActionWithSelectedItemT.hpp"
#include "OpenMeca/Core/System.hpp"
#include "OpenMeca/Gui/MainWindow.hpp"

namespace OpenMeca
{
  namespace Core
  {
    
    template <class T, class New>
    class ActionNewUserItemWithAutomaticSelection
    {
      public:    
      static QString Text();
      static std::string Id();
      static QIcon Icon();
      template<class Action> static void DoAction(Action& action);
    };
    
    template<class T, class New> 
    inline QString
    ActionNewUserItemWithAutomaticSelection<T,New>::Text()
    {
      return Action::tr("New");
    }

    template<class T, class New> 
    inline std::string
    ActionNewUserItemWithAutomaticSelection<T,New>::Id()
    {
      return "New";
    }

    template<class T, class Parent> 
    inline QIcon
    ActionNewUserItemWithAutomaticSelection<T,Parent>::Icon()
    {
      QPixmap pixmap = Core::Singleton< Core::ItemCommonProperty<T> >::Get().GetIconSymbol().pixmap(14,14);
      QPainter painter(&pixmap);
      QPen pen(Qt::darkGreen);
      pen.setWidth(2);
      painter.setPen(pen);
      painter.drawLine (6, 9, 12,  9);
      painter.drawLine (9, 6,  9,  12);
      return QIcon(pixmap);
    }

    template<class T, class Parent>
    template<class Action> 
    inline void
    ActionNewUserItemWithAutomaticSelection<T,Parent>::DoAction(Action& action)
    {
      if (Core::System::Get().GetSetOf<Parent>().GetTotItemNumber() ==0)
	{
          QString msg = Action::tr("You must build a ") + 
	    Parent::GetQStrType() + Action::tr(" instance first");
          QMessageBox::warning(&Gui::MainWindow::Get(), Action::tr("Warning"), msg);
	  return;
	}
      OMC_ASSERT_MSG(Core::System::Get().GetSetOf<Parent>().GetTotItemNumber() > 0,
		 "Can't automatically create item because there is not any possible parent");
      Parent& item = Core::System::Get().GetSetOf<Parent>()(0);
      Core::Singleton< Core::ItemCommonProperty<T> >::Get().GetDialog().New(action, item);
    }


  }
}


#endif
