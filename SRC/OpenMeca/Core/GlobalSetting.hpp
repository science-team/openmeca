// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@yakuru.fr>
//
// Copyright (C) 2012-2017 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Core_GlobalSetting_hpp
#define OpenMeca_Core_GlobalSetting_hpp

#include <QFile>
#include "OpenMeca/Core/XmlConfigFile.hpp"
#include "OpenMeca/Core/AutoRegister.hpp"
#include "OpenMeca/Core/Item.hpp"

namespace OpenMeca
{
  namespace Core
  {

    // Base class for global setting. A global setting is a unique setting 
    // that belongs to the openmeca software.
    class GlobalSetting : public Item, 
			  public AutoRegister<GlobalSetting>
    {
    public:    
      GlobalSetting(const std::string strType);
      virtual ~GlobalSetting();
      
      virtual void ReadXmlFile() = 0;
      virtual void WriteXmlFile() = 0;

      QFile& OpenXmlConfigFile();
      void SaveXmlConfigFile(QDomDocument&);
      
    private :
      GlobalSetting(const GlobalSetting&);             //Not Allowed
      GlobalSetting& operator=(const GlobalSetting&);  //Not Allowed

    protected:
      XmlConfigFile file_;

    }; 
    
    

  }
}
#endif
