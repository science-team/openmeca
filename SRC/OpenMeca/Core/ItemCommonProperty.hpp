// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@yakuru.fr>
//
// Copyright (C) 2012-2017 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Core_ItemCommonProperty_hpp
#define OpenMeca_Core_ItemCommonProperty_hpp


#include <QIcon>


#include "OpenMeca/Core/CommonProperty.hpp"
#include "OpenMeca/Core/Singleton.hpp"


namespace OpenMeca
{
  namespace Core
  {

    // Common property for the classes that inherit from the 'Item' class
    template<class T>
    class ItemCommonProperty :public CommonProperty,  public Singleton< ItemCommonProperty<T> >
    {
      friend class Singleton< ItemCommonProperty<T> >;
    
    public:
      const std::string& GetStrType() const;
      const QString GetQStrType() const;

      const QIcon& GetIconSymbol() const; 
      
      typedef typename T::Dialog Dialog;
      Dialog& GetDialog();
      
      
    private:
      ItemCommonProperty(const ItemCommonProperty<T>&);               //Not Allowed    
      ItemCommonProperty<T>& operator=(const ItemCommonProperty<T>&); //Not Allowed
      void BuildIconSymbol();

    protected:
      ItemCommonProperty();
      virtual ~ItemCommonProperty();
    
    private:
      const std::string strType_;
      QIcon iconSymbol_;
      

   
    };

    

    template<class T>
    inline  
    ItemCommonProperty<T>::ItemCommonProperty()
      :CommonProperty(),
       strType_(T::GetStrType()),
       //iconSymbol_(":/Rsc/Img/" + QString(strType_.c_str()) + ".png")
       iconSymbol_()
    {
      CommonProperty::AddClass(strType_, *this);
      BuildIconSymbol();
    }
    
    template<class T>
    inline  
    ItemCommonProperty<T>::~ItemCommonProperty()
    {
     
    }
      
    template<class T>
    inline void
    ItemCommonProperty<T>::BuildIconSymbol()
    {
      //Specialize this !
    }


    template<class T>
    inline const std::string&
    ItemCommonProperty<T>::GetStrType() const
    {
      return strType_;
    }

    template<class T>
    inline const QString
    ItemCommonProperty<T>::GetQStrType() const
    {
      return T::GetQStrType();
    }

    template<class T>
    inline const QIcon&
    ItemCommonProperty<T>::GetIconSymbol() const
    {
      return iconSymbol_;
    }

    template<class T>
    inline typename T::Dialog& 
    ItemCommonProperty<T>::GetDialog()
    {
      return Core::Singleton< Dialog >::Get();

    }

  
  }
}


#endif
