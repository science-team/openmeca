// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@yakuru.fr>
//
// Copyright (C) 2012-2017 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Core_SelectionManager_hpp
#define OpenMeca_Core_SelectionManager_hpp

#include <QObject>

#include "OpenMeca/Core/Singleton.hpp"
#include "OpenMeca/Core/Item.hpp"
#include "OpenMeca/Util/Color.hpp"

namespace OpenMeca
{
  namespace Core
  {


    // SelectionManager class that manages and stores the items selected by the user.
    class SelectionManager :  public QObject, public Singleton<SelectionManager>
    {
      Q_OBJECT
      friend class Singleton<SelectionManager>;

    public:    
      void SetItemSelected(Item&);
      void SetDrawableItemSelected(int);
      void SetNoItemSelected();
      bool IsItemSelected();
      Item& GetItemSelected();

      const Util::Color& GetSelectedColor() const;
    
    signals:
      void ItemSelected(Core::Item&);

    private:
      SelectionManager();
      virtual ~SelectionManager();
      Item& GetItem();
      SelectionManager(const SelectionManager&);            //Not Allowed    
      SelectionManager& operator=(const SelectionManager&); //Not Allowed

    private:
      Item* itemSelected_;
      Util::Color selectedColor_;
            
    };

  }
}
 
#endif
