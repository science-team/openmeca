// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@yakuru.fr>
//
// Copyright (C) 2012-2017 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Core_SystemSettingCommonProperty_hpp
#define OpenMeca_Core_SystemSettingCommonProperty_hpp


#include <QString>
#include <QIcon>

#include "OpenMeca/Core/ItemCommonProperty.hpp"
#include "OpenMeca/Core/Action/ActionEditSystemSetting.hpp"
#include "OpenMeca/Core/Action/ActionWithoutSelection.hpp"
#include "OpenMeca/Util/Color.hpp"


namespace OpenMeca
{
  namespace Core
  {

    // Common property for the classes that inherit from the 'SystemSetting' class
    template<class T>
    class SystemSettingCommonProperty : public ItemCommonProperty<T>, 
					public Singleton< SystemSettingCommonProperty<T> >
    {
      friend class Singleton< SystemSettingCommonProperty<T> >;

    public:
      void RegisterAction();
      void RegisterAction_Specialized();//Specialize this to add specific item
      

    protected:
      SystemSettingCommonProperty();
      virtual ~SystemSettingCommonProperty();
    
    private:
      SystemSettingCommonProperty(const SystemSettingCommonProperty<T>&);               //Not Allowed    
      SystemSettingCommonProperty<T>& operator=(const SystemSettingCommonProperty<T>&); //Not Allowed

    
    };

   

    template<class T>
    inline  
    SystemSettingCommonProperty<T>::SystemSettingCommonProperty()
      :ItemCommonProperty<T>()
    {
      
      
    }
    
    template<class T>
    inline  
    SystemSettingCommonProperty<T>::~SystemSettingCommonProperty()
    {
      
    }
  
    template<class T>
    inline void 
    SystemSettingCommonProperty<T>::RegisterAction()
    {
      Action& edit = 
	*new ActionWithoutSelection<T, ActionEditSystemSetting<T> >();
      ItemCommonProperty<T>::AddAction(edit);
      CommonProperty::AddPopUpAction(edit);
    }
    
    template<class T>
    inline void 
    SystemSettingCommonProperty<T>::RegisterAction_Specialized() 
    {
      
    }
  
  }
}


#endif
