// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@yakuru.fr>
//
// Copyright (C) 2012-2017 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include <QTextStream>
#include <QDebug>

#include "OpenMeca/Core/XmlConfigFile.hpp"
#include "OpenMeca/Core/ConfigDirectory.hpp"
#include "OpenMeca/Core/Macro.hpp"


namespace OpenMeca
{
  namespace Core
  {

    std::string 
    XmlConfigFile::GetDirectoryName()
    {
      return std::string("Xml");
    }
    
    std::string 
    XmlConfigFile::GetSuffix()
    {
      return std::string(".xml");
    }


    XmlConfigFile::XmlConfigFile(const std::string name)
      :ConfigFile(name + GetSuffix(), GetDirectoryName()),
       name_(name)
    {
      CheckFileVersion();
    }

    XmlConfigFile::~XmlConfigFile()
    {
    }

    void 
    XmlConfigFile::CheckFileVersion()
    {
      const Util::Version localVersion = GetLocalFileVersion();
      const Util::Version softVersion  = GetSoftFileVersion();

      if (localVersion < softVersion)
	{
	  QString msg = "";
	  msg += QObject::tr("The version of your local configuration file \"") + " ";
	  msg += QString(name_.c_str()) + "\" ";
	  msg += QObject::tr("is obsolete") + ". ";
	  msg += QObject::tr("OpenMeca will replace it") + ".";
	  OMC_INFO_MSG(msg);
	  RestoreBackup();
	}
    }

    Util::Version 
    XmlConfigFile::GetLocalFileVersion()
    {
      QFile& file = ConfigFile::Open();
      return ReadFileVersion(file, name_);
    }
    
    Util::Version 
    XmlConfigFile::GetSoftFileVersion()
    {
      QFile file(":/Rsc/Xml/" + QString(name_.c_str()) + ".xml");
      
      OMC_ASSERT_MSG(file.open(QIODevice::ReadOnly | QIODevice::Text)==true,
		     "Can't open the \"" + file.fileName().toStdString() + "\" config file because : " + file.errorString().toStdString());
      
      Util::Version v = ReadFileVersion(file, name_);
      return v;
    }

    void
    XmlConfigFile::SaveXml(QDomDocument& doc)
    {
      QDir dir = GetConfigFileDirectory();
      const QString filePath = GetAbsPath();
      if (QFile::exists(filePath)==false)
	{
	  file_.setFileName(filePath);
	  file_.remove();
	}
      file_.close();
      file_.setFileName(filePath);
      OMC_ASSERT_MSG(file_.open(QIODevice::WriteOnly), "Can't write config file");
      QTextStream TextStream(&file_);
      doc.save(TextStream, 0);
      file_.close();
    }
    

    Util::Version 
    XmlConfigFile::ReadFileVersion(QFile& file, const std::string& xmlTagName)
    {
      QDomDocument doc("");
      QString errorMsg = "";
      OMC_ASSERT_MSG(doc.setContent(&file, true, &errorMsg), 
		     "Can't set content of the xml doc " + file.fileName().toStdString() + " error is :" + errorMsg.toStdString());
      QDomNode n = doc.firstChild();
      while (!n.isNull()) 
	{
	  if (n.isElement()) 
	    {
	      QDomElement e = n.toElement();
	      if (e.tagName()==xmlTagName.c_str())
		{
		  if (e.hasAttribute("Version"))
		    {
		      const std::string v = e.attribute ("Version").toStdString();
		      return Util::Version(v);
		    }
		}
	    }
	  n = n.nextSibling();
	}
      return Util::Version("0.0.0");
    }

  }
}




