// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@yakuru.fr>
//
// Copyright (C) 2012-2017 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Core_XmlConfigFile_hpp
#define OpenMeca_Core_XmlConfigFile_hpp

#include <QDomDocument>

#include "OpenMeca/Core/ConfigFile.hpp"
#include "OpenMeca/Util/Version.hpp"

namespace OpenMeca
{
  namespace Core
  {


    // Base class that manages Xml config files used to save/load  global settings.
    class XmlConfigFile : public ConfigFile
    {
    
    public:
      static std::string GetSuffix(); 
      
      XmlConfigFile(const std::string);      
      ~XmlConfigFile(); 

      void SaveXml(QDomDocument& doc);
      void CheckFileVersion();
      Util::Version GetLocalFileVersion();
      Util::Version GetSoftFileVersion();


      
    private:
      XmlConfigFile();                                //Not Allowed    
      XmlConfigFile(const XmlConfigFile&);            //Not Allowed    
      XmlConfigFile& operator=(const XmlConfigFile&); //Not Allowed

    private:
      static std::string GetDirectoryName(); 
      static Util::Version ReadFileVersion(QFile& file, const std::string& xmlTagName);

    private:
      const std::string name_;

    };
  }
}


#endif
