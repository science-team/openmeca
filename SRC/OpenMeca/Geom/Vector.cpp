// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@yakuru.fr>
//
// Copyright (C) 2012-2017 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


// This source file was inspired of the "libGeometrical" from 
// the GranOO workbench : http://www.granoo.org and
// the qglviewer library : http://www.libqglviewer.com


#include "OpenMeca/Geom/Vector.hpp" 
#include "OpenMeca/Geom/Frame.hpp"


namespace OpenMeca
{
  namespace Geom 
  {
    template<> 
    chrono::ChVector<double> 
    Vector<_3D>::ToChVector() const
    {
      const Vector<_3D>& me = *this;
      return chrono::ChVector<double>(me[0], me[1], me[2]);
    }

    template<> 
    qglviewer::Vec
    Vector<_3D>::ToQGLVector() const
    {
      const Vector<_3D>& me = *this;
      return qglviewer::Vec(me[0], me[1], me[2]);
    }


    template<>
    void 
    Vector<_3D>::ProjectOnAxis(const Vector<_3D> & direction)
    {
      const double dirSquaredNorm = direction.GetSquaredNorm();
      if ( dirSquaredNorm < 1.0E-10)
	{
	  std::cerr << "Vector<_3D>::ProjectOnAxis: axis direction is not normalized (norm=" << direction.GetNorm() << ")" << std::endl;
	}
      *this = (((*this)*direction) / dirSquaredNorm) * direction;
    }

    template<>
    void 
    Vector<_3D>::ProjectOnPlane(const Vector<_3D> & normal)
    {
      const double normSquaredNorm = normal.GetSquaredNorm();
      if ( normSquaredNorm < 1.0E-10)
	{
	  std::cerr << "Vector<_3d>::ProjectOnPlane: plane normal is not normalized (norm=" << normal.GetNorm() <<")" << std::endl;
	}
      *this -= (((*this)*normal) / normSquaredNorm) * normal;
    }
    
    template<>
    Vector<_3D> 
    Vector<_3D>::GetOrthogonalVector() const
    {
      // Find smallest component. Keep equal case for null values.
      if ((fabs(coord_[1]) >= 0.9*fabs(coord_[0])) && (fabs(coord_[2]) >= 0.9*fabs(coord_[0])))
	return Vector<_3D>(0.0, -coord_[2], coord_[1], GetFrameFunctionAccess());
      else
	if ((fabs(coord_[0]) >= 0.9*fabs(coord_[1])) && (fabs(coord_[2]) >= 0.9*fabs(coord_[1])))
	  return Vector<_3D>(-coord_[2], 0.0, coord_[0], GetFrameFunctionAccess());
	else
	  return Vector<_3D>(-coord_[1], coord_[0], 0.0, GetFrameFunctionAccess());
    }

    template<>
    Vector<_3D> &
    Vector<_3D>::operator= (const chrono::ChVector<double>& v)
    {
      
      coord_[0] = v.x;
      coord_[1] = v.y;
      coord_[2] = v.z;
      return *this;
    }


    template<>
    void
    Vector<_3D>::Draw(const double scale) const
    {
      const Geom::Vector<_3D> vec1 = (*this) * scale;
      const Geom::Vector<_3D> vec2 = vec1.GetOrthogonalVector().Unit();
      const Geom::Vector<_3D> vec3 = (vec1^ vec2).Unit();

      glLineWidth(1.0f);
      glBegin(GL_LINES);
      glVertex3f(0., 0., 0.);
      glVertex3dv(vec1.GetPtr());
      glEnd();

      Geom::Vector<_3D> vecArrow1;
      Geom::Vector<_3D> vecArrow2;
      float r = 0.02 * vec1.GetNorm();
      float l = 0.1  * vec1.GetNorm();
      for (double i=0; i<=2*M_PI; i=i+0.2)
	{
	  glBegin(GL_TRIANGLE_STRIP);
	  vecArrow1 = - l*vec1.Unit() + r*vec2.Unit()*cos(i)     + r*vec3.Unit()*sin(i) ;
	  vecArrow2 = - l*vec1.Unit() + r*vec2.Unit()*cos(i+0.2) + r*vec3.Unit()*sin(i+0.2);
	  glVertex3dv(vec1.GetPtr());
	  glVertex3dv((vec1 + vecArrow1).GetPtr());
	  glVertex3dv((vec1 + vecArrow2).GetPtr());
	  glEnd();
	}
      glPopMatrix();
    }


  } // namespace Geom
}
