// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@yakuru.fr>
//
// Copyright (C) 2012-2017 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Gui_AbstractTreeItem_hpp
#define OpenMeca_Gui_AbstractTreeItem_hpp

#include <QTreeWidgetItem>
#include <QIcon>

#include "OpenMeca/Core/AutoRegister.hpp"


namespace OpenMeca
{
  namespace Core
  {
    class Item;
  }
  

  namespace Gui
  {

    class TreeView;
    
    class AbstractTreeItem : public QTreeWidgetItem, public Core::AutoRegister<AbstractTreeItem>
    {
    
    public:
      static void SetTreeView(TreeView&);
      static TreeView& GetTreeView();

      AbstractTreeItem(QTreeWidgetItem* parent);
      virtual ~AbstractTreeItem();

      void Update();
      virtual Core::Item& GetItem() = 0;
    
      void RelatedItemIsDeleted();
      bool IsRelatedItemIsDeleted() const;

    private:
      AbstractTreeItem();                        //Not allowed
      AbstractTreeItem(const AbstractTreeItem&);             //Not Allowed
      AbstractTreeItem& operator=(const AbstractTreeItem&);  //Not Allowed

    private:
      static TreeView* treeView_;
      bool relatedItemIsDeleted_;

    };
  }
}
#endif
