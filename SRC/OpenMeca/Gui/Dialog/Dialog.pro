## This file is part of OpenMeca, an easy software to do mechanical simulation.
##
## Author(s)    :  - Damien ANDRE  <openmeca@yakuru.fr>
##
## Copyright (C) 2012-2017 Damien ANDRE
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

include(./Shape/Shape.pro)
include(./Physic/Physic.pro)

FORMS   += Gui/Dialog/DialogUserItem.ui \
           Gui/Dialog/DialogCheckDeleteItem.ui \
           Gui/Dialog/DialogSimulation.ui

HEADERS += Gui/Dialog/Dialog.hpp \
           Gui/Dialog/DialogUserItem.hpp \
           Gui/Dialog/DialogNone.hpp \
           Gui/Dialog/DialogBody.hpp \
           Gui/Dialog/DialogLinkT.hpp \
           Gui/Dialog/DialogPartUserJunction.hpp \
           Gui/Dialog/DialogPartUserPoint.hpp \
           Gui/Dialog/DialogSystemSetting.hpp \
           Gui/Dialog/DialogCheckDeleteItem.hpp \
           Gui/Dialog/DialogLink.hpp \
           Gui/Dialog/DialogLinkMotor.hpp \
           Gui/Dialog/DialogLinkLinearMotor.hpp \
           Gui/Dialog/DialogLinkScrew.hpp \
           Gui/Dialog/DialogLinkGear.hpp \
           Gui/Dialog/DialogLinkPulley.hpp \
           Gui/Dialog/DialogLinkRackPinion.hpp \
           Gui/Dialog/DialogLinkSpring.hpp \
           Gui/Dialog/DialogPartUserPipe.hpp \
           Gui/Dialog/DialogPartUserShapeT.hpp \
           Gui/Dialog/DialogSimulation.hpp \
           Gui/Dialog/DialogScales.hpp \
           Gui/Dialog/DialogGravity.hpp \
           Gui/Dialog/DialogScene.hpp \
           Gui/Dialog/DialogSystemSettingT.hpp \
           Gui/Dialog/DialogUserItemT.hpp \
           Gui/Dialog/DialogSensorT.hpp \
           Gui/Dialog/DialogLoadT.hpp \
           Gui/Dialog/DialogVariable.hpp
           

SOURCES += Gui/Dialog/Dialog.cpp \
           Gui/Dialog/DialogBody.cpp \
           Gui/Dialog/DialogUserItem.cpp \
           Gui/Dialog/DialogCheckDeleteItem.cpp \
           Gui/Dialog/DialogLink.cpp \
           Gui/Dialog/DialogLinkMotor.cpp \
           Gui/Dialog/DialogLinkLinearMotor.cpp \
           Gui/Dialog/DialogLinkScrew.cpp \
           Gui/Dialog/DialogLinkGear.cpp \
           Gui/Dialog/DialogLinkPulley.cpp \
           Gui/Dialog/DialogLinkRackPinion.cpp \
           Gui/Dialog/DialogLinkSpring.cpp \
           Gui/Dialog/DialogPartUserJunction.cpp \
           Gui/Dialog/DialogPartUserPipe.cpp \
           Gui/Dialog/DialogPartUserPoint.cpp \
           Gui/Dialog/DialogSimulation.cpp \
           Gui/Dialog/DialogScales.cpp \
           Gui/Dialog/DialogGravity.cpp \
           Gui/Dialog/DialogScene.cpp \
           Gui/Dialog/DialogSystemSetting.cpp \           
           Gui/Dialog/DialogVariable.cpp

