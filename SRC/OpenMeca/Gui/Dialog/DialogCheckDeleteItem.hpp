// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@yakuru.fr>
//
// Copyright (C) 2012-2017 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Gui_DialogCheckDeleteItem_hpp 
#define OpenMeca_Gui_DialogCheckDeleteItem_hpp 

#include <QDialog>

#include "ui_DialogCheckDeleteItem.h"
#include "OpenMeca/Core/SetOfBase.hpp"

namespace OpenMeca
{
   namespace Core
   {
     class UserItem;
   }

  namespace Gui
  {

    class DialogCheckDeleteItem : public QDialog, private Ui::DialogCheckDeleteItem
    {
      Q_OBJECT
      
      public:
      static DialogCheckDeleteItem& Get();
      
      virtual ~DialogCheckDeleteItem();

      void BuildListView(const Core::SetOfBase<Core::UserItem>&);

    private:
      DialogCheckDeleteItem();

    private:
      static DialogCheckDeleteItem* me_;

    };

  }
}

#endif
