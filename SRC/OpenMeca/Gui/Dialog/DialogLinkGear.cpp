// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@yakuru.fr>
//
// Copyright (C) 2012-2017 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "OpenMeca/Gui/Dialog/DialogLinkGear.hpp"
#include "OpenMeca/Item/Link/Gear.hpp"
#include "OpenMeca/Item/LinkT.hpp"


namespace OpenMeca
{
  namespace Gui
  {

  
    DialogLinkGear::DialogLinkGear()
      :DialogLinkT<Item::LinkT<Item::Gear> >(),
       ratio_(this),
       interAxisLength_(this),
       modulus_(this),
       angleOfAction_(this),
       internalTeeth_(this)
    {
      ratio_.SetLabel(QObject::tr("Ratio"));
      ratio_.SetDimension(Util::Dimension::Get("Null"));

      interAxisLength_.SetLabel(QObject::tr("Length between axes"));
      interAxisLength_.SetDimension(Util::Dimension::Get("Length"));

      modulus_.SetLabel(QObject::tr("Modulus"));
      modulus_.SetDimension(Util::Dimension::Get("Length"));
      
      angleOfAction_.SetLabel(QObject::tr("Angle of action"));
      angleOfAction_.SetDimension(Util::Dimension::Get("Angle"));

      internalTeeth_.SetLabel(QObject::tr("Internal teeth"));

      GetPropTree().Add(ratio_);
      GetPropTree().Add(interAxisLength_);
      GetPropTree().Add(modulus_);
      GetPropTree().Add(angleOfAction_);
      GetPropTree().Add(internalTeeth_);
    }

    
    DialogLinkGear::~DialogLinkGear()
    {
    }

    void
    DialogLinkGear::Init()
    {
      DialogLinkT<Item::LinkT<Item::Gear> >::Init();
      ratio_.SetValue(GetCurrentItem().GetLinkType().GetRatio());
      interAxisLength_.SetValue(GetCurrentItem().GetLinkType().GetInterAxisLength());
      modulus_.SetValue(GetCurrentItem().GetLinkType().GetModulus());
      angleOfAction_.SetValue(GetCurrentItem().GetLinkType().GetAngleOfAction());
      internalTeeth_.SetValue(GetCurrentItem().GetLinkType().GetInternalTeeth());
    }


    bool
    DialogLinkGear::Check()
    {   
      bool check = DialogLinkT<Item::LinkT<Item::Gear> >::Check();
      if (internalTeeth_.GetCopy() == true && ratio_.GetCopy() == 1.)
	{
	  const QString help = QObject::tr("You can't set a ratio of 100% with internal teeth gear");
	  ratio_.DisplayHelp(help);
	  check = false;
	}

      return check;
    }


  }
}
