// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@yakuru.fr>
//
// Copyright (C) 2012-2017 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "OpenMeca/Gui/Dialog/DialogLinkRackPinion.hpp"
#include "OpenMeca/Item/Link/RackPinion.hpp"
#include "OpenMeca/Item/LinkT.hpp"


namespace OpenMeca
{
  namespace Gui
  {

  
    DialogLinkRackPinion::DialogLinkRackPinion()
      :DialogLinkT<Item::LinkT<Item::RackPinion> >(),
       ratio_(this),
       pinionRadius_(this),
       modulus_(this),
       angleOfAction_(this)
    {
      ratio_.SetLabel(QObject::tr("Ratio"));
      ratio_.SetDimension(Util::Dimension::Get("Null"));

      pinionRadius_.SetLabel(QObject::tr("Pinion radius"));
      pinionRadius_.SetDimension(Util::Dimension::Get("Length"));

      modulus_.SetLabel(QObject::tr("Modulus"));
      modulus_.SetDimension(Util::Dimension::Get("Length"));
      
      angleOfAction_.SetLabel(QObject::tr("Angle of action"));
      angleOfAction_.SetDimension(Util::Dimension::Get("Angle"));

      GetPropTree().Add(ratio_);
      GetPropTree().Add(pinionRadius_);
      GetPropTree().Add(modulus_);
      GetPropTree().Add(angleOfAction_);
    }

    
    DialogLinkRackPinion::~DialogLinkRackPinion()
    {
    }

    void
    DialogLinkRackPinion::Init()
    {
      DialogLinkT<Item::LinkT<Item::RackPinion> >::Init();
      ratio_.SetValue(GetCurrentItem().GetLinkType().GetRatio());
      pinionRadius_.SetValue(GetCurrentItem().GetLinkType().GetPinionRadius());
      modulus_.SetValue(GetCurrentItem().GetLinkType().GetModulus());
      angleOfAction_.SetValue(GetCurrentItem().GetLinkType().GetAngleOfAction());
    }


    bool
    DialogLinkRackPinion::Check()
    {      
      return DialogLinkT<Item::LinkT<Item::RackPinion> >::Check();
    }


  }
}
