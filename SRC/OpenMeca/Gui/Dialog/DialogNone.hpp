// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@yakuru.fr>
//
// Copyright (C) 2012-2017 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Gui_DialogNone_hpp
#define OpenMeca_Gui_DialogNone_hpp

#include "OpenMeca/Gui/Dialog/DialogUserItemT.hpp"
#include "OpenMeca/Core/Singleton.hpp"


namespace OpenMeca
{
  namespace Gui
  {
    template <class T>
    class DialogNone : public DialogUserItemT<T>,  public Core::Singleton<DialogNone <T> >
    {
      
    public:
      DialogNone() {OMC_ASSERT_MSG(0, "Invoking this method is not allowed");}
      ~DialogNone(){OMC_ASSERT_MSG(0, "Invoking this method is not allowed");}

    private:
      void Init(){OMC_ASSERT_MSG(0, "Invoking this method is not allowed");}
    };

  }
}
#endif
