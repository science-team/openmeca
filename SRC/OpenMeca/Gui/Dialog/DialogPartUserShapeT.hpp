// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@yakuru.fr>
//
// Copyright (C) 2012-2017 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Gui_DialogPartUserShapeT_hpp
#define OpenMeca_Gui_DialogPartUserShapeT_hpp

#include "OpenMeca/Gui/Dialog/DialogUserItemT.hpp"

#include "OpenMeca/Core/Singleton.hpp"
#include "OpenMeca/Core/AutoRegisteredPtr.hpp"

#include "OpenMeca/Gui/Prop/PropString.hpp"
#include "OpenMeca/Gui/Prop/PropPoint.hpp"
#include "OpenMeca/Gui/Prop/PropBool.hpp"
#include "OpenMeca/Gui/Prop/PropSelectItemT.hpp"



namespace OpenMeca
{
  namespace Item
  {
    template <class T> class PartUserShapeT;
    class PartUserPoint;
  }
}

namespace OpenMeca
{
  namespace Gui
  {

    template <class T>
    class DialogPartUserShapeT : public DialogUserItemT<Item::PartUserShapeT<T> >, 
				 public Core::Singleton<DialogPartUserShapeT<T> >
    {
      
    public:
      DialogPartUserShapeT();
      ~DialogPartUserShapeT();


    private:
      void Init();

    private:
      PropString id_;
      PropSelectItemT< Core::AutoRegisteredPtr<Core::UserItem, Item::PartUser > > parent_;
      PropBool collision_;
      typename T::GuiManager dim_;
    };

    template <class T>
    inline
    DialogPartUserShapeT<T>::DialogPartUserShapeT()
      :DialogUserItemT<Item::PartUserShapeT<T> >(),
       id_(this),
       parent_(this),
       collision_(this),
       dim_(this)
    {
      id_.SetLabel(QObject::tr("Name"));
      parent_.Prop::SetLabel(QObject::tr("Point"));
      collision_.SetLabel(QObject::tr("Enable collision detection"));


      // Init table
      DialogUserItem::GetPropTree().Add(id_);
      DialogUserItem::GetPropTree().Add(parent_);
      if (T::CollisionDetectionAllowed)
	DialogUserItem::GetPropTree().Add(collision_);

      dim_.Add(DialogUserItem::GetPropTree());
    }
  
    template <class T>
    inline
    DialogPartUserShapeT<T>::~DialogPartUserShapeT()
    {
    }

    template <class T>
    inline void
    DialogPartUserShapeT<T>::Init()
    {
      Item::PartUserShapeT<T>& currentItem = 
	DialogUserItemT<Item::PartUserShapeT<T> >::GetCurrentItem();

      id_.SetValue(currentItem.GetName());
      collision_.SetValue(currentItem.GetCollisionState());

      // populate parent list
      Core::SetOfBase<OpenMeca::Item::PartPoint>& points = 
	Core::System::Get().GetSetOf<OpenMeca::Item::PartPoint>();
      
      Core::SetOfBase<Core::UserItem> set;
      for (unsigned int i = 0; i < points.GetTotItemNumber(); ++i)
	{
    OpenMeca::Core::UserItem &item = (OpenMeca::Core::UserItem &)points(i);
	  if (!currentItem.HasChild(item))
	    set.AddItem(const_cast<Core::UserItem&>(item));
	}
      OMC_ASSERT_MSG(set.GetTotItemNumber() > 0, "The number of user item is null");
      parent_.SetList(set);
      
      
      OMC_ASSERT_MSG(set.GetTotItemNumber() > 0, "The number of user item is null");
      parent_.SetList(set);
      parent_.SetValue(currentItem.GetParentItemPtr());

      dim_.Init(currentItem.GetShape());
    }

  }
}
#endif
