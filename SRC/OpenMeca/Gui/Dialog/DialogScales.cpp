// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@yakuru.fr>
//
// Copyright (C) 2012-2017 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include <iostream>


#include "OpenMeca/Setting/Scales.hpp"
#include "OpenMeca/Core/System.hpp"
#include "OpenMeca/Gui/MainWindow.hpp"
#include "OpenMeca/Gui/Dialog/DialogScales.hpp"


namespace OpenMeca
{
  namespace Gui
  {
    DialogScales::DialogScales()
      :DialogUserItemT<Setting::Scales>(),
       propScales_()
    {
      Setting::Scales& scales =  Core::SystemSettingT<Setting::Scales>::Get();
      std::vector<std::string> keys =  scales.GetKeys();

      for (unsigned int i = 0; i < keys.size(); ++i)
	{
	  
	  const std::string scaleKey = keys[i];
	  PropDouble* prop = new PropDouble(this);
	  prop->SetLabel(QObject::tr(keys[i].c_str()));
	  prop->AddCondition(new Core::Superior<double>(0.));
	  prop->SetDimension(Util::Dimension::Get("Percent"));
	  GetPropTree().Add(*prop);

	  OMC_ASSERT_MSG(propScales_.count(scaleKey) == 0, "Can't find this scale key");
	  propScales_[scaleKey] = prop;
	}
    }

    DialogScales::~DialogScales()
    {
      std::map<std::string, PropDouble*>::iterator it;
      for (it=propScales_.begin(); it!=propScales_.end(); ++it)
	{
	  delete it->second;
	}
    }

    void
    DialogScales::Init()
    {
      std::map<std::string, PropDouble*>::iterator it;
      for (it=propScales_.begin(); it!=propScales_.end(); ++it)
	{
	  PropDouble* prop = it->second;
	  const std::string key = it->first;
	  prop->SetValue(GetCurrentItem().GetScaleValue(key));
	}
    }

  

  }
}
