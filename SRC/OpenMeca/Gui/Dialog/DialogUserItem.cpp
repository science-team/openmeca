// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@yakuru.fr>
//
// Copyright (C) 2012-2017 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <QShowEvent>
#include <QMessageBox>

#include "OpenMeca/Gui/MainWindow.hpp"
#include "OpenMeca/Gui/Dialog/DialogUserItem.hpp"
#include "OpenMeca/Gui/Prop/Prop.hpp"
#include "OpenMeca/Core/UserItem.hpp"
#include "OpenMeca/Core/System.hpp"

namespace OpenMeca
{
  namespace Gui
  {

   

    DialogUserItem::DialogUserItem()
      :Dialog(),
       newItemMode_(false)
    {
      Ui::DialogUserItem::setupUi(this);
      QObject::connect(preview_, SIGNAL(clicked()), this, SLOT(Preview()));
      QObject::connect(cancel_ , SIGNAL(clicked()), this, SLOT(Cancel()));
      QObject::connect(ok_     , SIGNAL(clicked()), this, SLOT(Ok()));
    }

    
    DialogUserItem::~DialogUserItem()
    {
    }


    void 
    DialogUserItem::Preview()
    {
      if (!IsCurrentUserItemStillExist())
	{
	  ManageUserItemNotExist();
	  return;
	}

      if (Check())
	{
	  ApplyChangement();
	  Core::System::Get().Update();
	}
    }
    
    void DialogUserItem::Ok()
    {
      if (!IsCurrentUserItemStillExist())
	{
	  ManageUserItemNotExist();
	  return;
	}

      if (Check())
	{
	  ApplyChangement();
	  QWidget::hide();
	  Dialog::Hide_CallBack();
	  Core::System::Get().Update();
	  Core::System::Get().GetHistoric().SystemEdited();
	  Dialog::AddActionToHistoric();
	  Reset();
	}
    }
    
    void DialogUserItem::Cancel()
    {
      CancelChangement();
      QWidget::hide();
      Dialog::Hide_CallBack();
      if (IsCurrentUserItemStillExist())
	{
	  if (newItemMode_)
	    delete &GetCurrentUserItem();
	  Core::System::Get().Update();
	}
      Reset();
    }

    
    void DialogUserItem::showEvent(QShowEvent* event)
    {
      QWidget::showEvent(event);
    }

    void
    DialogUserItem::ApplyChangement()
    {
      OMC_ASSERT_MSG(IsCurrentUserItemStillExist() == true, 
		 "The current item no longer exist");
      Dialog::ApplyChangement();
    }

    void 
    DialogUserItem::DisplayWarningUserItemNotExist()
    {
      QMessageBox::warning(this, tr("OpenMeca"), tr("The current edited item was deleted.\n"
						    "The dialog will be closed"));
    }

    void 
    DialogUserItem::ManageUserItemNotExist()
    {
      DisplayWarningUserItemNotExist();
      QWidget::hide();
      Dialog::Hide_CallBack();
      Reset();
    }


    void 
    DialogUserItem::Reset()
    {
      Dialog::Reset();
      newItemMode_ = false;
    }

    bool 
    DialogUserItem::IsNewItemAllowed() const
    {
      return true;
    }




    void 
    DialogUserItem::ActiveNewItemMode()
    {
      newItemMode_ = true;
    }
    
    void 
    DialogUserItem::DisableNewItemMode()
    {
      newItemMode_ = false;
    }
    

  }
}
