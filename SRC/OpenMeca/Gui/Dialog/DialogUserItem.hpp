// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@yakuru.fr>
//
// Copyright (C) 2012-2017 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef OpenMeca_Gui_Dialog_DialogUserItem_hpp_1 
#define OpenMeca_Gui_Dialog_DialogUserItem_hpp_1 

#include <QWidget>

#include "ui_DialogUserItem.h"
#include "OpenMeca/Gui/Dialog/Dialog.hpp"

namespace OpenMeca
{
  namespace Core
  {
    class UserItem;
  }

  namespace Gui
  {


    class DialogUserItem : public Dialog, 
			   private Ui::DialogUserItem
    {
      Q_OBJECT
      
    public slots:
      void Preview();
      void Ok();
      void Cancel();


    public:
      PropTree& GetPropTree(){return *tree_;}

     protected:
      DialogUserItem();
      virtual ~DialogUserItem();
      
      virtual void showEvent (QShowEvent* event );
      virtual void Init() = 0;
      virtual Core::UserItem& GetCurrentUserItem() = 0;
      virtual bool IsCurrentUserItemStillExist() = 0;
      virtual void Reset();
      virtual bool IsNewItemAllowed() const; //return true by default
      virtual void ApplyChangement();

      void ActiveNewItemMode();
      void DisableNewItemMode();
      
    private:
      
      void DisplayWarningUserItemNotExist();
      void ManageUserItemNotExist();

    private:
      bool newItemMode_;
    };

  }
}

#endif
