// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@yakuru.fr>
//
// Copyright (C) 2012-2017 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "OpenMeca/Gui/Dialog/DialogVariable.hpp"
#include "OpenMeca/Gui/MainWindow.hpp"
#include "OpenMeca/Item/Variable.hpp"
#include "OpenMeca/Util/Dimension.hpp"

namespace OpenMeca
{
  namespace Gui
  {
  
    DialogVariable::DialogVariable()
      :DialogUserItemT<Item::Variable>(),
       symbol_(this),
       value_(this)
    {
      symbol_.SetLabel(QObject::tr("Symbol"));
      
      value_.SetDimension(Util::Dimension::Get("Null"));
      value_.SetLabel(QObject::tr("Value"));

      // Init table
      GetPropTree().Add(symbol_);
      GetPropTree().Add(value_);
    }
  
    DialogVariable::~DialogVariable()
    {
    }

    void
    DialogVariable::Init()
    {
      // Equalize symbol and name
      //GetCurrentItem().GetSymbol() = GetCurrentItem().GetName();
      symbol_.SetValue(GetCurrentItem().GetSymbol());
      value_.SetValue(GetCurrentItem().GetValue());
    }

    void
    DialogVariable::ApplyChangement()
    {
      DialogUserItemT<Item::Variable>::ApplyChangement();
      
      // Equalize symbol and name
      const std::string name = GetCurrentItem().GetSymbol() + " = " + 
	value_.GetInputString().toStdString();
      GetCurrentItem().GetName() = name;
    }

    void
    DialogVariable::CancelChangement()
    {
      DialogUserItemT<Item::Variable>::CancelChangement();
      
      // Equalize symbol and name
      const std::string name = GetCurrentItem().GetSymbol() + " = " + 
	value_.GetInputString().toStdString();
      GetCurrentItem().GetName() = name;
    }
    
    bool 
    DialogVariable::Check()
    {
      if (!DialogUserItemT<Item::Variable>::Check())
	return false;
      
      Util::Var& var = GetCurrentItem();
      const std::string initialSymbol = var.GetSymbol();
      var.GetSymbol() = symbol_.GetCopy();
      if (var.IsDoublon())
	{
	  symbol_.DisplayHelp(QObject::tr("The name of the variable is already used"));
	  var.GetSymbol() = initialSymbol;
	  return false;
	}

      if (!var.IsValid())
	{
	  symbol_.DisplayHelp(QObject::tr("It's not a valid variable name"));
	  var.GetSymbol() = initialSymbol;
	  return false;
	}
      
      var.GetSymbol() = initialSymbol;
      return true;
    }
    
   

  
  }
}
