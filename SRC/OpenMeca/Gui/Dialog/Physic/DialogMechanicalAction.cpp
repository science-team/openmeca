// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@yakuru.fr>
//
// Copyright (C) 2012-2017 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "OpenMeca/Physic/MechanicalAction.hpp"
#include "OpenMeca/Gui/Dialog/Physic/DialogMechanicalAction.hpp"
#include "OpenMeca/Gui/Prop/PropTree.hpp"


namespace OpenMeca
{
  namespace Gui
  {
    namespace Physic
    {
    
      DialogMechanicalAction::DialogMechanicalAction(QWidget* widget):
	value_(widget),
	direction_(widget)
      {
	value_.SetLabel(QObject::tr("Value"));
	direction_.SetLabel(QObject::tr("Direction coordinate"));
      }

      DialogMechanicalAction::~DialogMechanicalAction()
      {
      }
      
      void 
      DialogMechanicalAction::Add(Gui::PropTree& prop)
      {
	prop.Add(value_);
	prop.Add(direction_);
      }
      
      void 
      DialogMechanicalAction::Init(OpenMeca::Physic::MechanicalAction& f)
      {
	value_.SetValue(f.GetValue());
	direction_.SetValue(f.GetDirectionMode());
      }
    
    }
  }
}
