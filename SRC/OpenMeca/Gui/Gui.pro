## This file is part of OpenMeca, an easy software to do mechanical simulation.
##
## Author(s)    :  - Damien ANDRE  <openmeca@yakuru.fr>
##
## Copyright (C) 2012-2017 Damien ANDRE
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

include(./Widget/Widget.pro)
include(./Dialog/Dialog.pro)
include(./Prop/Prop.pro)

FORMS   += Gui/MainWindow.ui \
           Gui/MainPlotWindow.ui \
           Gui/ImageDialog.ui \
           Gui/Player.ui \
           Gui/GeneralSetting.ui

HEADERS += Gui/AbstractTreeItem.hpp \
           Gui/ExampleAction.hpp \
           Gui/MainTreeItem.hpp \
           Gui/MainWindow.hpp \
           Gui/HelpBrowser.hpp \
           Gui/MainPlotWindow.hpp \
           Gui/GeneralSetting.hpp \
           Gui/ImageDialog.hpp \
           Gui/Player.hpp \
           Gui/SecondaryTreeItem.hpp \
           Gui/RootTreeItemT.hpp \
           Gui/TreeView.hpp \
           Gui/Viewer.hpp

SOURCES += Gui/AbstractTreeItem.cpp \
           Gui/ExampleAction.cpp \
           Gui/MainWindow.cpp \
           Gui/Player.cpp \
           Gui/Viewer.cpp \
           Gui/MainTreeItem.cpp \
           Gui/GeneralSetting.cpp \
           Gui/HelpBrowser.cpp \
           Gui/MainPlotWindow.cpp \
           Gui/ImageDialog.cpp \
           Gui/SecondaryTreeItem.cpp \
           Gui/TreeView.cpp
