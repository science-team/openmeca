// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@yakuru.fr>
//
// Copyright (C) 2012-2017 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <QMenu>
#include <QMessageBox>
#include <qwt/qwt_legend.h>

#include "OpenMeca/Gui/MainPlotWindow.hpp"
#include "OpenMeca/Core/System.hpp"
#include "OpenMeca/Item/Physical.hpp"
#include "OpenMeca/Setting/Simulation.hpp"
#include "OpenMeca/Util/Dimension.hpp"

namespace OpenMeca
{
  namespace Gui
  {
  

    MainPlotWindow* MainPlotWindow::me_ = 0;

    MainPlotWindow& 
    MainPlotWindow::Get()
    {
      OMC_ASSERT_MSG(me_!=0, "The singleton pointer is null");
      return *me_;
    }

    MainPlotWindow::MainPlotWindow(QWidget * parent)
      :QMainWindow(parent),
       time_(Core::SystemSettingT<Setting::Simulation>::Get().GetTimeArray()),
       data_(),
       currentUnitPtr_(0)
    {
      OMC_ASSERT_MSG(me_ == 0, "The singleton pointer is not null");
      setupUi(this);
      me_ = this;

      // pop-up menu when right-click
      plot_->setContextMenuPolicy(Qt::CustomContextMenu);

      // legend
      QwtLegend *legend = new QwtLegend;
      legend->setFrameStyle(QFrame::Box|QFrame::Sunken);
      plot_->insertLegend(legend, QwtPlot::BottomLegend);
      
      // axis
      plot_->setAxisTitle(QwtPlot::xBottom, "Time (s)");
      

      // Signal & slots
      Setting::Simulation* simu = &Core::SystemSettingT<Setting::Simulation>::Get();
      QObject::connect(actionClear_, SIGNAL(triggered()), this, SLOT(Clear()));
      QObject::connect(simu, SIGNAL(StepChanged(OpenMeca::Setting::Simulation::Step)), 
		       this, SLOT(Update(OpenMeca::Setting::Simulation::Step)));
      QObject::connect(plot_, SIGNAL(customContextMenuRequested(const QPoint &)), 
		       this, SLOT(PopUpMenu(const QPoint &)));
    }
    
    MainPlotWindow::~MainPlotWindow()
    {
    }
    
    bool
    MainPlotWindow::SetUnit(const Util::Unit& unit)
    {
      if (currentUnitPtr_ == 0)
	{
	  currentUnitPtr_ = &unit;
	  QString ylabel = unit.GetDimension().GetName().c_str();
	  ylabel += " (" + QString(unit.GetSymbol().c_str()) + " )";
	  plot_->setAxisTitle(QwtPlot::yLeft, ylabel);
	  return true;
	}
      else if (currentUnitPtr_ != &unit)
	{
	  const std::string msg = "You want to add a data with a different unit from the current displayed data on the graph. Are you sure ?";

	  int ret = QMessageBox::warning(this, tr("openmeca (plot)"),
					 tr(msg.c_str()),
					 QMessageBox::YesRole|QMessageBox::NoRole,
					 QMessageBox::NoRole);
	  if (ret==QMessageBox::NoRole)
	    {
	      plot_->setAxisTitle(QwtPlot::yLeft, "?");
	      return true;
	    }
	  return false;
	}
      return true;
    }

    void
    MainPlotWindow::showEvent(QShowEvent * event)
    {
      QMainWindow::showEvent(event);
      UpdateDataMenu(menuAdd_);
    }

    void MainPlotWindow::Update(OpenMeca::Setting::Simulation::Step)
    {
      if (isHidden()==true)
	return;
      
      std::map<QwtPlotCurve*, const std::vector<double>*>::iterator it;
      for (it=data_.begin(); it!=data_.end(); ++it)
	{
	  QwtPlotCurve* curve =it->first;
	  const std::vector<double>& data = *it->second;
	  SetSample(data, curve);
	}
      plot_->replot();
    }

    void
    MainPlotWindow::UpdateDataMenu(QMenu *menu)
    {
      menu->clear();

      Core::SetOfBase<Item::Physical>& physicals = 
	Core::System::Get().GetSetOf<Item::Physical>();
      
      for (unsigned int i = 0; i < physicals.GetTotItemNumber(); ++i)
	{
	  Item::Physical& phys = physicals(i);
	  const QString title = phys.GetName().c_str();
	  const QIcon icon = phys.GetIcon();
	  QMenu* submenu = menu->addMenu(icon, title); 
	  phys.FillDataMenu(submenu);
	}

    }

    void
    MainPlotWindow::AddData(const std::vector<double>& data, 
			    const QString& label, 
			    const Util::Unit& unit,
			    const Physic::Type& type)
    {
      if (SetUnit(unit))
	{
	  QwtPlotCurve* curve = new QwtPlotCurve(label); 
	  SetSample(data, curve);
	  curve->attach(plot_);
	  curve->setPen(* new QPen(Util::Color().GetQColor(),2, Qt::SolidLine));    
	  plot_->replot();
	  OMC_ASSERT_MSG(data_.count(curve) == 0, "The curve is already registered");
	  data_[curve] = &data;
	}
      recordedType_.push_back(&type);
    }

    void
    MainPlotWindow::Clear()
    {
      plot_->detachItems();
      recordedType_.clear();
      data_.clear();
      plot_->setAxisTitle(QwtPlot::yLeft, "");
      currentUnitPtr_ = 0;
      plot_->replot();
    }

    void MainPlotWindow::PopUpMenu(const QPoint &pos) 
    {
      UpdateDataMenu(menuAdd_);
      menuPlot_->exec(plot_->mapToGlobal(pos));
    }
    
    
    void MainPlotWindow::DataDeleted(const Physic::Type& type)
    {
      for (unsigned int i = 0; i < recordedType_.size(); ++i)
	{
	  if (recordedType_[i] == &type)
	    {
	      if (isVisible())
		{
		  const QString msg = QObject::tr("One of the data seems to be deleted, the plot will be cleared");
		  OMC_WARNING_MSG(msg);
		}
	      Clear();
	    }
	}
    }

    void MainPlotWindow::SetSample(const std::vector<double>& data, 
				   QwtPlotCurve* curve)
    {

      // OMC_ASSERT_MSG(data.size() == time_.size() || data.size() + 1 == time_.size(),
      // 		     "The number of data point is incoherent");

      int datasize = data.size();
      if (datasize > int(time_.size()))
	datasize =  time_.size();

      curve->setSamples(&time_[0], &data[0], datasize);
    }

  }
}
