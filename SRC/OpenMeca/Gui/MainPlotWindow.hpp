// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@yakuru.fr>
//
// Copyright (C) 2012-2017 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Gui_MainPlotWindow_hpp
#define OpenMeca_Gui_MainPlotWindow_hpp

#include <QMainWindow>
#include <qwt/qwt_plot.h>
#include <qwt/qwt_plot_curve.h>

#include "ui_MainPlotWindow.h"
#include "OpenMeca/Setting/Simulation.hpp"
#include "OpenMeca/Util/Unit.hpp"
#include "OpenMeca/Physic/Type.hpp"


namespace OpenMeca
{
  namespace Gui
  {
    class MainPlotWindow : public QMainWindow, 
		           private Ui::MainPlotWindow
    {
      Q_OBJECT
    public:
      static MainPlotWindow& Get();

      MainPlotWindow(QWidget * parent=0);
      virtual ~MainPlotWindow();
      
      void AddData(const std::vector<double>&, const QString& label, const Util::Unit&, const Physic::Type&);
      void DataDeleted(const Physic::Type&);												    

      
    private slots:
      void Clear();
      void Update(OpenMeca::Setting::Simulation::Step);
      void PopUpMenu(const QPoint &);

    private:
      bool SetUnit(const Util::Unit&);
      void SetSample(const std::vector<double>&, QwtPlotCurve* );
      void UpdateDataMenu(QMenu *menu);
      void showEvent(QShowEvent * event);
      
    private:
      static MainPlotWindow* me_;
      const std::vector<double>& time_;
      std::map<QwtPlotCurve*, const std::vector<double>*> data_;
      std::vector<const Physic::Type*> recordedType_;
      const Util::Unit* currentUnitPtr_;
    };

  }
}

#endif
