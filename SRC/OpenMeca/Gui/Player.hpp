// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@yakuru.fr>
//
// Copyright (C) 2012-2017 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Gui_Player_hpp
#define OpenMeca_Gui_Player_hpp

#include <QWidget>
#include "ui_Player.h"
#include "OpenMeca/Setting/Simulation.hpp"

namespace OpenMeca
{
  namespace Gui
  {
    

    class Player : public QWidget,
		   private Ui::Player
		   
    {
      Q_OBJECT
      
    public:
      Player(QWidget * parent);
      ~Player();
      
    signals:
      void Stopped();
      void Played();
      void Paused();
		   

    public slots:
      void TogglePlay(bool);
      void Stop();
      void Move(int);
      void Pause();
      void End();

    public slots:
      void Update(OpenMeca::Setting::Simulation::Step);
      void Record();

    signals:
      void MovedTo(unsigned int);

    private:
      void Play();
      QStringList GetSupportedImageFormat() const;
      
      void UpdateWidget();

    private:
      const QString timeLabelPrefix_;
      const QString iterLabelPrefix_;
      Setting::Simulation::Step step_;
    };

  }
}
#endif
