// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@yakuru.fr>
//
// Copyright (C) 2012-2017 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Prop_PropMatrix_hpp
#define OpenMeca_Prop_PropMatrix_hpp


#include <QLabel>
#include <QLineEdit>

#include "OpenMeca/Gui/Prop/PropT.hpp"
#include "OpenMeca/Gui/Widget/WidgetDouble.hpp"
#include "OpenMeca/Util/Dimension.hpp"
#include "OpenMeca/Core/Condition.hpp"

#include "OpenMeca/Geom/Point.hpp"
#include "OpenMeca/Geom/Quaternion.hpp"
#include "OpenMeca/Geom/Frame.hpp"
#include "OpenMeca/Geom/Matrix.hpp"


namespace OpenMeca
{
  namespace Gui
  {
    class PropMatrix: public QObject, public PropT<Geom::Matrix<_3D> >
    {
      Q_OBJECT
      public:
      PropMatrix(QWidget* parent);
      virtual ~PropMatrix();
      
      void SetDimension(const Util::Dimension& dim);
      const Util::Dimension& GetDimension() const;
      
      bool Check();
      void ApplyValue();

      void Insert(PropTree& tree);

      void AddComponentCondition(const Core::Condition<double>*);

    private:
      void Init();

    private slots:
      void XYEdited(const QString&);
      void XZEdited(const QString&);
      void YZEdited(const QString&);

    private:
      QTreeWidgetItem itemXX_;
      QTreeWidgetItem itemXY_;
      QTreeWidgetItem itemXZ_;

      QTreeWidgetItem itemYX_;
      QTreeWidgetItem itemYY_;
      QTreeWidgetItem itemYZ_;

      QTreeWidgetItem itemZX_;
      QTreeWidgetItem itemZY_;
      QTreeWidgetItem itemZZ_;

      WidgetDouble xx_;
      WidgetDouble yx_;
      WidgetDouble zx_;

      WidgetDouble xy_;
      WidgetDouble yy_;
      WidgetDouble zy_;

      WidgetDouble xz_;
      WidgetDouble yz_;
      WidgetDouble zz_;

   
    };

    
    
    


  }
}
#endif

