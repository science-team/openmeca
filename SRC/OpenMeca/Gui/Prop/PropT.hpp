// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@yakuru.fr>
//
// Copyright (C) 2012-2017 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Gui_Prop_PropBaseT_hpp
#define OpenMeca_Gui_Prop_PropBaseT_hpp



#include "OpenMeca/Gui/Prop/Prop.hpp"

namespace OpenMeca
{
  namespace Gui
  {
    template<class T>
    class PropT : public Prop
    {
      
    public:
      PropT(QWidget& parent);
      virtual ~PropT();
      virtual void SetValue(T&);
      void ApplyChangement();
      T& GetCopy();
      T& GetValue();
      T& GetInitialValue();
      void CancelChangement();
      void Reset();
      virtual bool Check() = 0;
      virtual void PostChangement();

    protected:
      virtual void Init() = 0;

    private:
      T* val_;
      T* copy_;
      T* initialValue_;
    };

    template<class T>
    inline
    PropT<T>::PropT(QWidget & parent)
      :Prop(parent),
       val_(0),
       copy_(0),
       initialValue_(0)
    {
    }
    
    template<class T>
    inline
    PropT<T>::~PropT()
    {
    }

    template<class T>
    inline T&
    PropT<T>::GetValue()
    {
      OMC_ASSERT_MSG(val_!=0, "The val pointer is null");
      return *val_;
    }



    template<class T>
    inline void 
    PropT<T>::SetValue(T& val)
    {
      OMC_ASSERT_MSG(val_==0, "The val pointer must be null");
      OMC_ASSERT_MSG(copy_ == 0, "The copy pointer must be null");
      OMC_ASSERT_MSG(initialValue_ == 0, "The initial value must be null");
      val_ = &val;
      copy_         = new T(val);
      initialValue_ = new T(val);
      Init();
    }


    template<class T>
    inline void     
    PropT<T>::CancelChangement()
    {
      GetValue() = GetInitialValue();
      GetCopy() = GetValue();
      Init();
      PostChangement();
    }

    template<class T>
    inline void     
    PropT<T>::ApplyChangement()
    {
      OMC_ASSERT_MSG(Check()==true, "The widget must be valid before applying changement");
      GetValue() = GetCopy();
      PostChangement();
    }

    template<class T>
    inline void     
    PropT<T>::Reset()
    {
      delete copy_;
      delete initialValue_;
      val_          = 0;
      copy_         = 0;
      initialValue_ = 0;
    }


    template<class T>
    inline T&  
    PropT<T>::GetCopy()
    {
      OMC_ASSERT_MSG(copy_ != 0, "The copy can't be null");
      return *copy_;
    }

    
    template<class T>
    inline T&  
    PropT<T>::GetInitialValue()
    {
      OMC_ASSERT_MSG(initialValue_ != 0, "The initial value can't be null");
      return *initialValue_;
    }

    template<class T>
    inline void 
    PropT<T>::PostChangement()
    {
    }

  }
}

#endif
