// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@yakuru.fr>
//
// Copyright (C) 2012-2017 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Widget_WidgetExpr_hpp
#define OpenMeca_Widget_WidgetExpr_hpp


#include <QWidget>
#include <QBoxLayout>
#include <QLabel>
#include <QLineEdit>

#include "ui_WidgetExpr.h"
#include "OpenMeca/Util/Dimension.hpp"
#include "OpenMeca/Core/Condition.hpp"
#include "OpenMeca/Util/Expr.hpp"

namespace OpenMeca
{
  namespace Gui
  {
    
    class WidgetExpr: public QWidget, 
		      private Ui::WidgetExpr
    {
      
    public:
      WidgetExpr(QWidget* parent);
      virtual ~WidgetExpr();
      
      void SetDimension(const Util::Dimension& dim);
      const Util::Dimension& GetDimension() const;
      
      void SetExpr(const Util::Expr& val);
      bool GetExpr(Util::Expr& val);
      bool ComputeExpression();

      QLineEdit& GetLineEdit();

      void DisplayHelp(const QString& message);

      
      void AddCondition(const Core::Condition<double>*);
      bool CheckCondition(const double& );

    private:
      const Util::Dimension* dim_;
      std::vector< const Core::Condition<double>* > condition_;
      
   
    };
    
  }
}
#endif

