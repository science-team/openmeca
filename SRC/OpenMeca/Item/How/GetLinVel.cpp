// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@yakuru.fr>
//
// Copyright (C) 2012-2017 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.




#include "OpenMeca/Item/How/GetLinVel.hpp"
#include "OpenMeca/Item/Body.hpp"

#include "Serialization/export.hpp"
BOOST_CLASS_EXPORT(OpenMeca::Item::How::GetLinVel::MySensor)




namespace OpenMeca
{
  namespace Item
  {
    namespace How
    {
      
      const std::string 
      GetLinVel::GetStrType()
      {
	return Physic::LinearVelocity::GetStrType();
      }

      const QString
      GetLinVel::GetQStrType()
      {
	return Physic::LinearVelocity::GetQStrType();
      }

      Geom::Vector<_3D> 
      GetLinVel::Acquire(MySensor& sensor)
      {
	PartPoint& parent =  sensor.GetParent();
	Body& body = const_cast<Body&>(parent.GetBody());
	Geom::Frame<_3D>&(Body::*fnptr)() = &Body::GetFrame;

	std::function<const Geom::Frame<_3D>& ()> f = 
	  std::bind(fnptr, std::ref(body));
	
	const Geom::Point<_3D> p(parent.GetPoint(), f);
	chrono::ChSharedBodyPtr& chbody = 
	  const_cast<chrono::ChSharedBodyPtr&>(body.GetChBodyPtr());
	
	chrono::ChVector<> chp = body.ExpressPointInLocalChFrame(p);
	chrono::ChVector<> v = chbody->RelPoint_AbsSpeed (&chp);

	return Geom::Vector<_3D>(v);

      }

    }
  }
}
    
   

    

