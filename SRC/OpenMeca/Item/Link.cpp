// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@yakuru.fr>
//
// Copyright (C) 2012-2017 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "OpenMeca/Item/Link.hpp"
#include "OpenMeca/Item/Part.hpp"
#include "OpenMeca/Item/Body.hpp"
#include "OpenMeca/Core/System.hpp"
#include "OpenMeca/Core/UserRootItemCommonProperty.hpp"
#include "OpenMeca/Gui/MainWindow.hpp"


#include "Serialization/export.hpp"
//Don't forget to export for dynamic serialization of child class
BOOST_CLASS_EXPORT(OpenMeca::Item::Link)



namespace OpenMeca
{  
  namespace Item
  {


    const std::string 
    Link::GetStrType()
    {
      return "Link";
    }

    const QString 
    Link::GetQStrType()
    {
      return QObject::tr("Link");
    }


    void
    Link::Init()
    {
      Core::Singleton< Core::UserRootItemCommonProperty<Link> >::Get();
      Core::Singleton< Core::UserItemCommonProperty<Link> >::Get();
    }
     

    Link::Link(const std::string& strType)
      :Core::UserRootItem(strType, Core::Singleton< Core::UserRootItemCommonProperty<Link> >::Get().GetRootTreeWidgetItem()),
       body1_(*this, **  Core::System::Get().GetSetOf<Body>().Begin()),
       body2_(*this, **++Core::System::Get().GetSetOf<Body>().Begin()),
       treeItembody1_(0),
       treeItembody2_(0),
       center_(),
       quaternion_(),
       frame_(center_,quaternion_),

       centerBody1_(std::bind(&Link::GetReferenceFrameBody1, std::ref(*this))),
       quaternionBody1_(std::bind(&Link::GetReferenceFrameBody1, std::ref(*this))),
       frameBody1_(centerBody1_, quaternionBody1_),

       centerBody2_(std::bind(&Link::GetReferenceFrameBody2, std::ref(*this))),
       quaternionBody2_(std::bind(&Link::GetReferenceFrameBody2, std::ref(*this))),
      frameBody2_(centerBody2_, quaternionBody2_)
    {
      treeItembody1_ = new Gui::SecondaryTreeItem(GetMainTreeItem(), 
						  std::bind(&Link::Body1, std::ref(*this)));
      treeItembody2_ = new Gui::SecondaryTreeItem(GetMainTreeItem(), 
						  std::bind(&Link::Body2, std::ref(*this)));
      AddSecondaryTreeItem(*treeItembody1_);
      AddSecondaryTreeItem(*treeItembody2_);
      UpdateReferenceFrame();
    }



    Link::Link(const std::string& strType, Body& body1, Body& body2)
      :Core::UserRootItem(strType, Core::Singleton< Core::UserRootItemCommonProperty<Link> >::Get().GetRootTreeWidgetItem()),
       body1_(*this, body1),
       body2_(*this, body2),
       treeItembody1_(0),
       treeItembody2_(0),
       center_(),
       quaternion_(),
       frame_(center_,quaternion_),
       
       centerBody1_(std::bind(&Link::GetReferenceFrameBody1, std::ref(*this))),
       quaternionBody1_(std::bind(&Link::GetReferenceFrameBody1, std::ref(*this))),
      frameBody1_(centerBody1_, quaternionBody1_),
      
      centerBody2_(std::bind(&Link::GetReferenceFrameBody2, std::ref(*this))),
      quaternionBody2_(std::bind(&Link::GetReferenceFrameBody2, std::ref(*this))),
      frameBody2_(centerBody2_, quaternionBody2_)
    {
      treeItembody1_ = new Gui::SecondaryTreeItem(GetMainTreeItem(), 
						  std::bind(&Link::Body1, std::ref(*this)));
      treeItembody2_ = new Gui::SecondaryTreeItem(GetMainTreeItem(), 
						  std::bind(&Link::Body2, std::ref(*this)));
      AddSecondaryTreeItem(*treeItembody1_);
      AddSecondaryTreeItem(*treeItembody2_);
      UpdateReferenceFrame();
    }



      
  

    Link::~Link()
    {
    }

    Body&
    Link::GetBody1()
    {
      return *body1_.GetPtr();
    }

    Body&
    Link::GetBody2()
    {
      return *body2_.GetPtr();
    }

    const Body& 
    Link::GetBody1() const
    {
      return *body1_.GetPtr();
    }


    const Body& 
    Link::GetBody2() const
    {
      return *body2_.GetPtr();
    }

    Body& 
    Link::Body1()
    {
      return *body1_.GetPtr();
    }


    Body& 
    Link::Body2()
    {
      return *body2_.GetPtr();
    }


    Core::AutoRegisteredPtr<Body, Link>& 
    Link::GetBody1Ptr()
    {
      return body1_;
    }

    const Core::AutoRegisteredPtr<Body, Link>& 
    Link::GetBody1Ptr() const
    {
      return body1_;
    }

    Core::AutoRegisteredPtr<Body, Link>& 
    Link::GetBody2Ptr()
    {
      return body2_;
    }

    const Core::AutoRegisteredPtr<Body, Link>& 
    Link::GetBody2Ptr() const
    {
      return body2_;
    }

    template<>
    Body&
    Link::GetBody<1>()
    {
      return GetBody1();
    }

    template<>
    Body&
    Link::GetBody<2>()
    {
      return GetBody2();
    }

    template<>
    const Body& 
    Link::GetBody<1>() const
    {
      return GetBody1();
    }


    template<>
    const Body& 
    Link::GetBody<2>() const
    {
      return GetBody2();
    }


    template<> 
    Part& 
    Link::GetPart<1>()
    {
      return GetPart1();
    }

    template<> 
    const Part& 
    Link::GetPart<1>() const
    {
      return GetPart1();
    }

    template<> 
    Part& 
    Link::GetPart<2>()
    {
      return GetPart2();
    }

    template<> 
    const Part& 
    Link::GetPart<2>() const
    {
      return GetPart2();
    }


    const Geom::Frame<_3D>& 
    Link::GetReferenceFrameBody1() const
    {
      return GetBody1().GetFrame();
    }

    const Geom::Frame<_3D>& 
    Link::GetReferenceFrameBody2() const
    {
      return GetBody2().GetFrame();
    }


    template<>
    const Geom::Frame<_3D>& 
    Link::GetFrameBody<1>() const
    {
      return frameBody1_;
    }
  
    template<>
    const Geom::Frame<_3D>& 
    Link::GetFrameBody<2>() const
    {
      return frameBody2_;
    }


    void 
    Link::UpdateReferenceFrame()
    {
      centerBody1_ = Geom::Point<_3D>(center_, 
      				      std::bind(&Link::GetReferenceFrameBody1, std::ref(*this)));
      quaternionBody1_ = Geom::Quaternion<_3D>(quaternion_, 
      					       std::bind(&Link::GetReferenceFrameBody1, std::ref(*this)));
      centerBody2_ = Geom::Point<_3D>(center_, 
      				      std::bind(&Link::GetReferenceFrameBody2, std::ref(*this)));
      quaternionBody2_ = Geom::Quaternion<_3D>(quaternion_, 
      					       std::bind(&Link::GetReferenceFrameBody2, std::ref(*this)));

    }



    void 
    Link::Update()
    {
      UpdateReferenceFrame();
      Core::UserItem::Update();
    }


    bool&
    Link::DrawLocalFrame()
    {
      return GetPart1().GetDrawLocalFrame();
    }

    const bool&
    Link::DrawLocalFrame() const
    {
      return GetPart1().GetDrawLocalFrame();
    }
    
    
  }
}




