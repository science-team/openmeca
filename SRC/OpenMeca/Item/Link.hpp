// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@yakuru.fr>
//
// Copyright (C) 2012-2017 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Item_Link_hpp
#define OpenMeca_Item_Link_hpp

#include "OpenMeca/Core/UserRootItem.hpp"
#include "OpenMeca/Core/AutoRegisteredPtr.hpp"
#include "OpenMeca/Core/ItemCommonProperty.hpp"

#include "OpenMeca/Gui/SecondaryTreeItem.hpp"
#include "OpenMeca/Util/Color.hpp"
#include "OpenMeca/Util/MotionLaw.hpp"
#include "OpenMeca/Core/None.hpp"
#include "OpenMeca/Item/Body.hpp"
#include "OpenMeca/Geom/Frame.hpp"
#include "OpenMeca/Geom/Point.hpp"
#include "OpenMeca/Util/ExprPoint.hpp"
#include "OpenMeca/Util/ExprAttitude.hpp"
#include "OpenMeca/Geom/Quaternion.hpp"

#include "ChronoEngine/physics/ChLinkLock.h"

namespace OpenMeca
{  
  namespace Item
  {
    class Body;

    // Base for mechanical links
    class Link: public Core::UserRootItem , public Core::AutoRegister<Link>
    {
    public:
      static const std::string GetStrType(); 
      static const QString GetQStrType(); 
      static void Init();

      typedef Core::None Dialog;

    public:
      Link(const std::string&);
      Link(const std::string&, Body&, Body&);

      virtual ~Link();

      virtual void Update();
      void UpdateReferenceFrame();

      bool& DrawLocalFrame();
      const bool& DrawLocalFrame() const;

      //Accessor
      OMC_ACCESSOR   (Center     , Util::ExprPoint      , center_    );
      OMC_ACCESSOR   (Quaternion , Util::ExprAttitude , quaternion_);
      OMC_ACCESSOR   (Frame      , Geom::Frame<_3D>     , frame_     );
      OMC_ACCESS_CST (FrameBody1 , Geom::Frame<_3D>     , frameBody1_);
      OMC_ACCESS_CST (FrameBody2 , Geom::Frame<_3D>     , frameBody2_);

      Body& GetBody1();
      Body& GetBody2();

      Body& Body1();
      Body& Body2();

      const Body& GetBody1() const;
      const Body& GetBody2() const;

      Core::AutoRegisteredPtr<Body, Link>& GetBody1Ptr();
      const Core::AutoRegisteredPtr<Body, Link>& GetBody1Ptr() const;

      Core::AutoRegisteredPtr<Body, Link>& GetBody2Ptr();
      const Core::AutoRegisteredPtr<Body, Link>& GetBody2Ptr() const;

      template<int N> Body& GetBody();
      template<int N> const Body& GetBody() const;

      const Geom::Frame<_3D>& GetReferenceFrameBody1() const;
      const Geom::Frame<_3D>& GetReferenceFrameBody2() const;

      template<int N> const Geom::Frame<_3D>& GetFrameBody() const;
      template<int N> std::function<const Geom::Frame<_3D>& ()> GetFrameFctBody() const;

      virtual Part& GetPart1() = 0;
      virtual const Part& GetPart1() const = 0;

      virtual Part& GetPart2() = 0;
      virtual const Part& GetPart2() const = 0;

      template<int N> Part& GetPart();
      template<int N> const Part& GetPart() const;

      virtual chrono::ChLink& GetChLink() = 0;

    private:
      Link();                        //Not allowed
      Link(const Link&);             //Not Allowed
      Link& operator=(const Link&);  //Not Allowed

      void EraseBody1();
      void EraseBody2();

      friend class boost::serialization::access;
      template<class Archive> void save(Archive& ar, const unsigned int) const;
      template<class Archive> void load(Archive& ar, const unsigned int);
      BOOST_SERIALIZATION_SPLIT_MEMBER()

    private:
      Core::AutoRegisteredPtr<Body, Link> body1_;
      Core::AutoRegisteredPtr<Body, Link> body2_;
      Gui::SecondaryTreeItem* treeItembody1_;
      Gui::SecondaryTreeItem* treeItembody2_;
      Util::ExprPoint center_;
      Util::ExprAttitude quaternion_;
      Geom::Frame<_3D> frame_;      

      Geom::Point<_3D> centerBody1_;
      Geom::Quaternion<_3D> quaternionBody1_;
      Geom::Frame<_3D> frameBody1_;      

      Geom::Point<_3D> centerBody2_;
      Geom::Quaternion<_3D> quaternionBody2_;
      Geom::Frame<_3D> frameBody2_;

    };


    template<class Archive>
    inline void
    Link::save(Archive& ar, const unsigned int) const
    {
      ar << BOOST_SERIALIZATION_BASE_OBJECT_NVP(Core::UserRootItem);
      ar << BOOST_SERIALIZATION_NVP(body1_);
      ar << BOOST_SERIALIZATION_NVP(body2_);
      ar << BOOST_SERIALIZATION_NVP(center_);
      ar << BOOST_SERIALIZATION_NVP(quaternion_);

    }


    template<class Archive>
    inline void
    Link::load(Archive& ar, const unsigned int version)
    {
      body1_.Unregister();
      body2_.Unregister();
      ar >> BOOST_SERIALIZATION_BASE_OBJECT_NVP(Core::UserRootItem);
      ar >> BOOST_SERIALIZATION_NVP(body1_);
      ar >> BOOST_SERIALIZATION_NVP(body2_);
      if (version >= 1)
	ar >> BOOST_SERIALIZATION_NVP(center_);
      else
	{
	  Geom::Point<_3D> fake;
	  ar >> BOOST_SERIALIZATION_NVP(fake);
	  center_ = fake;
	}
      
      if (version >= 2)
	ar >> BOOST_SERIALIZATION_NVP(quaternion_);
      else
	{
	  Geom::Quaternion<_3D> fake;
	  ar >> BOOST_SERIALIZATION_NVP(fake);
	  quaternion_ = fake;	  
	}
    }
    
    
    template<int N> 
    inline  std::function<const Geom::Frame<_3D>& ()> 
    Link::GetFrameFctBody() const
    {
      const Link& me = *this;
      const Geom::Frame<_3D>&(Link::*fnpt)() const = &Link::GetFrameBody<1>;
      std::function<const Geom::Frame<_3D>& ()> f = std::bind(fnpt, std::ref(me));
      return f;
    }

  }

}

namespace OpenMeca
{  
  namespace Core
  {
    template<>
    inline void
    ItemCommonProperty<OpenMeca::Item::Link>::BuildIconSymbol()
    {
      iconSymbol_ = Util::Icon::DrawIconFromSvgFile(":/Rsc/Img/Link.svg");
    }

  }
}


#include "Serialization/version.hpp"
BOOST_CLASS_VERSION(OpenMeca::Item::Link, 2)

#endif
