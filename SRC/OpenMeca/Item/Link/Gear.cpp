// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@yakuru.fr>
//
// Copyright (C) 2012-2017 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "OpenMeca/Item/Link/Gear.hpp"
#include "OpenMeca/Item/LinkT.hpp"
#include "OpenMeca/Gui/MainWindow.hpp"
#include "OpenMeca/Core/Drawable.hpp"
#include "OpenMeca/Core/System.hpp"

#include "OpenMeca/Core/Drawable.hpp" 
#include "OpenMeca/Item/PartPoint.hpp" 
#include "OpenMeca/Util/Draw.hpp"
#include "OpenMeca/Item/LinkT.hpp" 




namespace OpenMeca
{  
  namespace Item
  {
    const std::string 
    Gear::GetStrType()
    {
      return "Gear";
    }

    const QString 
    Gear::GetQStrType()
    {
      return QObject::tr("Gear");
    }


    Gear::Gear(Link& link)
      :LinkTypeBase(link),
       ratio_(1.),
       interAxisLength_(0.),
       modulus_(0.001),
       angleOfAction_(M_PI*20./180.),
       internalTeeth_(false)
    {
    }

    Gear::~Gear()
    {
    }



    template<> 
    double
    Gear::ComputeRadius<1>()
    {
      if (internalTeeth_ == false)
	return interAxisLength_ / (1. + (1./ratio_));
      return interAxisLength_ / (1. - (1./ratio_));
      
    }


    template<> 
    void 
    Gear::DrawPart<1>()
    {
      const double scale = Part::GetScaleValue();
      const float l = 0.1f*scale;
      const float R0 = ComputeRadius<1>();
      const float Rb = R0*cos(angleOfAction_); 
      OMC_ASSERT_MSG(R0==R0, "Problem while computing gear radius");
      
      glRotatef(180.0f, 0.0f, 0.0f, 1.0f);
      if (internalTeeth_ == false)
	{
	  glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
	  Util::Draw::Cylinder(Rb*1.01, l);
	  glRotatef(-90.0f, 0.0f, 1.0f, 0.0f);
	  Util::Draw::OuterCylinderTeeth(R0, modulus_, angleOfAction_, l);
	}
      else
	{
	  glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
	  Util::Draw::Cylinder(R0+(.1*R0), R0 + modulus_, l);
	  Util::Draw::InnerCylinderTeeth(R0, modulus_, angleOfAction_, l);
	}
    }

 

    template<> 
    double
    Gear::ComputeRadius<2>()
    {
      if (internalTeeth_ == false)
	return interAxisLength_ / (1. + ratio_);
      return interAxisLength_ / (ratio_ - 1.);
    }

    template<> 
    void 
    Gear::DrawPart<2>()
    {
      const double scale = Part::GetScaleValue();
      const float l = 0.1f*scale;
      const float R0 = ComputeRadius<2>();
      const float Rb = R0*cos(angleOfAction_); 
      OMC_ASSERT_MSG(R0==R0, "Problem while computing gear radius");

      glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
      Util::Draw::Cylinder(Rb*1.01, l);
      glRotatef(-90.0f, 0.0f, 1.0f, 0.0f);
      Util::Draw::OuterCylinderTeeth(R0, modulus_, angleOfAction_, l);
    }


    void 
    Gear::UpdatePart()
    {
      GetLink().GetPart2().GetCenter()[1] = interAxisLength_;
    }


    


  }
}




