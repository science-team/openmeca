## This file is part of OpenMeca, an easy software to do mechanical simulation.
##
## Author(s)    :  - Damien ANDRE  <openmeca@yakuru.fr>
##
## Copyright (C) 2012-2017 Damien ANDRE
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.


HEADERS += Item/Link/LinkTypeBase.hpp \
           Item/Link/Revolute.hpp \
           Item/Link/Slider.hpp \
           Item/Link/Cylindrical.hpp \
           Item/Link/LinearMotor.hpp \
           Item/Link/Motor.hpp \
           Item/Link/Planar.hpp \
           Item/Link/PointLine.hpp \
           Item/Link/PointPlane.hpp \
           Item/Link/Spherical.hpp \
           Item/Link/Screw.hpp \
           Item/Link/Gear.hpp \
           Item/Link/Pulley.hpp \
           Item/Link/RackPinion.hpp \
           Item/Link/Spring.hpp

SOURCES += Item/Link/LinkTypeBase.cpp \
           Item/Link/Revolute.cpp \
           Item/Link/Slider.cpp \
           Item/Link/Cylindrical.cpp \
           Item/Link/LinearMotor.cpp \
           Item/Link/Motor.cpp \
           Item/Link/Planar.cpp \
           Item/Link/PointLine.cpp \
           Item/Link/PointPlane.cpp \
           Item/Link/Spherical.cpp \
           Item/Link/Screw.cpp \
           Item/Link/Gear.cpp \
           Item/Link/Pulley.cpp \
           Item/Link/RackPinion.cpp \
           Item/Link/Spring.cpp
