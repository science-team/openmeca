// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@yakuru.fr>
//
// Copyright (C) 2012-2017 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Item_Link_Motor_hpp
#define OpenMeca_Item_Link_Motor_hpp

#include <string>
#include <QIcon>
#include <QColor>

#include "OpenMeca/Item/Link/LinkTypeBase.hpp"
#include "OpenMeca/Gui/Dialog/DialogLinkMotor.hpp"
#include "OpenMeca/Core/ItemCommonProperty.hpp"
#include "OpenMeca/Item/LinkT.hpp"
#include "OpenMeca/Util/CustomChFunction.hpp"
#include "OpenMeca/Item/Link_CreateAction_SpecializedT.hpp"

#include "ChronoEngine/physics/ChLinkEngine.h"
#include "ChronoEngine/physics/ChLinkEngine.h"


namespace OpenMeca
{    
  namespace Item
  {

    class Motor: public LinkTypeBase
    {
    public:
      static const std::string GetStrType();
      static const QString GetQStrType();

      typedef chrono::ChLinkEngine ChLink; 
      typedef Gui::DialogLinkMotor Dialog;

    public:
      Motor(Link&);
      ~Motor();

      template <int N> void DrawPart();
      template <int N> void BuildPoints(Core::SetOf<PartPoint>&, Core::DrawableUserItem&);

      // Accessors
      OMC_ACCESSOR(Velocity , Util::CustomChFunction,  velocity_);

    private:
      friend class boost::serialization::access;
      template<class Archive> void serialize(Archive& ar, const unsigned int version);
    
    private:
        Util::CustomChFunction velocity_;
    };


    template<class Archive>
    inline void
    Motor::serialize(Archive& ar, const unsigned int version)
    {
      ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(LinkTypeBase);
      if (version == 0)
	{
	  double fake = 0.;
	  ar & BOOST_SERIALIZATION_NVP(fake);
	  velocity_.SetExpressionFromValue(fake);
	  return;
	}
      ar & BOOST_SERIALIZATION_NVP(velocity_);
    }


  }
}

namespace OpenMeca
{  
  namespace Core
  {
    

    template<> 
    inline void
    ItemCommonProperty< OpenMeca::Item::LinkT<OpenMeca::Item::Motor> >::BuildIconSymbol()
    {
      OpenMeca::Item::LinkT<OpenMeca::Item::Motor>::BuildIconSymbol(iconSymbol_);
    }

    template<>
    inline void
    UserItemCommonProperty<OpenMeca::Item::LinkT<OpenMeca::Item::Motor> >::CreateAction_Specialized() 
    {
      OpenMeca::Item::Link_CreateAction_SpecializedT(*this);
    }

  }
}

#include "Serialization/version.hpp"
BOOST_CLASS_VERSION(OpenMeca::Item::Motor, 1)



#endif
