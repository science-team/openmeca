// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@yakuru.fr>
//
// Copyright (C) 2012-2017 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "OpenMeca/Item/Link/Planar.hpp"
#include "OpenMeca/Gui/MainWindow.hpp"
#include "OpenMeca/Core/Drawable.hpp"
#include "OpenMeca/Core/System.hpp"

#include "OpenMeca/Core/Drawable.hpp" 
#include "OpenMeca/Item/PartPoint.hpp" 
#include "OpenMeca/Util/Draw.hpp"
#include "OpenMeca/Util/Icon.hpp"
#include "OpenMeca/Item/LinkT.hpp" 




namespace OpenMeca
{  
  namespace Item
  {
    const std::string 
    Planar::GetStrType()
    {
      return "Planar";
    }

    const QString 
    Planar::GetQStrType()
    {
      return QObject::tr("Planar");
    }

    Planar::Planar(Link& link)
      :LinkTypeBase(link)
    {
    }
    
    Planar::~Planar()
    {
    }

    template<> 
    void 
    Planar::DrawPart<1>()
    {
      const double scale = Part::GetScaleValue();
      float Lx = 0.6f*scale;
      float Ly = 0.6f*scale;
      float Lz = 0.02f*scale;

      glTranslatef(0.0f, 0.0f, -Lz/2.f);
      Util::Draw::Box(Lx, Ly, Lz);
      glTranslatef(0.0f, 0.0f, -Lz/2.f);
    }

    

    template<>
    void 
    Planar::BuildPoints<1>(Core::SetOf<PartPoint>& set, Core::DrawableUserItem& item)
    {
      OMC_ASSERT_MSG(set.GetTotItemNumber()==0, "The number of point is not null");
      PartPoint* p1 = new PartPoint(item);
      p1->GetName() = "middle";
      p1->SetScaleCoordinate(0., 0., 0.);
      set.AddItem(*p1);
    }


    template<> 
    void 
    Planar::DrawPart<2>()
    {
      const double scale = Part::GetScaleValue();
      float Lx = 0.6f*scale;
      float Ly = 0.6f*scale;
      float Lz = 0.02f*scale;

      glTranslatef(0.0f, 0.0f,  Lz/2.f);
      Util::Draw::Box(Lx, Ly, Lz);
      glTranslatef(0.0f, 0.0f, -Lz/2.f);
    }


    template<>
    void 
    Planar::BuildPoints<2>(Core::SetOf<PartPoint>& set, Core::DrawableUserItem& item)
    {
      OMC_ASSERT_MSG(set.GetTotItemNumber()==0, "The number of point is not null");
      PartPoint* p1 = new PartPoint(item);
      p1->GetName() = "middle";
      p1->SetScaleCoordinate(0., 0., 0.);
      set.AddItem(*p1);
    }

  }
}




