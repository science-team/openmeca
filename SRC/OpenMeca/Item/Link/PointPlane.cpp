// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@yakuru.fr>
//
// Copyright (C) 2012-2017 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "OpenMeca/Item/Link/PointPlane.hpp"
#include "OpenMeca/Gui/MainWindow.hpp"
#include "OpenMeca/Core/Drawable.hpp"
#include "OpenMeca/Core/System.hpp"

#include "OpenMeca/Core/Drawable.hpp" 
#include "OpenMeca/Item/PartPoint.hpp" 
#include "OpenMeca/Util/Draw.hpp"
#include "OpenMeca/Util/Icon.hpp"
#include "OpenMeca/Item/LinkT.hpp" 



namespace OpenMeca
{  
  namespace Item
  {
    const std::string 
    PointPlane::GetStrType()
    {
      return "PointPlane";
    }

    const QString 
    PointPlane::GetQStrType()
    {
      return QObject::tr("PointPlane");
    }

    template<> 
    void 
    PointPlane::DrawPart<1>()
    {
      const double scale = Part::GetScaleValue();
      Util::Draw::Cone(0.2f*scale,0.2f*scale);
    }

    PointPlane::PointPlane(Link& link)
      :LinkTypeBase(link)
    {
    }

    PointPlane::~PointPlane()
    {
    }    

    template<>
    void 
    PointPlane::BuildPoints<1>(Core::SetOf<PartPoint>& set, Core::DrawableUserItem& item)
    {
      OMC_ASSERT_MSG(set.GetTotItemNumber()==0, "The number of point is not null");
      PartPoint* p1 = new PartPoint(item);
      p1->GetName() = "middle";
      p1->SetScaleCoordinate(0., 0., 0.2);
      set.AddItem(*p1);
    }


    template<> 
    void 
    PointPlane::DrawPart<2>()
    {
      const double scale = Part::GetScaleValue();
      glTranslatef(0.0f, 0.0f, -0.01f*scale);
      Util::Draw::Box(0.6f*scale, 0.6f*scale, 0.02f*scale);
      glTranslatef(0.0f, 0.0f, 0.01f*scale);
    }


    template<>
    void 
    PointPlane::BuildPoints<2>(Core::SetOf<PartPoint>& set, Core::DrawableUserItem& item)
    {
      OMC_ASSERT_MSG(set.GetTotItemNumber()==0, "The number of point is not null");
      PartPoint* p1 = new PartPoint(item);
      p1->GetName() = "middle";
      p1->SetScaleCoordinate(0., 0., -0.01);
      set.AddItem(*p1);
    }



    
  


  }
}




