// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@yakuru.fr>
//
// Copyright (C) 2012-2017 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Item_LinkT_hpp
#define OpenMeca_Item_LinkT_hpp

#include "OpenMeca/Core/AutoRegister.hpp"
#include "OpenMeca/Core/UserItemCommonProperty.hpp"
#include "OpenMeca/Core/ItemCommonProperty.hpp"
#include "OpenMeca/Item/Link.hpp"
#include "OpenMeca/Item/PartLinkT.hpp"
#include "OpenMeca/Gui/Dialog/DialogLinkT.hpp"
#include "OpenMeca/Util/Color.hpp"
#include "OpenMeca/Core/ItemCommonProperty.hpp"



namespace OpenMeca
{  
  namespace Item
  {
    class Body;

    // The link class that model mechanical link.
    // The 'T' type is used to define the link type, 'LinkT<Revolute>' for example.
    template <class T>
    class LinkT: public Link , public Core::AutoRegister< LinkT<T> >
    {
    
    public:
      static const std::string GetStrType() {return T::GetStrType();}; 
      static const QString GetQStrType() {return T::GetQStrType();}; 

      static void Init();
      static void BuildIconSymbol(QIcon&);
      static void DrawIcon(QIcon& icon, QColor color1, QColor color2);

      typedef typename T::Dialog Dialog;

    public:
      LinkT<T>();
      LinkT<T>(Body&, Body&);
      ~LinkT<T>();

      void BuildChSystem(chrono::ChSystem&);
      template<int N> void UpdatePartIcon(QIcon& icon);

      T& GetLinkType();
      const T& GetLinkType() const;
      virtual void Update();

      Part& GetPart1();
      const Part& GetPart1() const;

      Part& GetPart2();
      const Part& GetPart2() const;

      chrono::ChLink& GetChLink();

    private:
      void UpdateIcon();
      Core::SetOfBase<Core::UserItem> GetAssociatedSelectedItem();
      void BuildChSystemInit();
      void BuildDefaultChSystem(chrono::ChSystem&);


      friend class boost::serialization::access;
      template<class Archive> void save(Archive& ar, const unsigned int) const;
      template<class Archive> void load(Archive& ar, const unsigned int);
      BOOST_SERIALIZATION_SPLIT_MEMBER()

    private:
      chrono::ChSharedPtr<typename T::ChLink> chLinkPtr_;
      PartLinkT<T,1>* partBody1_;
      PartLinkT<T,2>* partBody2_;
      T linkType_;
    
    };

    template <class T>  
    template<class Archive>
    inline void
    LinkT<T>::save(Archive& ar, const unsigned int) const
    {
      ar << BOOST_SERIALIZATION_BASE_OBJECT_NVP(Link);
      ar << partBody1_;
      ar << partBody2_;
      ar << linkType_;
    }


    template <class T>  
    template<class Archive>
    inline void
    LinkT<T>::load(Archive& ar, const unsigned int)
    {
      delete partBody1_;
      delete partBody2_;
      partBody1_ = 0;
      partBody2_ = 0;

      ar >> BOOST_SERIALIZATION_BASE_OBJECT_NVP(Link);
      ar >> partBody1_;
      ar >> partBody2_;
      ar >> linkType_;
    }
    

    template <class T>  
    inline void 
    LinkT<T>::DrawIcon(QIcon& icon, QColor color1, QColor color2)
    {
      const QString iconFile = ":/Rsc/Img/Link/" + QString(T::GetStrType().c_str()) + ".svg";
      QSvgRenderer svgRenderer(Util::Icon::ChangeColorInSvgFile(iconFile,
			       Qt::red, color1, Qt::blue, color2));
      
      // render it on pixmap and translate to icon
      const int size = Gui::MainWindow::Get().GetIconSize();
      QPixmap pixmap(QSize(size,size));
      pixmap.fill(QColor (0, 0, 0, 0));
      QPainter painter(&pixmap);
      svgRenderer.render( &painter);
      icon =  QIcon(pixmap);
    }


    

    template <class T> 
    inline void 
    LinkT<T>::Init()
    {
      Core::Singleton< Core::UserItemCommonProperty< LinkT<T> > >::Get().CreateAction_All();
    }
     

      
    template <class T> 
    inline
    LinkT<T>::LinkT()
      :Link(LinkT<T>::GetStrType()),
       chLinkPtr_(0),
       partBody1_(new PartLinkT<T,1>(*this)),
       partBody2_(new PartLinkT<T,2>(*this)),
       linkType_(*this)
    {
    }

    template <class T> 
    inline
    LinkT<T>::LinkT(Body& body1, Body& body2)
    :Link(LinkT<T>::GetStrType(), body1, body2),
     chLinkPtr_(0),
     partBody1_(new PartLinkT<T,1>(*this)),
     partBody2_(new PartLinkT<T,2>(*this)),
     linkType_(*this)
    {
    }

    template <class T> 
    inline
    LinkT<T>::~LinkT()
    {
      // When deleting the chLinkPtr_ attribute, 
      // chronoengine automatically delete the chLinkType_ attribute
    }

    
  template <class T> 
  inline void 
  LinkT<T>::UpdateIcon()
  {
    const QColor color1 = GetBody1().GetColor().GetQColor();
    const QColor color2 = GetBody2().GetColor().GetQColor();
    LinkT<T>::DrawIcon(GetIcon(), color1, color2);
  }

  template <class T> 
  template <int N> 
  inline void 
  LinkT<T>::UpdatePartIcon(QIcon& icon)
  {
    const QColor color1 = GetBody1().GetColor().GetQColor();
    const QColor color2 = GetBody2().GetColor().GetQColor();
    if (N==1)
      LinkT<T>::DrawIcon(icon, color1, Qt::white);
    else if (N==2)
      LinkT<T>::DrawIcon(icon, Qt::white, color2);
    else
      OMC_ASSERT_MSG(0, "This case is forbidden");
  }




    template <class T> 
    inline Core::SetOfBase<Core::UserItem> 
    LinkT<T>::GetAssociatedSelectedItem()
    {
      Core::SetOfBase<Core::UserItem> set;
      set.AddItem(*partBody1_);
      set.AddItem(*partBody2_);
      return set;
    }

    template <class T> 
    inline T& LinkT<T>::GetLinkType()
    {
      return linkType_;
    }

    template <class T> 
    inline const T& LinkT<T>::GetLinkType() const
    {
      return linkType_;
    }

    template <class T> 
    inline void
    LinkT<T>::BuildChSystemInit()
    {
      chLinkPtr_ = chrono::ChSharedPtr<typename T::ChLink>(new typename T::ChLink());
    }

    template <class T> 
    inline void
    LinkT<T>::Update()
    {
      Link::Update();
      partBody1_->ChangeParentTreeItem(Link::GetBody<1>().GetMainTreeItem());
      partBody2_->ChangeParentTreeItem(Link::GetBody<2>().GetMainTreeItem());
      
    }

    template<class T>
    inline void
    LinkT<T>::BuildDefaultChSystem(chrono::ChSystem& chSystem)
    {
      BuildChSystemInit();
      chrono::ChSharedBodyPtr& b1Ptr = GetBody1().GetChBodyPtr();
      chrono::ChSharedBodyPtr& b2Ptr = GetBody2().GetChBodyPtr();
      chLinkPtr_->Initialize(b1Ptr, b2Ptr, GetFrame().ToChCoordsys_Global());
      chSystem.AddLink(chLinkPtr_);
    }

    template<class T>
    inline chrono::ChLink&
    LinkT<T>::GetChLink()
    {
      return *chLinkPtr_.get();
    }


    template<class T>
    inline  void 
    LinkT<T>::BuildIconSymbol(QIcon& icon)
    {
      const QString file = QString(":/Rsc/Img/Link/") + QString(T::GetStrType().c_str()) + QString(".svg");
      QSvgRenderer svgRenderer(file);
      const int size = Gui::MainWindow::Get().GetIconSize();
      QPixmap pixmap(QSize(size,size));
      pixmap.fill(QColor (0, 0, 0, 0));
      QPainter painter(&pixmap);
      svgRenderer.render( &painter);
      icon =  QIcon(pixmap);
    }

    template<class T>
    inline Part& 
    LinkT<T>::GetPart1()
    {
      OMC_ASSERT_MSG(partBody1_ != 0, "The required pointer is null");
      return *partBody1_;
    }

    template<class T>
    inline const Part& 
    LinkT<T>::GetPart1() const
    {
      OMC_ASSERT_MSG(partBody1_ != 0, "The required pointer is null");
      return *partBody1_;
    }

    template<class T>
    inline Part& 
    LinkT<T>::GetPart2()
    {
      OMC_ASSERT_MSG(partBody2_ != 0, "The required pointer is null");
      return *partBody2_;
    }
    
    template<class T>
    inline const Part& 
    LinkT<T>::GetPart2() const
    {
      OMC_ASSERT_MSG(partBody2_ != 0, "The required pointer is null");
      return *partBody2_;
    }

    
  }
}




#endif
