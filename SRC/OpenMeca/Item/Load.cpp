// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@yakuru.fr>
//
// Copyright (C) 2012-2017 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.



#include "OpenMeca/Item/Load.hpp"
#include "OpenMeca/Setting/Simulation.hpp"
#include "OpenMeca/Core/System.hpp"
#include "OpenMeca/Gui/RootTreeItemT.hpp"

#include "Serialization/export.hpp"
BOOST_CLASS_EXPORT(OpenMeca::Item::Load)


namespace OpenMeca
{
  namespace Item
  {    

    const std::string 
    Load::GetStrType()
    {
      return std::string("Load");
    }

    const QString 
    Load::GetQStrType()
    {
      return QObject::tr("Load");
    }

       
    Load::Load(const std::string strType, QTreeWidgetItem& parentTreeItem)
      :Physical(strType, parentTreeItem),
       treeItem_(0)
    {
      QTreeWidgetItem& rootItem = Gui::RootTreeItemT<Load>::Get();
      Core::Item& me = *this;
      treeItem_ = new Gui::SecondaryTreeItem(rootItem, me);
      AddSecondaryTreeItem(*treeItem_);
    }
    
    Load::~Load()
    {      
      delete treeItem_;
    }


    bool 
    Load::IsLoad() const
    {
      return true;
    }
    
    bool 
    Load::IsSensor() const
    {
      return false;
    }


  } 
}
