// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@yakuru.fr>
//
// Copyright (C) 2012-2017 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef _OpenMeca_Item_LoadT_hpp_
#define _OpenMeca_Item_LoadT_hpp_


#include "OpenMeca/Item/Load.hpp"
#include "OpenMeca/Core/UserItemCommonProperty.hpp"
#include "OpenMeca/Util/Icon.hpp"
#include "OpenMeca/Gui/Dialog/DialogLoadT.hpp"

namespace OpenMeca
{
  namespace Item
  {

   

    template<class Parent, class Quantity, class How>
    class LoadT : public Load, 
		  public Core::AutoRegister<LoadT<Parent, Quantity, How> >
    {
    public:  
      static const std::string GetStrType(); 
      static const QString GetQStrType(); 
      static void Init();
      static void DrawIcon(QIcon&, QColor);
      typedef Gui::DialogLoadT<Parent, Quantity, How> Dialog;

    public:      
      LoadT(Parent&);
      ~LoadT();

      void UpdateIcon();
      void DrawShape();
      void BeginDraw();
      void EndDraw();
      Quantity& GetQuantity();
      const Quantity& GetQuantity() const;

      void Apply();
      void BuildChSystem(chrono::ChSystem&);


      void SaveState();
      void ResetState();
      void RecoveryState(unsigned int);
      
      virtual const Geom::Frame<_3D>& GetFrame() const;     

      const Core::AutoRegisteredPtr<Parent, LoadT >& GetParentItemPtr() const;
      Core::AutoRegisteredPtr<Parent, LoadT >& GetParentItemPtr();

      Parent& GetParent();
      const Parent& GetParent() const;

    private:
      friend class boost::serialization::access;
      template<class Archive> void serialize(Archive& ar, const unsigned int version);

    private:
      Core::AutoRegisteredPtr<Parent, LoadT > parentItem_;
      Quantity quantity_;
    };

    template<class Parent, class Quantity, class How>
    template<class Archive>
    inline void
    LoadT<Parent, Quantity, How>::serialize(Archive& ar, const unsigned int)
    {
      ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(Load);
      ar & BOOST_SERIALIZATION_NVP(quantity_);
    }

    
    template<class Parent, class Quantity, class How>
    inline const std::string 
    LoadT<Parent, Quantity, How>::GetStrType()
    {
      return Load::GetStrType() + "@" + Quantity::GetStrType();
    }


    template<class Parent, class Quantity, class How>
    inline const QString
    LoadT<Parent, Quantity, How>::GetQStrType()
    {
      return Load::GetQStrType() + "@" + Quantity::GetQStrType();
    }


    template<class Parent, class Quantity, class How>
    inline void
    LoadT<Parent, Quantity, How>::Init()
    {
      Core::UserItemCommonProperty< LoadT<Parent, Quantity, How> > & prop = 
	Core::Singleton< Core::UserItemCommonProperty< LoadT<Parent, Quantity, How> > >::Get();

      prop.CreateAction_Edit();
      prop.CreateAction_Delete();

      // don't forget to instanciate UserRootItemCommonProperty<Body> before calling
      // create action
      Core::Singleton< Core::UserItemCommonProperty<Parent> >::Instanciate();
      prop.template CreateAction_NewWithAutomaticSelection<Parent>();
    }
    
    template <class Parent, class Quantity, class How>
    inline void 
    LoadT<Parent, Quantity, How>::DrawIcon(QIcon& icon, QColor color)
    {
      // Read svg icon file
      QString svgFileName = ":/Rsc/Img/Load/" + QString(Quantity::GetStrType().c_str()) + ".svg";
      Util::Icon::DrawIconFromSvgFile(svgFileName, icon, color);
    }


    template<class Parent, class Quantity, class How> 
    inline 
    LoadT<Parent, Quantity, How>::LoadT(Parent& parent)
      :Load(GetStrType(), parent.GetMainTreeItem()), 
       parentItem_(*this, parent),
       quantity_(*this)
    {
      UpdateIcon();
      Load::treeItem_->Update();
    }

    template<class Parent, class Quantity, class How> 
    inline 
    LoadT<Parent, Quantity, How>::~LoadT()
    {
    }

    template<class Parent, class Quantity, class How> 
    inline void 
    LoadT<Parent, Quantity, How>::UpdateIcon()
    {
      DrawIcon(GetIcon(), GetColor().GetQColor());
    }

    template<class Parent, class Quantity, class How> 
    inline Quantity& 
    LoadT<Parent, Quantity, How>::GetQuantity()
    {
      return quantity_;
    }

    template<class Parent, class Quantity, class How> 
    inline const Quantity& 
    LoadT<Parent, Quantity, How>::GetQuantity() const
    {
      return quantity_;
    }


    template<class Parent, class Quantity, class How> 
    inline void 
    LoadT<Parent, Quantity, How>::DrawShape()
    {
      quantity_.Draw();
    }

    template<class Parent, class Quantity, class How> 
    inline void 
    LoadT<Parent, Quantity, How>::BeginDraw()
    {
      quantity_.BeginDraw();
    }

    template<class Parent, class Quantity, class How> 
    inline void 
    LoadT<Parent, Quantity, How>::EndDraw()
    {
      quantity_.EndDraw();
    }


    template<class Parent, class Quantity, class How>
    inline void 
    LoadT<Parent, Quantity, How>::Apply() 
    { 
      How::Apply(*this);
    }

    template<class Parent, class Quantity, class How>
    inline void 
    LoadT<Parent, Quantity, How>::BuildChSystem(chrono::ChSystem&)
    {
      Apply();
    }

    template<class Parent, class Quantity, class How>
    inline void 
    LoadT<Parent, Quantity, How>::SaveState()
    {
      quantity_.GetDataType().SaveState();
    }

    template<class Parent, class Quantity, class How>
    inline void 
    LoadT<Parent, Quantity, How>::ResetState()
    {
      quantity_.GetDataType().ResetState();
    }

    template<class Parent, class Quantity, class How>
    inline void 
    LoadT<Parent, Quantity, How>::RecoveryState(unsigned int i)
    {
      quantity_.GetDataType().RecoveryState(i);
    }

    template<class Parent, class Quantity, class How>
    inline const Geom::Frame<_3D>& 
    LoadT<Parent, Quantity, How>::GetFrame() const
    {
      return GetParent().GetFrame();
    }

  

    template<class Parent, class Quantity, class How>
    inline const Core::AutoRegisteredPtr<Parent, LoadT<Parent, Quantity, How> >& 
    LoadT<Parent, Quantity, How>::GetParentItemPtr() const
    {
      return parentItem_;
    }

    
    template<class Parent, class Quantity, class How>
    inline Core::AutoRegisteredPtr<Parent, LoadT<Parent, Quantity, How> >& 
    LoadT<Parent, Quantity, How>::GetParentItemPtr()
      {
      return parentItem_;
    }


    template<class Parent, class Quantity, class How>
    inline Parent& 
    LoadT<Parent, Quantity, How>::GetParent()
    {
      return *GetParentItemPtr().GetPtr();
    }
    
    template<class Parent, class Quantity, class How>
    inline const Parent& 
    LoadT<Parent, Quantity, How>::GetParent() const
    {
      return *GetParentItemPtr().GetPtr();      
    }


  }
} 


namespace boost 
{ 
  namespace serialization 
  {
    
    template<class Archive, class Parent, class Quantity, class How>
    inline void save_construct_data(Archive & ar, 
				    const OpenMeca::Item::LoadT<Parent, Quantity, How> * t, 
				    const unsigned int)
    {
      const Parent* parent = &t->GetParent();
      ar << parent;
    }
    

    template<class Archive, class Parent, class Quantity, class How>
    inline void load_construct_data(Archive & ar, 
				    OpenMeca::Item::LoadT<Parent, Quantity, How> * t, 
				    const unsigned int)
    {
      Parent* parent = 0;
      ar >> parent;
      ::new(t)OpenMeca::Item::LoadT<Parent, Quantity, How>(*parent);
    }
  }
} // namespace ...


#endif
