// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@yakuru.fr>
//
// Copyright (C) 2012-2017 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Item_PartUserJunction_hpp
#define OpenMeca_Item_PartUserJunction_hpp


#include "OpenMeca/Item/PartUser.hpp"
#include "OpenMeca/Core/AutoRegisteredPtr.hpp"
#include "OpenMeca/Core/AutoRegister.hpp"
#include "OpenMeca/Gui/Dialog/DialogPartUserJunction.hpp"
#include "OpenMeca/Item/PartUser.hpp"
#include "OpenMeca/Item/PartPoint.hpp"

namespace OpenMeca
{  

  namespace Item
  {

    class PartPoint;

    // A junction is a pipe between two points
    class PartUserJunction: public PartUser, public Core::AutoRegister<PartUserJunction> 
    {
    public:
      static const std::string GetStrType() { return std::string("Junction");};
      static const QString GetQStrType() { return QObject::tr("Junction");};

      static void Init();
      static void DrawIcon(QIcon&, QColor);
      typedef Gui::DialogPartUserJunction Dialog;


    public:
      PartUserJunction(PartPoint& point);
      virtual ~PartUserJunction();
      void DrawShape();
      void UpdateIcon();
      void BeginDraw();


      // Accessor 
      OMC_ACCESSOR(StartPoint , PartPoint,  startPoint_);

      PartPoint& GetEndPoint();
      const PartPoint& GetEndPoint() const ;

      Core::AutoRegisteredPtr<PartPoint, PartUserJunction >& GetEndPointPtr();
      const Core::AutoRegisteredPtr<PartPoint, PartUserJunction >& GetEndPointPtr() const;


    private:
      PartUserJunction(); //Not allowed, just for serialization
      PartUserJunction(const PartUserJunction&);             //Not Allowed
      PartUserJunction& operator=(const PartUserJunction&);  //Not Allowed


      friend class boost::serialization::access;
      template<class Archive> void save(Archive& ar, const unsigned int) const;
      template<class Archive> void load(Archive& ar, const unsigned int);
      BOOST_SERIALIZATION_SPLIT_MEMBER()

    private:
      PartPoint& startPoint_;
      double length_;
      Core::AutoRegisteredPtr<PartPoint, PartUserJunction > endPoint_;
    };

    
    template<class Archive>
    inline void 
    PartUserJunction::save(Archive& ar, const unsigned int) const
    {
      ar << BOOST_SERIALIZATION_BASE_OBJECT_NVP(PartUser);
      ar << BOOST_SERIALIZATION_NVP(endPoint_);
    }
    
    template<class Archive>
    inline void 
    PartUserJunction::load(Archive& ar, const unsigned int)
    {
      ar >> BOOST_SERIALIZATION_BASE_OBJECT_NVP(PartUser);
      ar >> BOOST_SERIALIZATION_NVP(endPoint_);
    }


  }

}


namespace boost 
{ 
  namespace serialization 
  {
    template<class Archive>
    inline void save_construct_data(Archive & ar, 
				    const OpenMeca::Item::PartUserJunction * t, 
				    const unsigned int)
    {
      const OpenMeca::Item::PartPoint* parent = &t->GetStartPoint();
      ar << parent;
    }
    
    template<class Archive>
    inline void load_construct_data(Archive & ar, 
				    OpenMeca::Item::PartUserJunction * t, 
				    const unsigned int)
    {
      OpenMeca::Item::PartPoint* parent = 0;
      ar >> parent;
      ::new(t)OpenMeca::Item::PartUserJunction(*parent);
    }
  }
} // namespace ...

#endif
