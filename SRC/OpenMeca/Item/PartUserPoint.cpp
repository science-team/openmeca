// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@yakuru.fr>
//
// Copyright (C) 2012-2017 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "OpenMeca/Item/PartUserPoint.hpp"
#include "OpenMeca/Core/System.hpp"
#include "OpenMeca/Core/UserRootItemCommonProperty.hpp"

#include "OpenMeca/Item/PartPoint_CreateAction_SpecializedT.hpp"

#include "Serialization/export.hpp"
BOOST_CLASS_EXPORT(OpenMeca::Item::PartUserPoint)

namespace OpenMeca
{  
  namespace Core
  {

    template<>
    void 
    UserItemCommonProperty<OpenMeca::Item::PartUserPoint>::CreateAction_Specialized() 
    {
      AddPopUpSeparator();
      
      // We call PartPoint_CreateAction_SpecializedT because it is needed by other class
      UserItemCommonProperty<OpenMeca::Item::PartUserPoint>& me = *this;
      OpenMeca::Item::PartPoint_CreateAction_SpecializedT(me);
    }

  }
}


namespace OpenMeca
{  
  namespace Item
  {


    void
    PartUserPoint::Init()
    {
      Core::UserItemCommonProperty< PartUserPoint >& prop = 
	Core::Singleton< Core::UserItemCommonProperty< PartUserPoint > >::Get();

      prop.CreateAction_Edit();
      prop.CreateAction_Delete();
      
      // don't forget to instanciate UserRootItemCommonProperty<Body> before calling
      // create action
      Core::Singleton< Core::UserRootItemCommonProperty<OpenMeca::Item::Body> >::Instanciate();
      prop.CreateAction_NewWithAutomaticSelection<OpenMeca::Item::Body>();
     
      prop.CreateAction_Specialized();
    }


    void 
    PartUserPoint::DrawIcon(QIcon& icon, QColor color)
    {
      PartPoint::DrawIcon(icon, color);
    }

    
    PartUserPoint::PartUserPoint(Core::UserItem& parent)
      :PartPoint(GetStrType(), parent),
       centerExpr_(std::bind(&PartUser::GetReferenceFrame, std::ref(*this)))
    {
    }

    PartUserPoint::~PartUserPoint()
    {
    }

    void 
    PartUserPoint::Update()
    {
      PartPoint::Update();
      GetPoint() = centerExpr_;
    }

  }
}




