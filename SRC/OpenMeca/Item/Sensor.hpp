// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@yakuru.fr>
//
// Copyright (C) 2012-2017 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef _OpenMeca_Item_Sensor_hpp_
#define _OpenMeca_Item_Sensor_hpp_


#include "OpenMeca/Item/Physical.hpp"

namespace OpenMeca
{
  namespace Item
  {


    class Sensor : public Physical, public Core::AutoRegister<Sensor>
    {

    public:
      static const std::string GetStrType(); 
      static const QString GetQStrType(); 
      static void AcquireAll(); 
   
    public:
      Sensor(const std::string strType, QTreeWidgetItem& parentTreeItem);
      virtual ~Sensor();

      virtual void Acquire() = 0;

      bool IsLoad() const;
      bool IsSensor() const;

    private:
      friend class boost::serialization::access;
      template<class Archive> void serialize(Archive& ar, const unsigned int version);

    protected:
      Gui::SecondaryTreeItem* treeItem_;

    };
    

    template<class Archive>
    inline void
    Sensor::serialize(Archive& ar, const unsigned int)
    {
      ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(Physical);
    }

  }
} 

#endif
