// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@yakuru.fr>
//
// Copyright (C) 2012-2017 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <QWidget>

#include "OpenMeca/Item/Shape/Cylinder.hpp"
#include "OpenMeca/Item/PartUserShapeT.hpp"
#include "OpenMeca/Item/Body.hpp"
#include "OpenMeca/Util/Draw.hpp"

#include "Serialization/export.hpp"
BOOST_CLASS_EXPORT(OpenMeca::Item::PartUserShapeT<OpenMeca::Item::Shape::Cylinder>)



namespace OpenMeca
{
  namespace Item
  {
    namespace Shape
    {
    
      const std::string
      Cylinder::GetStrType()
      {
	return "Cylinder";
      }

      const QString
      Cylinder::GetQStrType()
      {
	return QObject::tr("Cylinder");
      }
      

      Cylinder::Cylinder(PartUser& part)
	:ShapeBase(part),
	 radius_(0.),
	 length_(0.),
	 axis_(1., 0., 0., std::bind(&PartUser::GetReferenceFrame, std::ref(part)))
      {
      }
   
      Cylinder::~Cylinder()
      {
      }
    

      void 
      Cylinder::BeginDraw()
      {
	Geom::Quaternion<_3D>& q = GetPart().GetQuaternion();
	const Geom::Vector<_3D>& X = GetPart().GetReferenceFrame().GetXAxis();
	q.SetVecFromTo(X,axis_);
      }

      void 
      Cylinder::Draw() const
      {
	Util::Draw::Cylinder(radius_, length_);
      }


      void 
      Cylinder::BuildChSystem(chrono::ChSystem&)
      {
	Body& body = GetPart().GetBody();
	Geom::Frame<_3D>&(Body::*fnptr)() = &Body::GetFrame;
	std::function<const Geom::Frame<_3D>& ()> f = std::bind(fnptr, std::ref(body));

	Geom::Point<_3D> p(GetPart().GetPoint(), f);
	chrono::ChVector<> chp = body.ExpressPointInLocalChFrame(p);
	
	//
	Geom::Quaternion<_3D> q(GetPart().GetQuaternion());
	const Geom::Vector<_3D>& Y = GetPart().GetReferenceFrame().GetYAxis();
	q.SetVecFromTo(Y,axis_);
	chrono::ChMatrix33<> chm = q.ToRotationMatrix().ToChMatrix();

	chrono::ChSharedBodyPtr& chbody = body.GetChBodyPtr();
	chbody->GetCollisionModel()->AddCylinder(radius_, radius_, length_, &chp, &chm);
	chbody->SetCollide(true);
      }
    
    }
  }
}
