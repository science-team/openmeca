## This file is part of OpenMeca, an easy software to do mechanical simulation.
##
## Author(s)    :  - Damien ANDRE  <openmeca@yakuru.fr>
##
## Copyright (C) 2012-2017 Damien ANDRE
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.



TEMPLATE  = app
TARGET    = ./BUILD/openmeca
DESTDIR   = ./
CONFIG   += qt silent


QMAKE_CXXFLAGS += -Wno-unused-local-typedefs

QT +=  help svg xml opengl sql

TRANSLATIONS = ./Rsc/Lang/openmeca_fr.ts

MOC_DIR     = ./BUILD/moc
UI_DIR      = ./BUILD/ui
OBJECTS_DIR = ./BUILD/obj
RCC_DIR     = ./BUILD/rcc


DEPENDPATH += ../../
 
INCLUDEPATH = ./ ../../ ../ \
              ./BUILD/ui \
              ../ChronoEngine/ \
              ../ChronoEngine/collision/bullet \
              ../ChronoEngine/collision/gimpact \
              ../QGLViewer/BUILD/include

LIBS      +=  ../ChronoEngine/BUILD/libchronoengine.a \
              ../Serialization/BUILD/libserialization.a \
              -lqwt-qt5 -lQGLViewer

CONFIG += c++11
              
win32 {
LIBS   += -lglu32
CONFIG += release
}

unix:!macx {
LIBS   += -lGL -lGLU
CONFIG += release
}

macx {
       QMAKE_CXXFLAGS += -DOSX
       QMAKE_CXXFLAGS += -ftemplate-depth=1024
               CONFIG += release
                 ICON  = ./Rsc/Img/OpenMeca.icns
             TARGET    = ./BUILD/OpenMeca

}

RESOURCES += ./Resources.qrc
SOURCES += Main.cpp

include(./Core/Core.pro)
include(./Physic/Physic.pro)
include(./Util/Util.pro)
include(./Geom/Geom.pro)
include(./Gui/Gui.pro)
include(./Item/Item.pro)
include(./Setting/Setting.pro)
