// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@yakuru.fr>
//
// Copyright (C) 2012-2017 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <QWidget>

#include "OpenMeca/Physic/AngularVelocity.hpp"
#include "OpenMeca/Util/Dimension.hpp"
#include "OpenMeca/Core/System.hpp"

#include "Serialization/export.hpp"
//Don't forget to export for dynamic serialization of child class
BOOST_CLASS_EXPORT(OpenMeca::Physic::AngularVelocity)

namespace OpenMeca
{  
  namespace Physic
  {

    const Util::Color AngularVelocity::color = Util::Color::Green;
    
    const std::string
    AngularVelocity::GetStrType()
    {
      return "AngularVelocity";
    }

    const QString
    AngularVelocity::GetQStrType()
    {
      return QObject::tr("AngularVelocity");
    }
      

    AngularVelocity::AngularVelocity(Item::Physical& item)
      :QuantityT<Vector3D>(item, color),
       unit_(Util::Dimension::Get("AngularVelocity").GetUnit("RadianPerSecond"))
    {
      
    }
   
    AngularVelocity::~AngularVelocity()
    {
    }

 
    const Util::Unit& 
    AngularVelocity::GetUnit() const
    {
      return unit_;
    }
    
    double
    AngularVelocity::GetScale() const
    {
      return Core::System::Get().GetScales().GetScaleValue(AngularVelocity::GetStrType());
    }



  }
}
