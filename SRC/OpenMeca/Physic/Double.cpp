// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@yakuru.fr>
//
// Copyright (C) 2012-2017 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <functional>

#include "OpenMeca/Physic/Double.hpp"
#include "OpenMeca/Gui/MainPlotWindow.hpp"

#include "Serialization/export.hpp"
//Don't forget to export for dynamic serialization of child class
BOOST_CLASS_EXPORT(OpenMeca::Physic::Double)

namespace OpenMeca
{
  namespace Physic
  {

    Double::Double(Quantity& q)
      :TypeT<double>(q),
       val_(0.), 
       storedVal_()
    {
    }

    Double::~Double()
    {
    }

    double& 
    Double::GetRealType()
    {
      return val_;
    }
    
    const double& 
    Double::GetRealType() const
    {
      return val_;
    }
    
    void
    Double::SaveState()
    {
      Update(val_);
    }

    void
    Double::Update()
    {
      storedVal_.push_back(0.);
    }


    void
    Double::Update(const double& v)
    {
      val_ = v;
      storedVal_.push_back(v);
    }

    void
    Double::ResetState()
    {
      storedVal_.clear();
    }

    void
    Double::RecoveryState(unsigned int i)
    {
      val_ = storedVal_[i];
    }

    void 
    Double::FillDataMenu(QMenu* menu)
    {
      QAction* Val = menu->addAction(Gui::MainPlotWindow::tr("Value"));
      QObject::connect(Val, SIGNAL(triggered()), this, SLOT(Val()));
    }

    void 
    Double::Val()
    {
      std::string label = GetLabel();
      Gui::MainPlotWindow::Get().AddData(storedVal_, label.c_str(), GetUnit(), *this);
    }

   
    void 
    Double::WriteHeaderDataFile(std::ofstream& file)
    {
      file << GetLabel() << "(" <<  GetUnit().GetSymbol() << ")";
    }

    void 
    Double::WriteDataFile(std::ofstream& file, unsigned int iter)
    {
      OMC_ASSERT_MSG(iter < storedVal_.size(), "The data are not coherent");
      file << storedVal_[iter];
    }

    void 
    Double::Draw() const
    {
      //Nothing
    }


  

  }
} 

