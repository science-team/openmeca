// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@yakuru.fr>
//
// Copyright (C) 2012-2017 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <QWidget>

#include "OpenMeca/Physic/MechanicalAction.hpp"
#include "OpenMeca/Util/Dimension.hpp"
#include "OpenMeca/Core/System.hpp"
#include "OpenMeca/Item/Link.hpp"

#include "Serialization/export.hpp"
//Don't forget to export for dynamic serialization of child class
BOOST_CLASS_EXPORT(OpenMeca::Physic::MechanicalAction)

namespace OpenMeca
{  
  namespace Physic
  {

    const std::string
    MechanicalAction::GetStrType()
    {
      return "MechanicalAction";
    }

    const QString
    MechanicalAction::GetQStrType()
    {
      return QObject::tr("MechanicalAction");
    }
      

    MechanicalAction::MechanicalAction(Item::Physical& item, const Util::Color& color)
      :QuantityT<Vector3D>(item, color),
       value_(std::bind(&MechanicalAction::GetFrame, std::ref(*this))),
       chForcePtr_(0),
       directionMode_(MechActionEnum::ABSOLUTE_DIR),
       startPointMode_(MechActionEnum::BODY_COORDINATE_POS)
    {
      
    }
    
    MechanicalAction::~MechanicalAction()
    {

    }
    
    
    Geom::Vector<_3D>& 
    MechanicalAction::GetValue()
    {
      return value_; 
    }
    
    const Geom::Vector<_3D>& 
    MechanicalAction::GetValue() const
    {
      return value_;
    }
    

    Geom::Point<_3D>
    MechanicalAction::GetStartPoint()
    {
      const Geom::Point<_3D>& p = GetPhysicalItem().GetFrame().GetCenter();
      Item::Body& body = GetBody();
      Geom::Frame<_3D>&(Item::Body::*fnpt)() = &Item::Body::GetFrame;
      std::function<const Geom::Frame<_3D>& ()> f_body = std::bind(fnpt, std::ref(body));
      return Geom::Point<_3D> (p, f_body);
    }


    MechActionEnum::DirectionMode& 
    MechanicalAction::GetDirectionMode()
    {
      return directionMode_;
    }
    

    const MechActionEnum::DirectionMode& 
    MechanicalAction::GetDirectionMode() const
    {
      return directionMode_;
    }

    const Geom::Frame<_3D>& 
    MechanicalAction::GetFrame() const
    {
      if (directionMode_ == MechActionEnum::BODY_COORDINATE_DIR)
	return GetBody().GetFrame();
      
      return Geom::Frame<_3D>::Global;	  
    }

    Item::Body& 
    MechanicalAction::GetBody()
    {
      return GetPhysicalItem().GetBody();
    }

    const Item::Body& 
    MechanicalAction::GetBody() const
    {
      return GetPhysicalItem().GetBody();
    }


    void 
    MechanicalAction::Apply()
    {
      Geom::Vector<_3D> v = value_;
      
      chForcePtr_ = chrono::ChSharedForcePtr(new chrono::ChForce);
      GetBody().GetChBodyPtr()->AddForce(chForcePtr_);
      
      Geom::Point<_3D> p(GetStartPoint(), &Geom::Frame<_3D>::GetGlobal);
      chForcePtr_->SetVpoint(p.ToChVector());
      chForcePtr_->SetFrame(startPointMode_);

      chForcePtr_->SetMforce(value_.GetNorm());
      chForcePtr_->SetAlign(directionMode_);

      if (directionMode_ == MechActionEnum::BODY_COORDINATE_DIR)
	chForcePtr_->SetRelDir(value_.Unit().ToChVector());
      else
      chForcePtr_->SetDir(value_.Unit().ToChVector());

      CompleteChForce(chForcePtr_);

      GetRealDataType().GetRealType().ResetState();
      GetRealDataType().GetRealType() = value_;
    }
    
    void 
    MechanicalAction::Draw()
    {
      
    }

    void 
    MechanicalAction::BeginDraw()
    {
      if (GetPhysicalItem().IsLoad())
	{
	  GetColor().ApplyGLColor();
	  const Geom::Point<_3D> p(GetPhysicalItem().GetFrame().GetCenter(), &Geom::Frame<_3D>::GetGlobal);
	  const Geom::Vector<_3D> v(value_, &Geom::Frame<_3D>::GetGlobal);
	  glTranslated(p[0], p[1], p[2]);
	  v.Draw(GetScale());
	  glTranslated(-p[0], -p[1], -p[2]);  
	  return;
	}
	  
      if (GetPhysicalItem().IsSensor())
	{
	  Core::UserItem& item = GetPhysicalItem().GetParent();

	  Item::Link* link = 0;
	  link = dynamic_cast<Item::Link*>(&item);
	  OMC_ASSERT_MSG(link != 0,
			 "If the mechanical load come form a sensor the parent must be a link");

	  Item::Body& body = link->GetBody1();

	  Geom::Frame<_3D>&(Item::Body::*fnptr)() = &Item::Body::GetFrame;
	  std::function<const Geom::Frame<_3D>& ()> f = 
	  std::bind(fnptr, std::ref(body));
	  

	  const Geom::Point<_3D>& p_link  = GetPhysicalItem().GetFrame().GetCenter();
	  const Geom::Point<_3D> p_loc(p_link[0], p_link[1], p_link[2], f);
          const Geom::Point<_3D> p(p_loc, Geom::Frame<_3D>::GetGlobal);
	  glTranslatef(p[0], p[1], p[2]);
	  data_.Draw();
          glTranslatef(-p[0], -p[1], -p[2]);
	}
      
    }


  }
}
