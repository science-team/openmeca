// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@yakuru.fr>
//
// Copyright (C) 2012-2017 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Physic_MechanicalAction_hpp
#define OpenMeca_Physic_MechanicalAction_hpp

#include "OpenMeca/Physic/QuantityT.hpp"
#include "OpenMeca/Physic/Vector3D.hpp"
#include "OpenMeca/Geom/Point.hpp"

#include "OpenMeca/Item/Physical.hpp"
#include "OpenMeca/Item/Body.hpp"
#include "OpenMeca/Physic/PhysEnum.hpp"

#include "OpenMeca/Gui/Dialog/Physic/DialogMechanicalAction.hpp"

namespace OpenMeca
{
  namespace Physic
  {
    
    class MechanicalAction : public QuantityT<Vector3D>
    {
    public:  
      static const std::string GetStrType();
      static const QString GetQStrType();
      typedef OpenMeca::Gui::Physic::DialogMechanicalAction GuiManager;

    public:
      MechanicalAction(Item::Physical& item, const Util::Color& color);
      ~MechanicalAction();

      virtual void Apply();
      
      Geom::Vector<_3D>& GetValue();
      const Geom::Vector<_3D>& GetValue() const;

      MechActionEnum::DirectionMode& GetDirectionMode();
      const MechActionEnum::DirectionMode& GetDirectionMode() const;

      virtual void Draw();
      virtual void BeginDraw();

    protected:
      virtual void CompleteChForce(chrono::ChSharedForcePtr&) = 0;
      const Geom::Frame<_3D>& GetFrame() const;
      Geom::Point<_3D> GetStartPoint();
      Item::Body& GetBody();
      const Item::Body& GetBody() const;


    private:
      friend class boost::serialization::access;
      template<class Archive> void serialize(Archive& ar, const unsigned int version);
      
    private:
      Geom::Vector<_3D> value_;
      chrono::ChSharedForcePtr chForcePtr_;
      MechActionEnum::DirectionMode directionMode_;
      const MechActionEnum::StartPointMode startPointMode_;
    }; 

    
    template<class Archive>
    inline void
    MechanicalAction::serialize(Archive& ar, const unsigned int)
    {
      ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(QuantityT<Vector3D>);
      ar & BOOST_SERIALIZATION_NVP(value_);
      ar & BOOST_SERIALIZATION_NVP(directionMode_);
    }


 
  }
}




#endif
