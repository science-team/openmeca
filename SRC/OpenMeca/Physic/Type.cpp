// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@yakuru.fr>
//
// Copyright (C) 2012-2017 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "OpenMeca/Physic/Type.hpp"
#include "OpenMeca/Physic/Quantity.hpp"
#include "OpenMeca/Gui/MainPlotWindow.hpp"

#include "Serialization/export.hpp"
//Don't forget to export for dynamic serialization of child class
BOOST_CLASS_EXPORT(OpenMeca::Physic::Type)

namespace OpenMeca
{
  namespace Physic
  {

    Type::Type(Quantity& q)
      :quantity_(q)
    {
    }

    Type::~Type()
    {
      Gui::MainPlotWindow::Get().DataDeleted(*this);
    }

    const std::string& 
    Type::GetLabel() const
    {
      return quantity_.GetLabel();
    }

     double 
     Type::GetScale() const
     {
       return quantity_.GetScale();
     }
    
    const Util::Unit&
    Type::GetUnit() const
    {
      return quantity_.GetUnit();
    }

    Quantity& 
    Type::GetQuantity()
    {
      return quantity_;
    }

    const Quantity& 
    Type::GetQuantity() const
    {
      return quantity_;
    }

  }
} 

