// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@yakuru.fr>
//
// Copyright (C) 2012-2017 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <functional>

#include "OpenMeca/Physic/Vector3D.hpp"
#include "OpenMeca/Gui/MainPlotWindow.hpp"

#include "Serialization/export.hpp"
//Don't forget to export for dynamic serialization of child class
BOOST_CLASS_EXPORT(OpenMeca::Physic::Vector3D)

namespace OpenMeca
{
  namespace Physic
  {

    Vector3D::Vector3D(Quantity& q)
      :TypeT<Geom::Vector<_3D> >(q),
       vec_(std::bind(&Quantity::GetFrame, std::ref(q))), 
       x_(), y_(), z_(), norm_()
    {
      const Geom::Frame<_3D>&(Quantity::*fnpt)() const= &Quantity::GetFrame;
      std::function<const Geom::Frame<_3D>& ()> f_body = std::bind(fnpt, std::ref(q));
      

    }

    Vector3D::~Vector3D()
    {
    }

    Geom::Vector<_3D>& 
    Vector3D::GetRealType()
    {
      return vec_;
    }
    
    const Geom::Vector<_3D>& 
    Vector3D::GetRealType() const
    {
      return vec_;
    }
    
    void
    Vector3D::SaveState()
    {
      Update(vec_);
    }

    void
    Vector3D::Update()
    {
      x_.push_back(0);
      y_.push_back(0);
      z_.push_back(0);
      norm_.push_back(0);
      vec_.SaveState();
    }


    void
    Vector3D::Update(const Geom::Vector<_3D>& v)
    {
      vec_ = v;
      x_.push_back(v[0]);
      y_.push_back(v[1]);
      z_.push_back(v[2]);
      norm_.push_back(v.GetNorm());
      vec_.SaveState();
    }

    void
    Vector3D::ResetState()
    {
      vec_.ResetState();
      x_.clear();
      y_.clear();
      z_.clear();
      norm_.clear();
    }

    void
    Vector3D::RecoveryState(unsigned int i)
    {
      vec_.RecoveryState(i);
    }

    void 
    Vector3D::FillDataMenu(QMenu* menu)
    {
      QAction* X = menu->addAction(Gui::MainPlotWindow::tr("X"));
      QAction* Y = menu->addAction(Gui::MainPlotWindow::tr("Y"));
      QAction* Z = menu->addAction(Gui::MainPlotWindow::tr("Z"));
      QAction* N = menu->addAction(Gui::MainPlotWindow::tr("Norm"));
      QObject::connect(X, SIGNAL(triggered()), this, SLOT(X()));
      QObject::connect(Y, SIGNAL(triggered()), this, SLOT(Y()));
      QObject::connect(Z, SIGNAL(triggered()), this, SLOT(Z()));
      QObject::connect(N, SIGNAL(triggered()), this, SLOT(Norm()));
    }

    void 
    Vector3D::X()
    {
      std::string label = GetLabel();
      label += " along X";
      Gui::MainPlotWindow::Get().AddData(x_, label.c_str(), GetUnit(), *this);
    }

    void 
    Vector3D::Y()
    {
      std::string label =  GetLabel();
      label += " along Y";
      Gui::MainPlotWindow::Get().AddData(y_, label.c_str(), GetUnit(), *this);
    }

    void 
    Vector3D::Z()
    {
      std::string label =  GetLabel();
      label += " along Z";
      Gui::MainPlotWindow::Get().AddData(z_, label.c_str(), GetUnit(), *this);
    }

    void 
    Vector3D::Norm()
    {
      std::string label =  GetLabel();
      label = "Norm of " + label;
      Gui::MainPlotWindow::Get().AddData(norm_, label.c_str(), GetUnit(), *this);
    }

    void 
    Vector3D::WriteHeaderDataFile(std::ofstream& file)
    {
      file << GetLabel() << " /X (" <<  GetUnit().GetSymbol() << ")\t"
	   << GetLabel() << " /Y (" <<  GetUnit().GetSymbol() << ")\t"
	   << GetLabel() << " /Z (" <<  GetUnit().GetSymbol() << ")";
    }

    void 
    Vector3D::WriteDataFile(std::ofstream& file, unsigned int iter)
    {
      OMC_ASSERT_MSG(iter < x_.size(), "The data are not coherent");
      OMC_ASSERT_MSG(iter < y_.size(), "The data are not coherent");
      OMC_ASSERT_MSG(iter < z_.size(), "The data are not coherent");
      file << x_[iter] << '\t'
	   << y_[iter] << '\t'
	   << z_[iter];
    }

    void 
    Vector3D::Draw() const
    {
      vec_.Draw(GetScale());
    }


  

  }
} 

