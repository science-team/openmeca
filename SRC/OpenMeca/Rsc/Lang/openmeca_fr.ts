<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>Action</name>
    <message>
        <location filename="../../Core/Action/ActionDeleteUserItem.hpp" line="49"/>
        <source>Delete</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="../../Core/Action/ActionEditSystemSetting.hpp" line="44"/>
        <source>Edit</source>
        <translation>Éditer</translation>
    </message>
    <message>
        <location filename="../../Core/Action/ActionNewUserItem.hpp" line="47"/>
        <source>New</source>
        <translation>Nouveau</translation>
    </message>
</context>
<context>
    <name>DialogCheckDeleteItem</name>
    <message>
        <location filename="../../Gui/Dialog/DialogCheckDeleteItem.ui" line="33"/>
        <source>Are you sure ?</source>
        <translation>Ếtes-vous sûr ?</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogCheckDeleteItem.ui" line="49"/>
        <source>Delete this item with all its child ?</source>
        <translation>Voulez-vous détruire cet élément et tous ses enfants ?</translation>
    </message>
</context>
<context>
    <name>DialogSimulation</name>
    <message>
        <source>Form</source>
        <translation type="obsolete">Fenêtre</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogSimulation.ui" line="20"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
</context>
<context>
    <name>DialogUserItem</name>
    <message>
        <source>Form</source>
        <translation type="obsolete">Fenêtre</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogUserItem.ui" line="77"/>
        <source>Preview</source>
        <translation>Aperçu</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogUserItem.ui" line="109"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogUserItem.ui" line="128"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>GeneralSetting</name>
    <message>
        <location filename="../../Gui/GeneralSetting.ui" line="14"/>
        <source>General setting</source>
        <translation>Préférence d&apos;openmeca</translation>
    </message>
    <message>
        <location filename="../../Gui/GeneralSetting.ui" line="24"/>
        <source>Language</source>
        <translation>Language</translation>
    </message>
    <message>
        <location filename="../../Gui/GeneralSetting.ui" line="30"/>
        <source>Choose your language :</source>
        <translation>Choix du language :</translation>
    </message>
    <message>
        <location filename="../../Gui/GeneralSetting.ui" line="59"/>
        <source>Reset Setting</source>
        <translation>Remise à zéro des préférences</translation>
    </message>
</context>
<context>
    <name>ImageDialog</name>
    <message>
        <location filename="../../Gui/ImageDialog.ui" line="26"/>
        <source>Save image as...</source>
        <translation>Sauver l&apos;image comme...</translation>
    </message>
    <message>
        <location filename="../../Gui/ImageDialog.ui" line="36"/>
        <source>Choose image parameter</source>
        <translation>Choisissez les paramètres de l&apos;image</translation>
    </message>
    <message>
        <location filename="../../Gui/ImageDialog.ui" line="56"/>
        <source>Image quality </source>
        <translation>Qualité de l&apos;image</translation>
    </message>
    <message>
        <location filename="../../Gui/ImageDialog.ui" line="69"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="../../Gui/ImageDialog.ui" line="98"/>
        <source>Image format</source>
        <translation>Format de l&apos;image</translation>
    </message>
</context>
<context>
    <name>MainPlotWindow</name>
    <message>
        <location filename="../../Gui/MainPlotWindow.ui" line="14"/>
        <source>openmeca (plot)</source>
        <translation>openmeca (graph)</translation>
    </message>
    <message>
        <location filename="../../Gui/MainPlotWindow.ui" line="34"/>
        <source>Plot</source>
        <translation>Tracer</translation>
    </message>
    <message>
        <location filename="../../Gui/MainPlotWindow.ui" line="38"/>
        <source>Add data</source>
        <translation>Ajouter des données</translation>
    </message>
    <message>
        <location filename="../../Gui/MainPlotWindow.ui" line="49"/>
        <source>Clear</source>
        <translation>Effacer</translation>
    </message>
    <message>
        <location filename="../../Gui/MainPlotWindow.ui" line="54"/>
        <source>sdqs</source>
        <translation>ssds</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>openMeca</source>
        <translation type="obsolete">openmeca</translation>
    </message>
    <message>
        <location filename="../../Gui/MainWindow.ui" line="58"/>
        <source>Global View</source>
        <translation>Global</translation>
    </message>
    <message>
        <location filename="../../Gui/MainWindow.ui" line="74"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="../../Gui/MainWindow.ui" line="97"/>
        <source>File</source>
        <translation>Fichier</translation>
    </message>
    <message>
        <location filename="../../Gui/MainWindow.ui" line="116"/>
        <source>Edit</source>
        <translation>Édition</translation>
    </message>
    <message>
        <location filename="../../Gui/MainWindow.ui" line="14"/>
        <source>openmeca</source>
        <translation>openmeca</translation>
    </message>
    <message>
        <location filename="../../Gui/MainWindow.ui" line="101"/>
        <source>Example</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Gui/MainWindow.ui" line="127"/>
        <source>ToolBar</source>
        <translation>Bar d&apos;outils</translation>
    </message>
    <message>
        <location filename="../../Gui/MainWindow.ui" line="148"/>
        <source>Save</source>
        <translation>Enregistrer</translation>
    </message>
    <message>
        <location filename="../../Gui/MainWindow.ui" line="157"/>
        <source>Save as...</source>
        <translation>Enregistrer comme...</translation>
    </message>
    <message>
        <location filename="../../Gui/MainWindow.ui" line="166"/>
        <source>Open...</source>
        <translation>Ouvrir...</translation>
    </message>
    <message>
        <location filename="../../Gui/MainWindow.ui" line="175"/>
        <source>Undo</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../../Gui/MainWindow.ui" line="187"/>
        <source>Redo</source>
        <translation>Refaire</translation>
    </message>
    <message>
        <location filename="../../Gui/MainWindow.ui" line="199"/>
        <source>Quit...</source>
        <translation>Quitter....</translation>
    </message>
    <message>
        <location filename="../../Gui/MainWindow.ui" line="204"/>
        <source>Show...</source>
        <translation>Montrer...</translation>
    </message>
    <message>
        <location filename="../../Gui/MainWindow.ui" line="213"/>
        <source>Settings...</source>
        <translation>Préférences...</translation>
    </message>
    <message>
        <location filename="../../Gui/MainWindow.ui" line="222"/>
        <source>New system</source>
        <translation>Nouveau systéme</translation>
    </message>
    <message>
        <location filename="../../Gui/MainWindow.ui" line="227"/>
        <source>sdqs</source>
        <translation type="unfinished">ssds</translation>
    </message>
</context>
<context>
    <name>OpenMeca::Core::Action</name>
    <message>
        <location filename="../../Core/Action/ActionEditGlobalSetting.hpp" line="48"/>
        <location filename="../../Core/Action/ActionEditItem.hpp" line="48"/>
        <source>Edit</source>
        <translation>Éditer</translation>
    </message>
    <message>
        <location filename="../../Core/Action/ActionNewUserItemWithAutomaticSelection.hpp" line="52"/>
        <source>New</source>
        <translation>Nouveau</translation>
    </message>
    <message>
        <location filename="../../Core/Action/ActionNewUserItemWithAutomaticSelection.hpp" line="83"/>
        <source>You must build a </source>
        <translation>Vous devez contruire un(e)</translation>
    </message>
    <message>
        <location filename="../../Core/Action/ActionNewUserItemWithAutomaticSelection.hpp" line="84"/>
        <source> instance first</source>
        <translation> d&apos;abord</translation>
    </message>
    <message>
        <location filename="../../Core/Action/ActionNewUserItemWithAutomaticSelection.hpp" line="85"/>
        <source>Warning</source>
        <translation>Attention</translation>
    </message>
    <message>
        <location filename="../../Core/Action/ActionNewUserItemWithSelection.hpp" line="47"/>
        <source>New </source>
        <translation>Nouveau </translation>
    </message>
    <message>
        <location filename="../../Item/Body.cpp" line="332"/>
        <source>Fix/Unfix</source>
        <translation>Fixer/Libre</translation>
    </message>
    <message>
        <location filename="../../Item/Body.cpp" line="361"/>
        <source>Show/Unshow local frame</source>
        <translation>Montrer/Cacher le repère local</translation>
    </message>
</context>
<context>
    <name>OpenMeca::Gui</name>
    <message>
        <source>New </source>
        <translation type="obsolete">Nouveau </translation>
    </message>
</context>
<context>
    <name>OpenMeca::Gui::DialogBody</name>
    <message>
        <source>Name</source>
        <translation type="obsolete">Nom</translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="obsolete">Couleur</translation>
    </message>
    <message>
        <source>Clamped</source>
        <translation type="obsolete">Bâti</translation>
    </message>
    <message>
        <source>Show local frame</source>
        <translation type="obsolete">Montrer le repère local</translation>
    </message>
    <message>
        <source>Mass</source>
        <translation type="obsolete">Masse</translation>
    </message>
    <message>
        <source>Center</source>
        <translation type="obsolete">Centre de masse</translation>
    </message>
    <message>
        <source>Inertia</source>
        <translation type="obsolete">Tenseur d&apos;inertie</translation>
    </message>
</context>
<context>
    <name>OpenMeca::Gui::DialogGravity</name>
    <message>
        <source>Gravity</source>
        <translation type="obsolete">Gravité</translation>
    </message>
</context>
<context>
    <name>OpenMeca::Gui::DialogLinkGear</name>
    <message>
        <source>Ratio</source>
        <translation type="obsolete">Réduction</translation>
    </message>
    <message>
        <source>Length between axes</source>
        <translation type="obsolete">Longueur inter-axes</translation>
    </message>
    <message>
        <source>Modulus</source>
        <translation type="obsolete">Module</translation>
    </message>
    <message>
        <source>Angle of action</source>
        <translation type="obsolete">Angle d&apos;action</translation>
    </message>
    <message>
        <source>Internal teeth</source>
        <translation type="obsolete">Denture intérieure</translation>
    </message>
</context>
<context>
    <name>OpenMeca::Gui::DialogLinkMotor</name>
    <message>
        <source>Angular velocity</source>
        <translation type="obsolete">Vitesse angulaire</translation>
    </message>
</context>
<context>
    <name>OpenMeca::Gui::DialogLinkPulley</name>
    <message>
        <source>Pinion 1 radius</source>
        <translation type="obsolete">Rayon du pignon 1</translation>
    </message>
    <message>
        <source>Pinion 2 radius</source>
        <translation type="obsolete">Rayon du pignon 2</translation>
    </message>
    <message>
        <source>Length between axes</source>
        <translation type="obsolete">Longueur inter-axes</translation>
    </message>
</context>
<context>
    <name>OpenMeca::Gui::DialogLinkRackPinion</name>
    <message>
        <source>Ratio</source>
        <translation type="obsolete">Réduction</translation>
    </message>
    <message>
        <source>Pinion radius</source>
        <translation type="obsolete">Rayon du pignon</translation>
    </message>
    <message>
        <source>Modulus</source>
        <translation type="obsolete">Module</translation>
    </message>
    <message>
        <source>Angle of action</source>
        <translation type="obsolete">Angle d&apos;action</translation>
    </message>
</context>
<context>
    <name>OpenMeca::Gui::DialogLinkScrew</name>
    <message>
        <source>Step</source>
        <translation type="obsolete">Pas</translation>
    </message>
</context>
<context>
    <name>OpenMeca::Gui::DialogLinkSpring</name>
    <message>
        <source>Stiffness</source>
        <translation type="obsolete">Raideur</translation>
    </message>
    <message>
        <source>Damping factor</source>
        <translation type="obsolete">Amortissement</translation>
    </message>
    <message>
        <source>Free length</source>
        <translation type="obsolete">Longueur à vide</translation>
    </message>
</context>
<context>
    <name>OpenMeca::Gui::DialogPartUserJunction</name>
    <message>
        <source>Name</source>
        <translation type="obsolete">Nom</translation>
    </message>
    <message>
        <source>End Point</source>
        <translation type="obsolete">Point de fin</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation type="obsolete">Attention</translation>
    </message>
    <message>
        <source>Need two points to build a junction</source>
        <translation type="obsolete">Deux points sont requis pour construire un jonction</translation>
    </message>
    <message>
        <source>The end point is the same as the start point</source>
        <translation type="obsolete">Le point de fin est le même que le point de début</translation>
    </message>
</context>
<context>
    <name>OpenMeca::Gui::DialogPartUserPipe</name>
    <message>
        <source>Name</source>
        <translation type="obsolete">Nom</translation>
    </message>
    <message>
        <source>Attached to point</source>
        <translation type="obsolete">Attaché au point</translation>
    </message>
    <message>
        <source>Axis</source>
        <translation type="obsolete">Axe</translation>
    </message>
    <message>
        <source>Length</source>
        <translation type="obsolete">Longueur</translation>
    </message>
</context>
<context>
    <name>OpenMeca::Gui::DialogPartUserPoint</name>
    <message>
        <source>Name</source>
        <translation type="obsolete">Nom</translation>
    </message>
    <message>
        <source>Parent Item</source>
        <translation type="obsolete">Élement parent</translation>
    </message>
    <message>
        <source>Center</source>
        <translation type="obsolete">Centre de masse</translation>
    </message>
</context>
<context>
    <name>OpenMeca::Gui::DialogScene</name>
    <message>
        <source>Draw scene axis</source>
        <translation type="obsolete">Montrer le repère global</translation>
    </message>
    <message>
        <source>Background color</source>
        <translation type="obsolete">Couleur de fond</translation>
    </message>
    <message>
        <source>Scene center</source>
        <translation type="obsolete">Centre de la scène 3D</translation>
    </message>
    <message>
        <source>Scene radius</source>
        <translation type="obsolete">Rayon de la scène 3D</translation>
    </message>
    <message>
        <source>Camera</source>
        <translation type="obsolete">Type de vue</translation>
    </message>
</context>
<context>
    <name>OpenMeca::Gui::DialogSimulation</name>
    <message>
        <source>time step</source>
        <translation type="obsolete">Pas de temps</translation>
    </message>
    <message>
        <source>total simulation time</source>
        <translation type="obsolete">Temps total à simuler</translation>
    </message>
    <message>
        <source>Solver</source>
        <translation type="obsolete">Solveur</translation>
    </message>
    <message>
        <source>Integration</source>
        <translation type="obsolete">Intégrateur</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation type="vanished">Attention</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogSimulation.cpp" line="103"/>
        <source>The time step must be lower than the total simulation time</source>
        <translation>Le pas de temps doit être plus petit que le temps total simulé</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogSimulation.cpp" line="143"/>
        <source>Please stop your simulation first</source>
        <translation type="unfinished">Veuillez arrêter d&apos;abord votre simulation</translation>
    </message>
</context>
<context>
    <name>OpenMeca::Gui::DialogUserItem</name>
    <message>
        <location filename="../../Gui/Dialog/DialogUserItem.cpp" line="119"/>
        <source>OpenMeca</source>
        <translation>openmeca</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogUserItem.cpp" line="119"/>
        <source>The current edited item was deleted.
The dialog will be closed</source>
        <translation>L&apos;élément courant a été détruit. L&apos;action en cours va être annulée.</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogLink.cpp" line="43"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogLink.cpp" line="44"/>
        <source>First body</source>
        <translation>Solide 1</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogLink.cpp" line="45"/>
        <source>Second body</source>
        <translation>Solide 2</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogLink.cpp" line="47"/>
        <source>Center</source>
        <translation>Centre de masse</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogLink.cpp" line="50"/>
        <source>Rotation</source>
        <translation>Rotation</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogLink.cpp" line="52"/>
        <source>Draw local frame</source>
        <translation>Montrer le repère local</translation>
    </message>
</context>
<context>
    <name>OpenMeca::Gui::GeneralSetting</name>
    <message>
        <location filename="../../Gui/GeneralSetting.cpp" line="75"/>
        <source>Information</source>
        <translation>Information</translation>
    </message>
    <message>
        <location filename="../../Gui/GeneralSetting.cpp" line="76"/>
        <source>You must restart openmeca to apply settings</source>
        <translation>Vous devez redémarrer openmeca pour que vos changements soient pris en compte</translation>
    </message>
    <message>
        <location filename="../../Gui/GeneralSetting.cpp" line="91"/>
        <source>Question</source>
        <translation>Question</translation>
    </message>
    <message>
        <source>please remove manually this directory</source>
        <translation type="vanished">Veuillez supprimer manuellement ce répertoire</translation>
    </message>
    <message>
        <source>You are going to reset the setting of openmeca, are you sure ?</source>
        <translation type="vanished">Vous êtes sur le point de supprimer vos préférences, êtes-vous sûr ?</translation>
    </message>
    <message>
        <source>Can&apos;t delete the config directory</source>
        <translation type="vanished">Impossible de supprimer le repertoire de configuration</translation>
    </message>
    <message>
        <source>please remove this directory by hand</source>
        <translation type="vanished">vous devez supprimer ce répertoire à la main</translation>
    </message>
</context>
<context>
    <name>OpenMeca::Gui::MainPlotWindow</name>
    <message>
        <location filename="../../Gui/MainPlotWindow.cpp" line="96"/>
        <source>openmeca (plot)</source>
        <translation>openmeca (graphe)</translation>
    </message>
    <message>
        <location filename="../../Physic/Vector3D.cpp" line="109"/>
        <source>X</source>
        <translation>X</translation>
    </message>
    <message>
        <location filename="../../Physic/Vector3D.cpp" line="110"/>
        <source>Y</source>
        <translation>Y</translation>
    </message>
    <message>
        <location filename="../../Physic/Vector3D.cpp" line="111"/>
        <source>Z</source>
        <translation>Z</translation>
    </message>
    <message>
        <location filename="../../Physic/Vector3D.cpp" line="112"/>
        <source>Norm</source>
        <translation>Norme</translation>
    </message>
    <message>
        <location filename="../../Physic/Double.cpp" line="93"/>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
</context>
<context>
    <name>OpenMeca::Gui::MainWindow</name>
    <message>
        <location filename="../../Gui/MainWindow.cpp" line="128"/>
        <source>Mechanical System</source>
        <translation>Système mécanique</translation>
    </message>
    <message>
        <location filename="../../Gui/MainWindow.cpp" line="427"/>
        <source>Save As</source>
        <translation>Sauver comme</translation>
    </message>
    <message>
        <location filename="../../Gui/MainWindow.cpp" line="483"/>
        <source>OpenMeca Files (*.omc)</source>
        <translation>Fichier openmeca (*.omc)</translation>
    </message>
    <message>
        <location filename="../../Gui/MainWindow.cpp" line="455"/>
        <source>The current action will clear your system, do you want to continue ?</source>
        <translation>Vous allez effacer votre systéme, voulez-vous continuer ?</translation>
    </message>
    <message>
        <location filename="../../Gui/MainWindow.cpp" line="457"/>
        <source>Are you sure ?</source>
        <translation>Ếtes-vous sûr ?</translation>
    </message>
    <message>
        <location filename="../../Gui/MainWindow.cpp" line="483"/>
        <source>Open File</source>
        <translation>Ouvrir un fichier</translation>
    </message>
    <message>
        <location filename="../../Gui/MainWindow.cpp" line="531"/>
        <source>The document has been modified.</source>
        <translation>Le système a été modifié.</translation>
    </message>
    <message>
        <location filename="../../Gui/MainWindow.cpp" line="532"/>
        <source>Do you want to save your changes?</source>
        <translation>Voulez-vous sauvegarder les changements ?</translation>
    </message>
    <message>
        <location filename="../../Gui/MainWindow.cpp" line="569"/>
        <source>Save data as</source>
        <translation>Enregistrer les données</translation>
    </message>
    <message>
        <source>Data file (*.txt)</source>
        <translation type="vanished">Fichier de donnée (*.txt)</translation>
    </message>
    <message>
        <location filename="../../Gui/MainWindow.cpp" line="602"/>
        <source>Welcome to openmeca !</source>
        <translation>Bienvenue dans openmeca !</translation>
    </message>
    <message>
        <location filename="../../Gui/MainWindow.cpp" line="603"/>
        <source>Openmeca is an easy to use software that helps you to build mechanical systems and simulate it</source>
        <translation>Openmeca est un logiciel qui permet de construire et de simuler des systèmes mécaniques</translation>
    </message>
    <message>
        <location filename="../../Gui/MainWindow.cpp" line="606"/>
        <source>OpenMeca is distributed under the free GPL v3 license</source>
        <translation>Openmeca est distribué sous la licence libre GPLv3</translation>
    </message>
    <message>
        <location filename="../../Gui/MainWindow.cpp" line="608"/>
        <source>It means that you are free to use and to distribute it !</source>
        <translation>Cela signifie que vous êtes libre d&apos;utiliser et de redistribuer ce logiciel !</translation>
    </message>
    <message>
        <location filename="../../Gui/MainWindow.cpp" line="611"/>
        <source>Main informations</source>
        <translation>Informations générales</translation>
    </message>
    <message>
        <location filename="../../Gui/MainWindow.cpp" line="615"/>
        <source>About openmeca</source>
        <translation>À propos d&apos;openmeca</translation>
    </message>
    <message>
        <location filename="../../Gui/MainWindow.cpp" line="661"/>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <source>You are loading a file created with an older version of openmeca 
</source>
        <translation type="vanished">Vous êtes en train de charger un fichier créé avec une ancienne version d&apos;openmeca </translation>
    </message>
    <message>
        <source>File version: </source>
        <translation type="vanished">Version du fichier: </translation>
    </message>
    <message>
        <source>Soft version: </source>
        <translation type="vanished">Version d&apos;openmeca: </translation>
    </message>
    <message>
        <source>Warning</source>
        <translation type="vanished">Attention</translation>
    </message>
</context>
<context>
    <name>OpenMeca::Gui::Physic::DialogMechanicalAction</name>
    <message>
        <source>Value</source>
        <translation type="obsolete">Valeur</translation>
    </message>
    <message>
        <source>Direction coordinate</source>
        <translation type="obsolete">Coordonnées de la direction</translation>
    </message>
</context>
<context>
    <name>OpenMeca::Gui::Player</name>
    <message>
        <location filename="../../Gui/Player.cpp" line="35"/>
        <source>Simulated time : </source>
        <translation>Temps simulé</translation>
    </message>
    <message>
        <location filename="../../Gui/Player.cpp" line="36"/>
        <source>Iteration number : </source>
        <translation>Nombre d&apos;itération</translation>
    </message>
    <message>
        <location filename="../../Gui/Player.cpp" line="161"/>
        <source>Save files in directory</source>
        <translation>Enregister les fichiers dans le répertoire</translation>
    </message>
</context>
<context>
    <name>OpenMeca::Gui::PropTree</name>
    <message>
        <source>Property</source>
        <translation type="obsolete">Propriété</translation>
    </message>
    <message>
        <source>Value</source>
        <translation type="obsolete">Valeur</translation>
    </message>
</context>
<context>
    <name>OpenMeca::Gui::Shape::DialogBox</name>
    <message>
        <source>Length along X</source>
        <translation type="obsolete">Longueur suivant X</translation>
    </message>
    <message>
        <source>Length along Y</source>
        <translation type="obsolete">Longueur suivant Y</translation>
    </message>
    <message>
        <source>Length along Z</source>
        <translation type="obsolete">Longueur suivant Z</translation>
    </message>
    <message>
        <source>Rotation</source>
        <translation type="obsolete">Rotation</translation>
    </message>
</context>
<context>
    <name>OpenMeca::Gui::Shape::DialogCylinder</name>
    <message>
        <source>Radius</source>
        <translation type="obsolete">Rayon</translation>
    </message>
    <message>
        <source>Length</source>
        <translation type="obsolete">Longueur</translation>
    </message>
    <message>
        <source>Axis</source>
        <translation type="obsolete">Axe</translation>
    </message>
</context>
<context>
    <name>OpenMeca::Gui::Shape::DialogGround</name>
    <message>
        <source>Axis</source>
        <translation type="obsolete">Axe</translation>
    </message>
</context>
<context>
    <name>OpenMeca::Gui::Shape::DialogSphere</name>
    <message>
        <source>Radius</source>
        <translation type="obsolete">Rayon</translation>
    </message>
</context>
<context>
    <name>Player</name>
    <message>
        <source>Form</source>
        <translation type="obsolete">Fenêtre</translation>
    </message>
    <message>
        <location filename="../../Gui/Player.ui" line="17"/>
        <source>Play/Pause</source>
        <translation>Lecture/Pause</translation>
    </message>
    <message>
        <location filename="../../Gui/Player.ui" line="72"/>
        <source>Stop and reset</source>
        <translation>Arrêt et mise à zéro</translation>
    </message>
    <message>
        <location filename="../../Gui/Player.ui" line="107"/>
        <source>Record image</source>
        <translation>Enregistrer des images</translation>
    </message>
    <message>
        <location filename="../../Gui/Player.ui" line="130"/>
        <source>Current simulation step</source>
        <translation>Pas courant de la simulation</translation>
    </message>
    <message>
        <location filename="../../Gui/Player.ui" line="142"/>
        <source>Iteration number : </source>
        <translation>Nombre d&apos;itération</translation>
    </message>
    <message>
        <location filename="../../Gui/Player.ui" line="149"/>
        <source>Simulated time : </source>
        <translation>Temps simulé</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>You must build a </source>
        <translation type="obsolete">Vous devez contruire un(e)</translation>
    </message>
    <message>
        <source> instance first</source>
        <translation type="obsolete"> d&apos;abord</translation>
    </message>
    <message>
        <location filename="../../Core/Macro.cpp" line="92"/>
        <location filename="../../Gui/Dialog/DialogLink.cpp" line="91"/>
        <location filename="../../Gui/Dialog/DialogLinkT.hpp" line="128"/>
        <location filename="../../Gui/Dialog/DialogPartUserJunction.cpp" line="57"/>
        <location filename="../../Gui/Dialog/DialogPartUserJunction.cpp" line="76"/>
        <source>Warning</source>
        <translation>Attention</translation>
    </message>
    <message>
        <location filename="../../Core/Condition.hpp" line="93"/>
        <source>Must be superior to </source>
        <translation>Doit être supérieur à </translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogBody.cpp" line="45"/>
        <location filename="../../Gui/Dialog/DialogLoadT.hpp" line="73"/>
        <location filename="../../Gui/Dialog/DialogPartUserJunction.cpp" line="38"/>
        <location filename="../../Gui/Dialog/DialogPartUserPipe.cpp" line="39"/>
        <location filename="../../Gui/Dialog/DialogPartUserPoint.cpp" line="38"/>
        <location filename="../../Gui/Dialog/DialogPartUserShapeT.hpp" line="79"/>
        <location filename="../../Gui/Dialog/DialogSensorT.hpp" line="70"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogBody.cpp" line="47"/>
        <source>Color</source>
        <translation>Couleur</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogBody.cpp" line="49"/>
        <source>Clamped</source>
        <translation>Fixer</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogBody.cpp" line="50"/>
        <source>Show local frame</source>
        <translation>Montrer le repère local</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogBody.cpp" line="51"/>
        <source>Show mass center</source>
        <translation>Montrer le centre de masse</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogBody.cpp" line="54"/>
        <source>Mass</source>
        <translation>Masse</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogBody.cpp" line="57"/>
        <location filename="../../Gui/Dialog/DialogPartUserPoint.cpp" line="40"/>
        <source>Center</source>
        <translation>Centre de masse</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogBody.cpp" line="61"/>
        <source>Inertia</source>
        <translation>Tenseur d&apos;inertie</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogBody.cpp" line="64"/>
        <source>Static friction coeficient</source>
        <translation>Coef de friction statique</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogBody.cpp" line="67"/>
        <source>Sliding friction coeficient</source>
        <translation>Coef de friction en glissement</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogGravity.cpp" line="38"/>
        <location filename="../../Setting/Gravity.cpp" line="41"/>
        <source>Gravity</source>
        <translation>Gravité</translation>
    </message>
    <message>
        <source>First body</source>
        <translation type="obsolete">Solide 1</translation>
    </message>
    <message>
        <source>Second body</source>
        <translation type="obsolete">Solide 2</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/Shape/ShapeDialogBox.cpp" line="52"/>
        <source>Rotation</source>
        <translation>Rotation</translation>
    </message>
    <message>
        <source>Draw local frame</source>
        <translation type="obsolete">Montrer le repère local</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogLink.cpp" line="92"/>
        <source>The bodys must be different</source>
        <translation>Les deux solides doivent êtres différents</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogLinkGear.cpp" line="40"/>
        <location filename="../../Gui/Dialog/DialogLinkRackPinion.cpp" line="39"/>
        <source>Ratio</source>
        <translation>Réduction</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogLinkGear.cpp" line="43"/>
        <location filename="../../Gui/Dialog/DialogLinkPulley.cpp" line="44"/>
        <source>Length between axes</source>
        <translation>Longueur inter-axes</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogLinkGear.cpp" line="46"/>
        <location filename="../../Gui/Dialog/DialogLinkRackPinion.cpp" line="45"/>
        <source>Modulus</source>
        <translation>Module</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogLinkGear.cpp" line="49"/>
        <location filename="../../Gui/Dialog/DialogLinkRackPinion.cpp" line="48"/>
        <source>Angle of action</source>
        <translation>Angle d&apos;action</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogLinkGear.cpp" line="52"/>
        <source>Internal teeth</source>
        <translation>Denture intérieure</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogLinkGear.cpp" line="84"/>
        <source>You can&apos;t set a ratio of 100% with internal teeth gear</source>
        <translation>Vous ne pouvez pas mettre un rapport de réduction de 100% avec une denture intérieure </translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogLinkMotor.cpp" line="36"/>
        <source>Angular velocity</source>
        <translation>Vitesse angulaire</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogLinkPulley.cpp" line="38"/>
        <source>Pinion 1 radius</source>
        <translation>Rayon du pignon 1</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogLinkPulley.cpp" line="41"/>
        <source>Pinion 2 radius</source>
        <translation>Rayon du pignon 2</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogLinkRackPinion.cpp" line="42"/>
        <source>Pinion radius</source>
        <translation>Rayon du pignon</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogLinkScrew.cpp" line="36"/>
        <source>Step</source>
        <translation>Pas</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogLinkSpring.cpp" line="41"/>
        <source>Stiffness</source>
        <translation>Raideur</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogLinkSpring.cpp" line="44"/>
        <location filename="../../Gui/Dialog/DialogLinkSpring.cpp" line="47"/>
        <source>Damping factor</source>
        <translation>Amortissement</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogLinkSpring.cpp" line="50"/>
        <source>Free length</source>
        <translation>Longueur à vide</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogLinkSpring.cpp" line="53"/>
        <source>Initial length</source>
        <translation>Longueur initiale</translation>
    </message>
    <message>
        <source>openmeca</source>
        <translation type="obsolete">openmeca</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogLoadT.hpp" line="74"/>
        <location filename="../../Gui/Dialog/DialogSensorT.hpp" line="71"/>
        <source>Parent</source>
        <translation>Parent</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogPartUserJunction.cpp" line="39"/>
        <source>End Point</source>
        <translation>Point de fin</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogPartUserJunction.cpp" line="58"/>
        <source>Need two points to build a junction</source>
        <translation>Deux points sont requis pour construire une jonction</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogPartUserJunction.cpp" line="77"/>
        <source>The end point is the same as the start point</source>
        <translation>Le point de fin est le même que le point de début</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogPartUserPipe.cpp" line="40"/>
        <source>Attached to point</source>
        <translation>Attaché au point</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogPartUserPipe.cpp" line="41"/>
        <location filename="../../Gui/Dialog/Shape/DialogCylinder.cpp" line="49"/>
        <location filename="../../Gui/Dialog/Shape/DialogGround.cpp" line="35"/>
        <source>Axis</source>
        <translation>Axe</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogPartUserPipe.cpp" line="44"/>
        <location filename="../../Gui/Dialog/Shape/DialogCylinder.cpp" line="45"/>
        <source>Length</source>
        <translation>Longueur</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogPartUserPoint.cpp" line="39"/>
        <source>Parent Item</source>
        <translation>Élement parent</translation>
    </message>
    <message>
        <location filename="../../Geom/Point.hpp" line="57"/>
        <location filename="../../Gui/Dialog/DialogPartUserShapeT.hpp" line="80"/>
        <location filename="../../Item/PartUserPoint.hpp" line="40"/>
        <source>Point</source>
        <translation>Point</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogPartUserShapeT.hpp" line="81"/>
        <source>Enable collision detection</source>
        <translation>Activer la collision de detection</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogScene.cpp" line="42"/>
        <source>Draw scene axis</source>
        <translation>Montrer le repère global</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogScene.cpp" line="44"/>
        <source>Background color</source>
        <translation>Couleur de fond</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogScene.cpp" line="46"/>
        <source>Scene center</source>
        <translation>Centre de la scène 3D</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogScene.cpp" line="49"/>
        <source>Scene radius</source>
        <translation>Rayon de la scène 3D</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogScene.cpp" line="52"/>
        <source>Camera</source>
        <translation>Type de vue</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogSimulation.cpp" line="49"/>
        <source>time step</source>
        <translation>Pas de temps</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogSimulation.cpp" line="53"/>
        <source>total simulation time</source>
        <translation>Temps total à simuler</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogSimulation.cpp" line="56"/>
        <source>Solver</source>
        <translation>Solveur</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogSimulation.cpp" line="57"/>
        <source>Integration</source>
        <translation>Intégrateur</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogSimulation.cpp" line="60"/>
        <source>Animation period</source>
        <translation>Période d&apos;animation</translation>
    </message>
    <message>
        <source>The time step must be lower than the total simulation time</source>
        <translation type="obsolete">Le pas de temps doit être plus petit que le temps total simulé</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogVariable.cpp" line="38"/>
        <source>Symbol</source>
        <translation>Symbole</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogVariable.cpp" line="41"/>
        <location filename="../../Gui/Dialog/Physic/DialogMechanicalAction.cpp" line="37"/>
        <location filename="../../Gui/Prop/PropTree.cpp" line="36"/>
        <location filename="../../Gui/Prop/PropTree.cpp" line="50"/>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogVariable.cpp" line="94"/>
        <source>The name of the variable is already used</source>
        <translation>Le nom de la variable est déjà utilisée</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogVariable.cpp" line="101"/>
        <source>It&apos;s not a valid variable name</source>
        <translation>Nom de variable invalide</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/Physic/DialogMechanicalAction.cpp" line="38"/>
        <source>Direction coordinate</source>
        <translation>Direction</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/Shape/ShapeDialogBox.cpp" line="41"/>
        <source>Length along X</source>
        <translation>Longueur suivant X</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/Shape/ShapeDialogBox.cpp" line="45"/>
        <source>Length along Y</source>
        <translation>Longueur suivant Y</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/Shape/ShapeDialogBox.cpp" line="49"/>
        <source>Length along Z</source>
        <translation>Longueur suivant Z</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/Shape/DialogCylinder.cpp" line="41"/>
        <location filename="../../Gui/Dialog/Shape/DialogSphere.cpp" line="38"/>
        <source>Radius</source>
        <translation>Rayon</translation>
    </message>
    <message>
        <source>openmeca (plot)</source>
        <translation type="obsolete">openmeca (graphe)</translation>
    </message>
    <message>
        <source>Mechanical System</source>
        <translation type="obsolete">Système mécanique</translation>
    </message>
    <message>
        <source>About openmeca</source>
        <translation type="obsolete">À propos d&apos;openmeca</translation>
    </message>
    <message>
        <source>You are loading a file created with an older version of openmeca 
</source>
        <translation type="obsolete">Vous êtes en train de charger un fichier créé avec une ancienne version d&apos;openmeca </translation>
    </message>
    <message>
        <source>File version: </source>
        <translation type="obsolete">Version du fichier: </translation>
    </message>
    <message>
        <source>Soft version: </source>
        <translation type="obsolete">Version d&apos;openmeca: </translation>
    </message>
    <message>
        <source>Simulated time : </source>
        <translation type="obsolete">Temps simulé</translation>
    </message>
    <message>
        <source>Iteration number : </source>
        <translation type="obsolete">Nombre d&apos;itération</translation>
    </message>
    <message>
        <location filename="../../Gui/Prop/PropAttitude.cpp" line="114"/>
        <location filename="../../Gui/Prop/PropExprAttitude.cpp" line="115"/>
        <source>The axis can&apos;t be null</source>
        <translation>L&apos;axe ne peut être nul</translation>
    </message>
    <message>
        <location filename="../../Gui/Prop/PropAxis.cpp" line="105"/>
        <source>The axis can not be null</source>
        <translation>L&apos;axe ne peut être nul</translation>
    </message>
    <message>
        <location filename="../../Gui/Prop/PropTree.cpp" line="36"/>
        <location filename="../../Gui/Prop/PropTree.cpp" line="50"/>
        <source>Property</source>
        <translation>Propriété</translation>
    </message>
    <message>
        <source>Fix/Unfix</source>
        <translation type="obsolete">Fixé/Défixé</translation>
    </message>
    <message>
        <source>Show/Unshow local frame</source>
        <translation type="obsolete">Montrer/Cacher le repère local</translation>
    </message>
    <message>
        <source>X</source>
        <translation type="obsolete">X</translation>
    </message>
    <message>
        <source>Y</source>
        <translation type="obsolete">Y</translation>
    </message>
    <message>
        <source>Z</source>
        <translation type="obsolete">Z</translation>
    </message>
    <message>
        <source>Norm</source>
        <translation type="obsolete">Norme</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="obsolete">Supprimer</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="obsolete">Éditer</translation>
    </message>
    <message>
        <location filename="../../Core/SystemSetting.cpp" line="84"/>
        <source>System</source>
        <translation>Système</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogUserItemT.hpp" line="87"/>
        <source>New </source>
        <translation>Nouveau </translation>
    </message>
    <message>
        <location filename="../../Item/Body.hpp" line="63"/>
        <source>Body</source>
        <translation>Solide</translation>
    </message>
    <message>
        <location filename="../../Item/How/SetForce.cpp" line="47"/>
        <source>AbsoluteForce</source>
        <translation>ForceAbsolue</translation>
    </message>
    <message>
        <location filename="../../Item/Link.cpp" line="50"/>
        <source>Link</source>
        <translation>Liaison</translation>
    </message>
    <message>
        <location filename="../../Item/Link/Cylindrical.cpp" line="45"/>
        <source>Cylindrical</source>
        <translation>Pivot glissant</translation>
    </message>
    <message>
        <location filename="../../Item/Link/Gear.cpp" line="48"/>
        <source>Gear</source>
        <translation>Engrenage</translation>
    </message>
    <message>
        <location filename="../../Item/Link/Motor.cpp" line="45"/>
        <source>Motor</source>
        <translation>Moteur</translation>
    </message>
    <message>
        <location filename="../../Item/Link/Planar.cpp" line="48"/>
        <source>Planar</source>
        <translation>AppuiPlan</translation>
    </message>
    <message>
        <location filename="../../Item/Link/PointLine.cpp" line="47"/>
        <source>PointLine</source>
        <translation>Linéaire annulaire</translation>
    </message>
    <message>
        <location filename="../../Item/Link/PointPlane.cpp" line="47"/>
        <source>PointPlane</source>
        <translation>Ponctuelle</translation>
    </message>
    <message>
        <location filename="../../Item/Link/Pulley.cpp" line="48"/>
        <source>Pulley</source>
        <translation>Poulie courroie</translation>
    </message>
    <message>
        <location filename="../../Item/Link/RackPinion.cpp" line="48"/>
        <source>RackPinion</source>
        <translation>Pignon crémaillère</translation>
    </message>
    <message>
        <location filename="../../Item/Link/Revolute.cpp" line="46"/>
        <source>Revolute</source>
        <translation>Pivot</translation>
    </message>
    <message>
        <location filename="../../Item/Link/Screw.cpp" line="48"/>
        <source>Screw</source>
        <translation>Hélioidale</translation>
    </message>
    <message>
        <location filename="../../Item/Link/Slider.cpp" line="46"/>
        <source>Slider</source>
        <translation>Glissière</translation>
    </message>
    <message>
        <location filename="../../Item/Link/Spherical.cpp" line="45"/>
        <source>Spherical</source>
        <translation>Rotule</translation>
    </message>
    <message>
        <location filename="../../Item/Link/Spring.cpp" line="48"/>
        <source>Spring</source>
        <translation>Ressort</translation>
    </message>
    <message>
        <location filename="../../Item/Load.cpp" line="45"/>
        <source>Load</source>
        <translation>Chargement</translation>
    </message>
    <message>
        <location filename="../../Item/Part.cpp" line="40"/>
        <source>Part</source>
        <translation>Pièce</translation>
    </message>
    <message>
        <location filename="../../Item/PartPoint.hpp" line="44"/>
        <source>AutomaticPoint</source>
        <translation>Point automatique</translation>
    </message>
    <message>
        <location filename="../../Item/PartUserJunction.hpp" line="45"/>
        <source>Junction</source>
        <translation>Jonction</translation>
    </message>
    <message>
        <location filename="../../Item/PartUserPipe.hpp" line="45"/>
        <source>Pipe</source>
        <translation>Tuyau</translation>
    </message>
    <message>
        <location filename="../../Item/Sensor.cpp" line="45"/>
        <source>Sensor</source>
        <translation>Capteur</translation>
    </message>
    <message>
        <location filename="../../Item/Shape/Box.cpp" line="47"/>
        <source>Box</source>
        <translation>Pavé</translation>
    </message>
    <message>
        <location filename="../../Item/Shape/Cylinder.cpp" line="48"/>
        <source>Cylinder</source>
        <translation>Cylindre</translation>
    </message>
    <message>
        <location filename="../../Item/Shape/Ground.cpp" line="47"/>
        <source>Ground</source>
        <translation>Bâti</translation>
    </message>
    <message>
        <location filename="../../Item/Shape/Sphere.cpp" line="49"/>
        <source>Sphere</source>
        <translation>Sphère</translation>
    </message>
    <message>
        <location filename="../../Physic/AngularVelocity.cpp" line="46"/>
        <source>AngularVelocity</source>
        <translation>Vitesse angulaire</translation>
    </message>
    <message>
        <location filename="../../Physic/Force.cpp" line="44"/>
        <source>Force</source>
        <translation>Force</translation>
    </message>
    <message>
        <location filename="../../Physic/LinearPosition.cpp" line="46"/>
        <source>LinearPosition</source>
        <translation>Position linéaire</translation>
    </message>
    <message>
        <location filename="../../Physic/LinearVelocity.cpp" line="46"/>
        <source>LinearVelocity</source>
        <translation>Vitesse linéaire</translation>
    </message>
    <message>
        <location filename="../../Physic/MechanicalAction.cpp" line="45"/>
        <source>MechanicalAction</source>
        <translation>Action mécanique</translation>
    </message>
    <message>
        <source>New</source>
        <translation type="obsolete">Nouveau</translation>
    </message>
    <message>
        <location filename="../../Core/Software.cpp" line="119"/>
        <source>Geometry</source>
        <translation>Géométrie</translation>
    </message>
    <message>
        <location filename="../../Core/Software.cpp" line="120"/>
        <source>Cosmetic</source>
        <translation>Habillage</translation>
    </message>
    <message>
        <location filename="../../Core/Software.cpp" line="121"/>
        <source>Loading</source>
        <translation>Chargement</translation>
    </message>
    <message>
        <location filename="../../Core/Software.cpp" line="122"/>
        <source>Plot data...</source>
        <translation>Tracer les données...</translation>
    </message>
    <message>
        <location filename="../../Core/Software.cpp" line="123"/>
        <source>Save data...</source>
        <translation>Sauver les données...</translation>
    </message>
    <message>
        <location filename="../../Core/Software.cpp" line="124"/>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <location filename="../../Core/Software.cpp" line="125"/>
        <source>About...</source>
        <translation>À propos...</translation>
    </message>
    <message>
        <location filename="../../Core/Software.cpp" line="126"/>
        <source>Help...</source>
        <translation>Aide...</translation>
    </message>
    <message>
        <location filename="../../Core/Software.cpp" line="127"/>
        <source>3D viewer help...</source>
        <translation>Aide du viewer 3D...</translation>
    </message>
    <message>
        <location filename="../../Core/Software.cpp" line="130"/>
        <source>Author(s)</source>
        <translation>Autheur(s)</translation>
    </message>
    <message>
        <location filename="../../Core/Software.cpp" line="131"/>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <location filename="../../Core/Software.cpp" line="132"/>
        <source>Licence</source>
        <translation>Licence</translation>
    </message>
    <message>
        <location filename="../../Core/Software.cpp" line="133"/>
        <source>Contact</source>
        <translation>Contact</translation>
    </message>
    <message>
        <location filename="../../Core/Software.cpp" line="134"/>
        <source>Project</source>
        <translation>Projet</translation>
    </message>
    <message>
        <location filename="../../Core/Software.cpp" line="137"/>
        <source>gears</source>
        <translation type="unfinished">engrenage</translation>
    </message>
    <message>
        <location filename="../../Core/Software.cpp" line="138"/>
        <source>gyroscope</source>
        <translation type="unfinished">gyroscope</translation>
    </message>
    <message>
        <location filename="../../Core/Software.cpp" line="139"/>
        <source>reprap</source>
        <translation type="unfinished">reprap</translation>
    </message>
    <message>
        <location filename="../../Core/Software.cpp" line="140"/>
        <source>robot</source>
        <translation type="unfinished">robot</translation>
    </message>
    <message>
        <location filename="../../Core/Software.cpp" line="141"/>
        <source>screw</source>
        <translation type="unfinished">vis-écrou</translation>
    </message>
    <message>
        <location filename="../../Core/Software.cpp" line="142"/>
        <source>steering</source>
        <translation type="unfinished">direction</translation>
    </message>
    <message>
        <location filename="../../Core/Software.cpp" line="143"/>
        <source>cranck-shaft</source>
        <translation type="unfinished">bielle-manivelle</translation>
    </message>
    <message>
        <location filename="../../Setting/Scales.cpp" line="47"/>
        <source>Scales</source>
        <translation>Échelles</translation>
    </message>
    <message>
        <location filename="../../Setting/Scene.cpp" line="42"/>
        <source>Scene</source>
        <translation>Scène</translation>
    </message>
    <message>
        <location filename="../../Setting/Simulation.cpp" line="49"/>
        <source>Simulation</source>
        <translation>Simulation</translation>
    </message>
    <message>
        <location filename="../../Geom/Frame.hpp" line="73"/>
        <source>Frame</source>
        <translation>Repère</translation>
    </message>
    <message>
        <location filename="../../Geom/Matrix.hpp" line="88"/>
        <source>Matrix</source>
        <translation>Matrice</translation>
    </message>
    <message>
        <location filename="../../Geom/Quaternion.hpp" line="68"/>
        <source>Quaternion</source>
        <translation>Quaternion</translation>
    </message>
    <message>
        <location filename="../../Geom/Vector.hpp" line="107"/>
        <source>Vector</source>
        <translation>Vecteur</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogLinkT.hpp" line="125"/>
        <source>You must build 2 bodys before adding a new link</source>
        <translation>Vous devez construire deux solides avant de créer une nouvelle liaison</translation>
    </message>
    <message>
        <location filename="../../Core/Macro.cpp" line="36"/>
        <source>Sorry, openmeca encounters an error</source>
        <translation>Désolé, openmeca rencontre une erreur fatale</translation>
    </message>
    <message>
        <location filename="../../Core/Macro.cpp" line="38"/>
        <source>Error details :</source>
        <translation>Détail de l&apos;erreur :</translation>
    </message>
    <message>
        <location filename="../../Core/Macro.cpp" line="39"/>
        <source>source file :</source>
        <translation>fichier source :</translation>
    </message>
    <message>
        <location filename="../../Core/Macro.cpp" line="40"/>
        <source>line :</source>
        <translation>ligne :</translation>
    </message>
    <message>
        <location filename="../../Core/Macro.cpp" line="41"/>
        <source>condition :</source>
        <translation>condition :</translation>
    </message>
    <message>
        <location filename="../../Core/Macro.cpp" line="44"/>
        <source>message :</source>
        <translation>message :</translation>
    </message>
    <message>
        <location filename="../../Core/Macro.cpp" line="46"/>
        <source>The program may be unstable now, do you really want to continue ?</source>
        <translation>Le programme peut être instable maintenant, voulez vous continuer ?</translation>
    </message>
    <message>
        <source>The program will stop, do you want to try to save your work ?</source>
        <translation type="vanished">Le programme va s&apos;arrêter, vous pouvez essayer de sauvegarder votre travail</translation>
    </message>
    <message>
        <location filename="../../Core/Macro.cpp" line="49"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../../Core/Macro.cpp" line="55"/>
        <source>Do You want to save your work before quitting OpenMeca ?</source>
        <translation>Voulez-vous sauvez votre travail avant de quitter OpenMeca ?</translation>
    </message>
    <message>
        <location filename="../../Core/Macro.cpp" line="56"/>
        <source>Question</source>
        <translation>Question</translation>
    </message>
    <message>
        <location filename="../../Core/Macro.cpp" line="83"/>
        <source>Information</source>
        <translation>Information</translation>
    </message>
    <message>
        <location filename="../../Core/ConfigDirectory.cpp" line="46"/>
        <source>The config file directory of openmeca does not exist, openmeca will build a new config directory &quot;</source>
        <translation>Le répértoire de configuration d&apos;openmeca n&apos;existe pas. Openmeca va construire un nouveau répertoire de configuration</translation>
    </message>
    <message>
        <location filename="../../Gui/MainPlotWindow.cpp" line="196"/>
        <source>One of the data seems to be deleted, the plot will be cleared</source>
        <translation>Une des données semble avoir été détruite. Le graphe va être éffacé.</translation>
    </message>
    <message>
        <location filename="../../Core/System.cpp" line="177"/>
        <source>Can&apos;t save file</source>
        <translation>Impossible de sauver le fichier</translation>
    </message>
    <message>
        <location filename="../../Core/System.cpp" line="195"/>
        <source>Can&apos;t open file</source>
        <translation>Impossible d&apos;ouvrir un fichier</translation>
    </message>
    <message>
        <source>You are loading a file created with an</source>
        <translation type="vanished">Vous ouvrez un fichier créé avec une</translation>
    </message>
    <message>
        <source>older version of openmeca</source>
        <translation type="vanished">version plus ancienne d&apos;OpenMeca</translation>
    </message>
    <message>
        <source>Note that your file version is</source>
        <translation type="vanished">La version du fichier est</translation>
    </message>
    <message>
        <source>and the software version is</source>
        <translation type="vanished">La version d&apos;OpenMeca est</translation>
    </message>
    <message>
        <location filename="../../Gui/Dialog/DialogLinkLinearMotor.cpp" line="36"/>
        <source>Displacement</source>
        <translation>Déplacement</translation>
    </message>
    <message>
        <location filename="../../Gui/Prop/PropMotionLaw.cpp" line="57"/>
        <source>Enabled</source>
        <translation>Activé</translation>
    </message>
    <message>
        <location filename="../../Gui/Prop/PropMotionLaw.cpp" line="61"/>
        <source>Motion Law</source>
        <translation>Loi de mouvement</translation>
    </message>
    <message>
        <location filename="../../Item/How/SetTorque.cpp" line="47"/>
        <source>AbsoluteTorque</source>
        <translation>CoubleAbsolu</translation>
    </message>
    <message>
        <location filename="../../Item/Link/LinearMotor.cpp" line="45"/>
        <source>LinearMotor</source>
        <translation>MoteurLinéaire</translation>
    </message>
    <message>
        <location filename="../../Item/Variable.hpp" line="46"/>
        <source>Variable</source>
        <translation>Variable</translation>
    </message>
    <message>
        <location filename="../../Physic/AngularAcceleration.cpp" line="46"/>
        <source>AngularAcceleration</source>
        <translation>AccélérationAngulaire</translation>
    </message>
    <message>
        <location filename="../../Physic/AngularPosition.cpp" line="46"/>
        <source>AngularPosition</source>
        <translation>PositionAngulaire</translation>
    </message>
    <message>
        <location filename="../../Physic/LinearAcceleration.cpp" line="46"/>
        <source>LinearAcceleration</source>
        <translation>AccélérationLinéaire</translation>
    </message>
    <message>
        <location filename="../../Physic/Torque.cpp" line="44"/>
        <source>Torque</source>
        <translation>Couple</translation>
    </message>
    <message>
        <location filename="../../Core/XmlConfigFile.cpp" line="67"/>
        <source>The version of your local configuration file &quot;</source>
        <translation>La version de votre fichier de configuration local</translation>
    </message>
    <message>
        <location filename="../../Core/XmlConfigFile.cpp" line="69"/>
        <source>is obsolete</source>
        <translation>est obsolète</translation>
    </message>
    <message>
        <location filename="../../Core/XmlConfigFile.cpp" line="70"/>
        <source>OpenMeca will replace it</source>
        <translation>OpenMeca va le remplacer automatiquement</translation>
    </message>
    <message>
        <location filename="../../Gui/GeneralSetting.cpp" line="90"/>
        <source>You are going to reset the setting of openmeca, are you sure ?</source>
        <translation>Vous êtes sur le point de supprimer vos préférences, êtes-vous sûr ?</translation>
    </message>
    <message>
        <source>It means that the config directory</source>
        <translation type="vanished">Le repertoire de configuration</translation>
    </message>
    <message>
        <source>will be removed</source>
        <translation type="vanished">va être supprimer</translation>
    </message>
    <message>
        <location filename="../../Main.cpp" line="45"/>
        <source>Welcome to openmeca</source>
        <translation>Bienvenu dans openmeca</translation>
    </message>
    <message>
        <location filename="../../Main.cpp" line="60"/>
        <source>Good bye, see you soon !</source>
        <translation>Au revoir, a bientot !</translation>
    </message>
</context>
<context>
    <name>WidgetDouble</name>
    <message>
        <location filename="../../Gui/Widget/WidgetDouble.ui" line="14"/>
        <source>Form</source>
        <translation>Fenêtre</translation>
    </message>
</context>
<context>
    <name>WidgetExpr</name>
    <message>
        <location filename="../../Gui/Widget/WidgetExpr.ui" line="14"/>
        <source>Form</source>
        <translation>Fenêtre</translation>
    </message>
</context>
<context>
    <name>WidgetScales</name>
    <message>
        <source>Form</source>
        <translation type="obsolete">Fenêtre</translation>
    </message>
    <message>
        <location filename="../../Gui/Widget/WidgetScales.ui" line="25"/>
        <source>%</source>
        <translation>%</translation>
    </message>
</context>
<context>
    <name>WidgetSelectItem</name>
    <message>
        <source>Form</source>
        <translation type="obsolete">Fenêtre</translation>
    </message>
</context>
</TS>
