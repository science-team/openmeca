// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@yakuru.fr>
//
// Copyright (C) 2012-2017 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Setting_Gravity_hpp
#define OpenMeca_Setting_Gravity_hpp

#include "Serialization/archive/text_oarchive.hpp"
#include "Serialization/archive/text_iarchive.hpp"

#include "OpenMeca/Core/SystemSettingT.hpp"
#include "OpenMeca/Core/SetOf.hpp"
#include "OpenMeca/Core/AutoRegister.hpp"
#include "OpenMeca/Core/None.hpp"

#include "OpenMeca/Geom/Vector.hpp"

namespace OpenMeca
{
  namespace Gui
  {
    class DialogGravity;
  }

  namespace Setting
  {

    class Gravity : public Core::SystemSettingT<Gravity>, public Core::AutoRegister<Gravity>
    {
    public:
      typedef Gui::DialogGravity Dialog;

    public:  
      static void Init();
      static const std::string GetStrType();
      static const QString GetQStrType();

    public:    
      Gravity();
      ~Gravity();


      void Save(boost::archive::text_oarchive&, const unsigned int);
      void Load(boost::archive::text_iarchive&, const unsigned int);

      void BuildChSystem(chrono::ChSystem&);

      // Accessors
      OMC_ACCESSOR(Value, Geom::Vector<_3D>,  g_);


    private:
      Gravity(const Gravity&);            //Not Allowed    
      Gravity& operator=(const Gravity&); //Not Allowed

    private:
      Geom::Vector<_3D> g_;
    };

   
  }
}
 
#endif
