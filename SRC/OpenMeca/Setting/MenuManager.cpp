// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@yakuru.fr>
//
// Copyright (C) 2012-2017 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "OpenMeca/Setting/MenuManager.hpp"
#include "OpenMeca/Core/GlobalSettingCommonProperty.hpp"
#include "OpenMeca/Util/Icon.hpp"

namespace OpenMeca
{
  namespace Setting
  {

    void
    MenuManager::Init()
    {
      Core::Singleton< Core::GlobalSettingCommonProperty<MenuManager> >::Get();
      Core::Singleton<MenuManager>::Get().ReadXmlFile();
    }

    const std::string
    MenuManager::GetStrType()
    {
      return "MenuManager";
    }

    

    MenuManager::MenuManager()
      :Core::GlobalSettingT<MenuManager>(),
       menuKey_("Menu"),
       classKey_("Class"),
       actionClassKey_("ActionClass"),
       actionKey_("Action"),
       separatorKey_("Separator"),
       actions_()
    {
    }
    
   
    MenuManager::~MenuManager()
    {
    }

    std::string 
    MenuManager::GetClassId() const
    {
      return GetStrType();
    }

    void MenuManager::ReadXmlFile()
    {
      QDomDocument doc("mydocument");
      QFile& file = OpenXmlConfigFile();
      OMC_ASSERT_MSG(doc.setContent(&file), "Can't set content of the xml file");
      QDomNode n = doc.firstChild();
      std::map<const QDomElement*, QMenu* > menuNode;

      while (!n.isNull()) 
	{
	  if (n.isElement()) 
	    {
	      QDomElement e = n.toElement();
	      if (e.tagName()==QString(GetClassId().c_str()))
		{
		  QDomNode n1 = e.firstChild ();
		  while (!n1.isNull()) 
		    {
		      if (n1.isElement()) 
			{
			  QDomElement e1 = n1.toElement();
			  const QString title = QObject::tr(qPrintable(e1.attribute("Title")));
			  OMC_ASSERT_MSG(title != QString(), "The title is empty");
			  QMenu* menu = 0;
			  if (e1.hasAttribute("Icon"))
			    {
			      QIcon icon = BuildIcon(e1);
			      menu = Gui::MainWindow::Get().menuBar()->addMenu(icon, title);
			    }
			  else
			    menu = Gui::MainWindow::Get().menuBar()->addMenu(title);
			  ReadMenu(e1, menu);
			}
		      n1 = n1.nextSibling();
		    }
		}
	    }
	  n = n.nextSibling();
	}
    }
    
    void
    MenuManager::ReadMenu(QDomElement& e, QMenu* menu)
    {
      OMC_ASSERT_MSG(e.tagName()==QString(menuKey_.c_str()),
		 "The first tag name must be MenuManager");
      QDomNode n = e.firstChild ();
      while (!n.isNull()) 
	{
	  if (n.isElement()) 
	    {
	      
	      QDomElement e = n.toElement();
	      if (e.tagName()==QString(classKey_.c_str()))
		{
		  const QString type = e.attribute("Type");
		  Core::CommonProperty& theClass = Core::CommonProperty::GetClass(type.toStdString());

		  QDomNode n1 = e.firstChild ();
		  while (!n1.isNull()) 
		    {
		      if (n1.isElement()) 
			{
			  
			  QDomElement e1 = n1.toElement();
			  if (e1.tagName()==QString(actionClassKey_.c_str()))
			    {
			      const QString Id = e1.attribute("Id");
			      Core::Action& action = theClass.GetAction(Id.toStdString());
			      if (e1.hasAttribute("Title"))
				{
				  const QString title = QObject::tr(qPrintable(e1.attribute("Title")));
				  action.setText(title);
				}
			      menu->addAction(&action);
			    }
			  else if (e1.tagName()==QString(separatorKey_.c_str()))
			    {
			      menu->addSeparator();
			    }
			  else if(e.tagName()==QString(actionKey_.c_str()))
			    {
			      AddAction(e, menu);
			    }
			  else
			    {
			      OMC_ASSERT_MSG(0, "Unknown tag name");
			    }
			}
		      n1 = n1.nextSibling();
		    }
		    
		  
		}
	      else if(e.tagName()==QString(actionKey_.c_str()))
		{
		  AddAction(e, menu);
		}
	      
	      else if (e.tagName()==QString(menuKey_.c_str()))
		{
		  const QString title = QObject::tr(qPrintable(e.attribute("Title")));
		  OMC_ASSERT_MSG(title!=QString(), "The title is empty");

		  QMenu* childmenu = 0;
		  if (e.hasAttribute("Icon"))
		    {
		      QIcon icon = BuildIcon(e);
		      childmenu = menu->addMenu(icon, title);
		    }
		  else
		    childmenu = menu->addMenu(title);
		  
		  ReadMenu(e, childmenu);
		}
	      
	      else if (e.tagName()==QString(separatorKey_.c_str()))
		{
		  menu->addSeparator();
		}
	      else
		{
		  OMC_ASSERT_MSG(0, "Unknown tag name");
		}

	      
	    }
	  n = n.nextSibling();
	}
    }

    void MenuManager::AddAction(QDomElement& e, QMenu* menu)
    {
      OMC_ASSERT_MSG(e.tagName()==QString(actionKey_.c_str()), "Unknown tag name");
      const QString title = QObject::tr(qPrintable(e.attribute("Title")));
      const QString UniqueID = e.attribute("UniqueId");
      OMC_ASSERT_MSG(actions_.count(UniqueID.toStdString()) == 0, "This action ID already exists");
      QAction* action = new QAction(title, menu);
      if (e.hasAttribute("Icon"))
	action->setIcon(BuildIcon(e));
      menu->addAction(action);
      actions_[UniqueID.toStdString()] = action;
    }

    QAction* 
    MenuManager::GetQAction(const std::string& actionID)
    {
      OMC_ASSERT_MSG(actions_.count(actionID) == 1, "Can't find this action");
      return actions_[actionID];
    }


    void MenuManager::WriteXmlFile()
    {
      OMC_ASSERT_MSG(0, "Not implemented yet");//TODO
      
    }

    QIcon 
    MenuManager::BuildIcon(QDomElement& e)
    {
      OMC_ASSERT_MSG(e.hasAttribute("Icon"),
		     "The element must have the Icon tag !");
      const QString iconStr = ":/Rsc/Img/" + e.attribute("Icon");
      QIcon icon;
      if (e.hasAttribute("KeepIconColor"))
	icon = Util::Icon::DrawIconFromSvgFile(iconStr);
      else
	Util::Icon::DrawIconFromSvgFile(iconStr, icon, Qt::gray);
      return icon;
    }


  }
}
