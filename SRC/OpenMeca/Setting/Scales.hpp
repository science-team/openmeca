// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@yakuru.fr>
//
// Copyright (C) 2012-2017 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Setting_Scales_hpp
#define OpenMeca_Setting_Scales_hpp

#include "Serialization/archive/text_oarchive.hpp"
#include "Serialization/archive/text_iarchive.hpp"

#include "OpenMeca/Core/SystemSettingT.hpp"
#include "OpenMeca/Core/SetOf.hpp"
#include "OpenMeca/Gui/Widget/WidgetScales.hpp"
#include "OpenMeca/Core/AutoRegister.hpp"
#include "OpenMeca/Core/None.hpp"

namespace OpenMeca
{
  namespace Gui
  {
    class DialogScales;
  }

  namespace Setting
  {


    // Class that models the different scales for the 3D view. All the 
    // 3D object can be scaled by the user to adapt the sketch to the 
    // system dimension.
    class Scales : public Core::SystemSettingT<Scales>, public Core::AutoRegister<Scales>
    {
    public:
      typedef Gui::DialogScales Dialog;

    public:  
      static void Init();
      static const std::string GetStrType();
      static const QString GetQStrType();

    public:    
      Scales();
      ~Scales();

      void SetWidget(Gui::WidgetScales&);
      void AddScale(const std::string&);
      double& GetScaleValue(const std::string&);
      void Update();
      const std::vector<std::string>& GetKeys() const;

      void Save(boost::archive::text_oarchive&, const unsigned int);
      void Load(boost::archive::text_iarchive&, const unsigned int);

    private:
      Scales(const Scales&);            //Not Allowed    
      Scales& operator=(const Scales&); //Not Allowed

    private:
      std::map<std::string, double> scales_;
      std::vector<std::string> scaleKeys_;
      Gui::WidgetScales* widget_;
    };

   
  }
}
 
#endif
