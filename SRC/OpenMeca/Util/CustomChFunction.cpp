// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@yakuru.fr>
//
// Copyright (C) 2012-2017 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.




#include "OpenMeca/Util/CustomChFunction.hpp"
#include "OpenMeca/Util/Var.hpp"


namespace OpenMeca
{  
  namespace Util
  {

    CustomChFunction::CustomChFunction(double& x, double y)
      :Expr(),
       chrono::ChFunction(),
       x_(x), y_(y)
       
    {
      Expr::SetVariable(y_);
    }


    CustomChFunction::~CustomChFunction()
    {
    }

    void 
    CustomChFunction::PostSerializationLoad()
    {
      // We manage unit here
      if (IsExpressionNumber())
	SetExpressionFromValue(y_/GetFactor());
    }


    double 
    CustomChFunction::Get_y(double)
    {
      Expr::Update();
      return y_;
    }

    double 
    CustomChFunction::Get_y_dx(double)
    {
      OMC_ASSERT_MSG(Expr::var_ != 0,
		     "The variable was not initialized");

      exprtk::expression<double> expression;
      expression.register_symbol_table(Var::GetSymbolTable());

      exprtk::parser<double> parser;
      OMC_ASSERT_MSG(parser.compile(Expr::expression_, expression),
		     "Can't understand the expression \"" + expression_ + "\"");
      
      return exprtk::derivative(expression, x_)*GetFactor();
    }

    double 
    CustomChFunction::Get_y_dxdx (double)
    {
      OMC_ASSERT_MSG(Expr::var_ != 0,
		     "The variable was not initialized");

      exprtk::expression<double> expression;
      expression.register_symbol_table(Var::GetSymbolTable());

      exprtk::parser<double> parser;
      OMC_ASSERT_MSG(parser.compile(Expr::expression_, expression),
		     "Can't understand the expression \"" + expression_ + "\"");
      
      return exprtk::second_derivative(expression, x_)*GetFactor();
    }

    chrono::ChFunction* 
    CustomChFunction::new_Duplicate ()
    {
      CustomChFunction& f = *new CustomChFunction(x_, y_);
      f.expression_ = expression_;
      f.SetDimension(GetDimension());
      return &f;
    }


  }
}
