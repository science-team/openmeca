// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@yakuru.fr>
//
// Copyright (C) 2012-2017 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <sstream>

#include "OpenMeca/Util/Expr.hpp"
#include "OpenMeca/Util/Var.hpp"




namespace OpenMeca
{  
  namespace Util
  {



    Core::SetOfBase<Expr> Expr::all_ = Core::SetOfBase<Expr>();


    bool 
    Expr::IsValid(const std::string & exp_str)
    {
      double x = 0.;
      Expr exp(exp_str, x);
      return exp.Check();
    }

    double 
    Expr::Compute(const std::string &exp_str)
    {
      double x = 0.;
      Expr exp(exp_str, x);
      return exp();
    }

    void
    Expr::UpdateAll()
    {
      Var::UpdateTable();

      for (unsigned int i =0; i < all_.GetTotItemNumber(); ++i)
	if (!all_(i).IsNull())
	  all_(i).Update();
    }

    

    Expr::Expr()
      :dim_(0), var_(0),
       expression_("0"),
       isCopy_(false)
    {
      all_.AddItem(*this);
    }

    Expr::Expr(double& var)
      :dim_(0), var_(0),
       expression_(""),
       isCopy_(false)
    {
      SetVariable(var);
      all_.AddItem(*this);
    }

    Expr::Expr(const std::string& exp, double& var)
      :dim_(0), var_(&var),
       expression_(exp),
       isCopy_(false)
    {
      all_.AddItem(*this);
    }

    Expr::Expr(const Expr& exp)
      :dim_(exp.dim_), var_(0),
       expression_(exp.expression_),
       isCopy_(true)
    {
    }



    Expr::~Expr()
    {
      if (!isCopy_)
	all_.RemoveItem(*this);
    }

    bool 
    Expr::IsNull() const
    {
      return expression_.empty();
    }

    void 
    Expr::SetVariable(double& var)
    {
      OMC_ASSERT_MSG(var_==0, "The variable is already defined");
      SetExpressionFromValue(var);
      var_ = &var;
    }

    Expr& 
    Expr::operator=(const Expr& exp)
    {
      expression_ = exp.expression_;
      return *this;
    }

    bool
    Expr::Check()
    {
      OMC_ASSERT_MSG(var_ != 0,
		     "The variable was not initialized");

      exprtk::expression<double> expression;
      expression.register_symbol_table(Var::GetSymbolTable());

      exprtk::parser<double> parser;
      return parser.compile(expression_,expression);
    }


    double 
    Expr::operator()(void)
    {
      exprtk::expression<double> expression;
      expression.register_symbol_table(Var::GetSymbolTable());

      exprtk::parser<double> parser;
      OMC_ASSERT_MSG(parser.compile(expression_,expression),
		     "Can't understand the expression \"" + expression_ + "\"");
      return expression.value();
    }

    void 
    Expr::Update()
    {
      OMC_ASSERT_MSG(var_ != 0      , "The variable was not initialized");
      OMC_ASSERT_MSG(expression_!="", "The expression is empty");
      
      Expr& me = *this;
      double& var = *var_;
      var = me()*GetFactor();
    }

    const std::string& 
    Expr::ToString() const
    {
      return expression_;
    }
    
    void 
    Expr::SetString(std::string str)
    {
      expression_ = std::string(str);
    }

    void
    Expr::SetExpressionFromValue(double val)
    {
      std::ostringstream strs;
      strs << val;
      expression_ = strs.str();
    }

    double 
    Expr::GetVariableValue() const
    {
      OMC_ASSERT_MSG(var_ != 0, "The variable was not initialized");
      return *var_;
    }

    void 
    Expr::SetDimension(const Dimension& dim)
    {
      OMC_ASSERT_MSG(dim_==0, "The expression is already defined");
      dim_ = &dim;
    }
    
    const Dimension& 
    Expr::GetDimension() const
    {
      OMC_ASSERT_MSG(dim_!=0, "The expression has no dimension");
      return *dim_;
    }
    
    double 
    Expr::GetFactor() const
    {
      return GetDimension().GetUserChoice().GetFactor();
    }

    bool 
    Expr::IsExpressionNumber() const
    {
      const QString str =  ToString().c_str();
      bool isNumber = false;
      str.toDouble(&isNumber);
      return isNumber;
    }
    

  }

}


