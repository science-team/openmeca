// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@yakuru.fr>
//
// Copyright (C) 2012-2017 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <sstream>

#include "OpenMeca/Util/ExprAttitude.hpp"
#include "OpenMeca/Util/Var.hpp"




namespace OpenMeca
{  
  namespace Util
  {
      
    ExprAttitude::ExprAttitude(std::function<const Geom::Frame<_3D>& ()> f)
      :Geom::Quaternion<_3D>(f),
       axis_(1., 0., 0., f), angle_(0)
    {
      axis_.SetDimension(Util::Dimension::Get("Length"));
      angle_.SetDimension(Util::Dimension::Get("Angle"));
    }



    ExprAttitude::~ExprAttitude()
    {
    }


    ExprAttitude& 
    ExprAttitude::operator=(const Geom::Quaternion<_3D>& q)
    {
      Geom::Vector<_3D> axis = q.GetAxis();
      if (axis.GetNorm() == 0.)
	axis[0] = 1.;
      const double angle = q.GetAngle();
      axis_  = axis;
      angle_ = angle;
      return (*this);
    }

    ExprAttitude& 
    ExprAttitude::operator=(const ExprAttitude& q)
    {
      axis_ = q.axis_;
      angle_ = q.angle_;
      return (*this);
    }

    

    void
    ExprAttitude::Update()
    {
      axis_.GetExpressionX().Update();
      axis_.GetExpressionY().Update();
      axis_.GetExpressionZ().Update();
      angle_.Update();
      
      Geom::Quaternion<_3D>::SetAxisAngle(axis_,  angle_.GetValue());
    }

  

  }

}


