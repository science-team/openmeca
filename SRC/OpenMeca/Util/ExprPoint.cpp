// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@yakuru.fr>
//
// Copyright (C) 2012-2017 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <sstream>

#include "OpenMeca/Util/ExprPoint.hpp"
#include "OpenMeca/Util/Var.hpp"




namespace OpenMeca
{  
  namespace Util
  {
      
    ExprPoint::ExprPoint(std::function<const Geom::Frame<_3D>& ()> f)
      :Geom::Point<_3D>(f),
       expX_(Geom::Point<_3D>::operator[](0)),
       expY_(Geom::Point<_3D>::operator[](1)),
       expZ_(Geom::Point<_3D>::operator[](2))
    {
      expX_.SetDimension(Util::Dimension::Get("Length"));
      expY_.SetDimension(Util::Dimension::Get("Length"));
      expZ_.SetDimension(Util::Dimension::Get("Length"));
      
    }

    ExprPoint::ExprPoint(double x, double y, double z, std::function<const Geom::Frame<_3D>& ()> f)
      :Geom::Point<_3D>(x, y, z, f),
       expX_(Geom::Point<_3D>::operator[](0)),
       expY_(Geom::Point<_3D>::operator[](1)),
       expZ_(Geom::Point<_3D>::operator[](2))
    {
      expX_.SetDimension(Util::Dimension::Get("Length"));
      expY_.SetDimension(Util::Dimension::Get("Length"));
      expZ_.SetDimension(Util::Dimension::Get("Length"));
    }


    ExprPoint::~ExprPoint()
    {
    }


    ExprPoint& 
    ExprPoint::operator=(const Geom::Point<_3D>& p)
    {
      Geom::Point<_3D>& me = *this;
      me = p;
      expX_.SetExpressionFromValue(me[0]);
      expY_.SetExpressionFromValue(me[1]);
      expZ_.SetExpressionFromValue(me[2]);
      return (*this);
    }

    ExprPoint& 
    ExprPoint::operator=(const ExprPoint& p)
    {
      expX_ = p.expX_;
      expY_ = p.expY_;
      expZ_ = p.expZ_;
      return (*this);
    }

  

  }

}


