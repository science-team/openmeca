// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@yakuru.fr>
//
// Copyright (C) 2012-2017 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <QFile>

#include "OpenMeca/Util/Icon.hpp"
#include "OpenMeca/Gui/MainWindow.hpp"

namespace OpenMeca
{
  namespace Util
  {

    QByteArray
    Icon::ChangeColorInSvgFile(const QString& fileName, const QColor& from, const QColor& to)
    {
      QFile file(fileName);
      OMC_ASSERT_MSG(file.open(QFile::ReadOnly | QFile::Text), "Can't open svg file");
      QByteArray fileData;
      fileData = file.readAll();
      QString text(fileData);
      text.replace(from.name(), to.name());
      file.close();
      return text.toLatin1();
      
    }

    QByteArray
    Icon::ChangeColorInSvgFile(const QString& fileName, const QColor& from1, const QColor& to1,const QColor& from2, const QColor& to2)
    {
      QFile file(fileName);
      OMC_ASSERT_MSG(file.open(QFile::ReadOnly | QFile::Text), "Can't open svg file");
      QByteArray fileData;
      fileData = file.readAll();
      QString text(fileData);
      text.replace(from1.name(), to1.name());
      text.replace(from2.name(), to2.name());
      file.close();
      return text.toLatin1();
      
    }

    

    QIcon
    Icon::DrawIconFromSvgFile(const QString& fileName)
    {
      QSvgRenderer svgRenderer(fileName);
      // render it on pixmap and translate to icon
      const int size = Gui::MainWindow::Get().GetIconSize();
      QPixmap pixmap(QSize(size,size));
      pixmap.fill(QColor (0, 0, 0, 0));
      QPainter painter(&pixmap);
      svgRenderer.render( &painter);
      return QIcon(pixmap);
    }
    

    void
    Icon::DrawIconFromSvgFile(const QString& fileName, QIcon& icon, QColor color)
    {
      QSvgRenderer svgRenderer(Util::Icon::ChangeColorInSvgFile(fileName, Qt::red, color));

      // render it on pixmap and translate to icon
      const int size = Gui::MainWindow::Get().GetIconSize();
      QPixmap pixmap(QSize(size,size));
      pixmap.fill(QColor (0, 0, 0, 0));
      QPainter painter(&pixmap);
      svgRenderer.render( &painter);
      icon =  QIcon(pixmap);
    }

   
  }
}
