// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@yakuru.fr>
//
// Copyright (C) 2012-2017 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Util_Lang_hpp
#define OpenMeca_Util_Lang_hpp

#include <string>
#include <map>

#include "OpenMeca/Core/Macro.hpp"

namespace OpenMeca
{
  namespace Util
  {

    class Lang
    {
    public: 
      static std::string GetStrType();
      static Lang& GetByID(const std::string&);
      static Lang& GetByIndex(const int);
      static std::map<const std::string, Lang*>& GetAll();

    private:
      static std::map<const std::string, Lang*> all_;


    public:
      Lang(const std::string& id, const std::string& name, const std::string& file);
      ~Lang();
      void Load();
      int GetIndex() const;
      
      OMC_ACCESS_CST(ID  , std::string, id_);
      OMC_ACCESS_CST(Name, std::string, name_);
      OMC_ACCESS_CST(File, std::string, file_);

    private:
      const std::string id_; 
      const std::string name_;
      const std::string file_;
    };
      
  }
}
#endif
