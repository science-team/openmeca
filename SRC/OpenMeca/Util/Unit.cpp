// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@yakuru.fr>
//
// Copyright (C) 2012-2017 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "OpenMeca/Util/Unit.hpp"
#include "OpenMeca/Util/Dimension.hpp"


namespace OpenMeca
{
  namespace Util
  {

    std::string Unit::GetStrType(){return "Unit";}

    Unit::Unit(Dimension& dimension, const std::string name, const std::string symbol, const double factor)
      :dimension_(dimension),
       name_(name), 
       symbol_(symbol), 
       factor_(factor)
    {
      dimension_.AddUnit(*this);
    }
    

    Unit::~Unit()
    {
      dimension_.RemoveUnit(*this);
    }


    const std::string& 
    Unit::GetName() const
    {
      return name_;
    }

    const std::string& 
    Unit::GetSymbol() const
    {
      return symbol_;
    }

    double 
    Unit::GetFactor() const
    {
      return factor_;
    }

    const Dimension&
    Unit::GetDimension() const
    {
      return dimension_;
    }
    
      
  }
}
