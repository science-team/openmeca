// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@yakuru.fr>
//
// Copyright (C) 2012-2017 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <QString>
#include <QStringList>

#include "OpenMeca/Util/Version.hpp"
#include "OpenMeca/Core/Macro.hpp"

namespace OpenMeca
{
  namespace Util
  {

    std::ostream& 
    operator<< (std::ostream& os, const Version & v)
    {
      os << v.str_;
      return os;
    }


    Version::Version(const std::string& vstr)
      :str_(""),
       omc_major_(0), omc_minor_(0), omc_revision_(0)
    {
      ReadString(vstr);
    }


    Version::Version()
      :str_(""),
       omc_major_(0), omc_minor_(0), omc_revision_(0)
       
    {
    }
   
    Version::~Version()
    {
    }

    void 
    Version::ReadString(const std::string& vstr)
    {
      str_ = vstr;
      QString str(vstr.c_str());
      QStringList fields = str.split(".");
      OMC_ASSERT_MSG(fields.count()==3, 
		     "Problem while reading version, the version must contain tree number : major.minor.revision and I read \"" + vstr + "\"");
      omc_major_    = fields[0].toInt();
      omc_minor_    = fields[1].toInt();
      omc_revision_ = fields[2].toInt();
    }

    const std::string& 
    Version::ToString() const
    {
      OMC_ASSERT_MSG(str_ != "", "The required version is empty");
      return str_;
    }

    
    bool 
    Version::operator<(const Version& otherVersion) const
    {
      OMC_ASSERT_MSG(str_ != "", "The required version is empty");
      OMC_ASSERT_MSG(otherVersion.str_ != "", "The required version is empty");

       if(omc_major_ < otherVersion.omc_major_)
            return true;
        if(omc_minor_ < otherVersion.omc_minor_)
            return true;
        if(omc_revision_ < otherVersion.omc_revision_)
            return true;
        return false;
    }
    bool 
    Version::operator>(const Version& otherVersion) const
    {

      if(omc_major_ > otherVersion.omc_major_)
	return true;
      else if(omc_major_ < otherVersion.omc_major_)
	return false;
      
      if(omc_minor_ > otherVersion.omc_minor_)
	return true;
      else if(omc_minor_ < otherVersion.omc_minor_)
	return false;
      
      if(omc_revision_ > otherVersion.omc_revision_)
	return true;
      else if(omc_revision_ < otherVersion.omc_revision_)
	return false;

        return false;
    }

     bool 
    Version::operator==(const Version& otherVersion) const
    {
      OMC_ASSERT_MSG(str_ != "", "The required version is empty");
      OMC_ASSERT_MSG(otherVersion.str_ != "", "The required version is empty");
      return (omc_major_    == otherVersion.omc_major_ &&
	      omc_minor_    == otherVersion.omc_minor_ && 
	      omc_revision_ == otherVersion.omc_revision_);
    }
      

  }
}
