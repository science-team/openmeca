######################################################################
# qmake internal options
######################################################################

CONFIG           += qt     
CONFIG           += warn_on
CONFIG           += no_keywords
CONFIG           += silent
CONFIG           += release
CONFIG           += staticlib

TEMPLATE  = lib
TARGET    = ./BUILD/serialization
DESTDIR   = ./

QMAKE_CXXFLAGS +=-w -D CH_API_COMPILE

OBJECTS_DIR = ./BUILD/obj

INCLUDEPATH = ../ 

CONFIG += c++11

SOURCES += ./src/binary_oarchive.cpp \
./src/basic_pointer_iserializer.cpp  \
./src/extended_type_info_typeid.cpp  \
./src/xml_oarchive.cpp  \
./src/xml_iarchive.cpp \
./src/binary_woarchive.cpp \
./src/codecvt_null.cpp \
./src/binary_wiarchive.cpp \
./src/text_woarchive.cpp \
./src/shared_ptr_helper.cpp \
./src/basic_xml_archive.cpp \
./src/basic_oarchive.cpp \
./src/binary_iarchive.cpp \
./src/archive_exception.cpp \
./src/stl_port.cpp \
./src/basic_iarchive.cpp \
./src/xml_archive_exception.cpp \
./src/extended_type_info.cpp \
./src/basic_text_woprimitive.cpp \
./src/void_cast.cpp \
./src/basic_text_wiprimitive.cpp \
./src/basic_iserializer.cpp \
./src/polymorphic_oarchive.cpp \
./src/basic_archive.cpp \
./src/text_iarchive.cpp \
./src/xml_woarchive.cpp \
./src/xml_wiarchive.cpp \
./src/basic_pointer_oserializer.cpp \
./src/xml_grammar.cpp \
./src/basic_text_oprimitive.cpp \
./src/utf8_codecvt_facet.cpp \
./src/polymorphic_iarchive.cpp \
./src/basic_text_iprimitive.cpp \
./src/text_oarchive.cpp \
./src/basic_oserializer.cpp \
./src/basic_serializer_map.cpp \
./src/extended_type_info_no_rtti.cpp \
./src/xml_wgrammar.cpp \
./src/text_wiarchive.cpp

target.path = ./BUILD/lib
include.path = ./BUILD/include/Serialization
include.files = $${HEADERS}
INSTALLS *= target
